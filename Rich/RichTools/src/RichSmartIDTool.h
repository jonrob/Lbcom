/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichSmartIDTool.h
 *
 *  Header file for tool : Rich::SmartIDTool
 *
 *  @author Antonis Papanestis
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   2003-10-28
 */
//-----------------------------------------------------------------------------

#pragma once

#include "DetDesc/GenericConditionAccessorHolder.h"

// Base class
#include "RichKernel/RichToolBase.h"

// Interfaces
#include "RichInterfaces/IRichSmartIDTool.h"

// Utils
#include "RichUtils/RichMap.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichSmartIDSorter.h"

// RichDet
#include "RichDet/DeRich.h"
#include "RichDet/DeRichPDPanel.h"
#include "RichDet/DeRichSystem.h"

#include <array>
#include <vector>

namespace Rich {

  //-----------------------------------------------------------------------------
  /** @class SmartIDTool RichSmartIDTool.h
   *
   *  A tool to preform the manipulation of RichSmartID channel identifiers
   *
   *  @author Antonis Papanestis
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   2003-10-28
   *
   *  @todo Move application of panel offset in RichSmartIDTool::globalToPDPanel
   *        into DeRichPDPanel class
   */
  //-----------------------------------------------------------------------------

  class SmartIDTool final : public LHCb::DetDesc::ConditionAccessorHolder<extends<Rich::ToolBase, ISmartIDTool>> {

  public: // Methods for Gaudi Framework
    using ConditionAccessorHolder::ConditionAccessorHolder;
    StatusCode initialize() override;

  public: // methods (and doxygen comments) inherited from interface
    // Converts a RichSmartID channel identification into a position in global LHCb coordinates.
    bool globalPosition( const LHCb::RichSmartID& smartid, Gaudi::XYZPoint& detectPoint ) const override;

    // Finds the average position of a cluster of RichSmartIDs, in global LHCb coordinates
    bool globalPosition( const Rich::PDPixelCluster& cluster, Gaudi::XYZPoint& detectPoint ) const override;

    // Converts an PD RichSmartID identification into a position in global LHCb coordinates.
    bool pdPosition( const LHCb::RichSmartID& pdid, Gaudi::XYZPoint& pdPoint ) const override;

    // Computes the global position coordinate for a given position in local
    Gaudi::XYZPoint globalPosition( const Gaudi::XYZPoint& localPoint, const Rich::DetectorType rich,
                                    const Rich::Side side ) const override;

    // Converts a position in global coordinates to the corresponding RichSmartID
    bool smartID( const Gaudi::XYZPoint& globalPoint, LHCb::RichSmartID& smartid ) const override;

    // Supplies a vector of all currently active and valid channels in the RICH detectors
    LHCb::RichSmartID::Vector readoutChannelList() const override {
      return readoutChannelList( m_photoDetPanels.get() );
    }

    // Supplies a vector of all currently active and valid channels in the RICH detectors
    LHCb::RichSmartID::Vector readoutChannelList( RichPDPanels const& ) const override;

    // Converts a position in global coordinates to the local coordinate system.
    Gaudi::XYZPoint globalToPDPanel( const Gaudi::XYZPoint& globalPoint ) const override;

    std::string_view getPDPanelsPath() const override { return m_photoDetPanels.key(); }

  private:
    /// photodetector for each rich
    ConditionAccessor<RichPDPanels> m_photoDetPanels{this, name() + "-PhotoDetPanels"};

    Gaudi::Property<bool> m_hitPhotoCathSide{this, "HitOnPhotoCathSide", false,
                                             "false to get the hit on the outside of PD window (inlcuding refraction)"};
  };

} // namespace Rich
