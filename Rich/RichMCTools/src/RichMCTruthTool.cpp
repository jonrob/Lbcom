/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// From PartProp
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/ParticleProperty.h"

// STL
#include <algorithm>
#include <memory>
#include <string>

// from Gaudi
#include "GaudiKernel/IIncidentListener.h"
#include "GaudiKernel/IIncidentSvc.h"

// base class
#include "RichKernel/RichToolBase.h"

// Kernel
#include "Kernel/RichParticleIDType.h"

// Rich Utils
#include "RichUtils/RichHashMap.h"
#include "RichUtils/RichMap.h"
#include "RichUtils/RichPixelCluster.h"

// Event model
#include "Event/MCParticle.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichDigitSummary.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichOpticalPhoton.h"
#include "Event/MCRichSegment.h"
#include "Event/MCRichTrack.h"
#include "Event/MCTruth.h"
#include "Event/Track.h"
#include "Kernel/RichParticleIDType.h"

// Linkers
#include "Linker/LinkedTo.h"

// Interfaces
#include "MCInterfaces/IRichMCTruthTool.h"

namespace Rich::MC {

  //-----------------------------------------------------------------------------
  /** @class MCTruthTool RichMCTruthTool.h
   *
   *  Tool performing MC truth associations
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   15/03/2002
   *
   *  @todo Figure out how to best deal with sub-pixel info in MC mappings
   */
  //-----------------------------------------------------------------------------

  class MCTruthTool final : public extends<Rich::ToolBase, IMCTruthTool, IIncidentListener> {

  public: // Methods for Gaudi Framework
    /// Standard constructor
    using extends::extends;

    // Initialization of the tool after creation
    StatusCode initialize() override;

    /** Implement the handle method for the Incident service.
     *  This is used to inform the tool of software incidents.
     *
     *  @param incident The incident identifier
     */
    void handle( const Incident& incident ) override;

  public: // methods (and doxygen comments) inherited from interface
    // Find best MCParticle association for a given reconstructed Track
    const LHCb::MCParticle* mcParticle( const LHCb::Track* track, const double minWeight = 0.5 ) const override;

    // get MCRichHits for MCParticle
    const SmartRefVector<LHCb::MCRichHit>& mcRichHits( const LHCb::MCParticle* mcp ) const override;

    // Get the MCRichHits associated to a given RichSmartID
    const SmartRefVector<LHCb::MCRichHit>& mcRichHits( const LHCb::RichSmartID smartID ) const override;

    // Get a vector of MCParticles associated to given RichSmartID
    bool mcParticles( const LHCb::RichSmartID id, std::vector<const LHCb::MCParticle*>& mcParts ) const override;

    // Determines the particle mass hypothesis for a given MCParticle
    Rich::ParticleIDType mcParticleType( const LHCb::MCParticle* mcPart ) const override;

    // Finds the MCRichDigit association for a RichSmartID channel identifier
    const LHCb::MCRichDigit* mcRichDigit( const LHCb::RichSmartID id ) const override;

    // Finds the MCRichTrack associated to a given MCParticle
    const LHCb::MCRichTrack* mcRichTrack( const LHCb::MCParticle* mcPart ) const override;

    // Finds the MCRichOpticalPhoton associated to a given MCRichHit
    const LHCb::MCRichOpticalPhoton* mcOpticalPhoton( const LHCb::MCRichHit* mcHit ) const override;

    // Access the bit-pack history object for the given RichSmartID
    bool getMcHistories( const LHCb::RichSmartID                       id,
                         std::vector<const LHCb::MCRichDigitSummary*>& histories ) const override;

    // Checks if the given RichSmartID is the result of a background hit
    bool isBackground( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of a photon which
    // underwent reflections inside the PD
    bool isHPDReflection( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of backscattering
    // of the HPD silicon sensor
    bool isSiBackScatter( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of scintillation
    // of the radiator
    bool isRadScintillation( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of true Cherenkov
    bool isCherenkovRadiation( const LHCb::RichSmartID id, const Rich::RadiatorType rad ) const override;

    // Returns true if MC information for the RICH hits are available
    bool richMCHistoryAvailable() const override;

    // Checks if RICH extended MC information (MCRichOpticalPhoton, MCRichSegment etc.)
    bool extendedMCAvailable() const override;

    // Get the MCRichHits associated to a cluster of RichSmartIDs
    void mcRichHits( const Rich::PDPixelCluster& cluster, SmartRefVector<LHCb::MCRichHit>& hits ) const override;

    // Access the bit-pack history objects for the given cluster of RichSmartIDs
    bool getMcHistories( const Rich::PDPixelCluster&                   cluster,
                         std::vector<const LHCb::MCRichDigitSummary*>& histories ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of a background
    bool isBackground( const Rich::PDPixelCluster& cluster ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of a photon which
    // underwent reflections inside the PD
    bool isHPDReflection( const Rich::PDPixelCluster& cluster ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of backscattering
    // of the PD silicon sensor
    bool isSiBackScatter( const Rich::PDPixelCluster& cluster ) const override;

    // Checks if the given cluster of RichSmartIDs is the result scintillation
    // of radiator
    bool isRadScintillation( const Rich::PDPixelCluster& cluster ) const override;

    // Checks if the given RichSmartID is the result of true Cherenkov
    bool isCherenkovRadiation( const Rich::PDPixelCluster& cluster, const Rich::RadiatorType rad ) const override;

    // Get a vector of MCParticles associated to given RichSmartID cluster
    bool mcParticles( const Rich::PDPixelCluster&           cluster,
                      std::vector<const LHCb::MCParticle*>& mcParts ) const override;

  private: // definitions
    /// typedef of the Linker object for MCParticles to MCRichTracks
    typedef LinkedTo<LHCb::MCRichTrack> MCPartToRichTracks;

    /// typedef of the Linker object for MCRichHits to MCRichOpticalPhotons
    typedef LinkedTo<LHCb::MCRichOpticalPhoton> MCRichHitToPhoton;

    /// Typedef for vector of pointers to MCRichDigitSummaries
    typedef std::vector<const LHCb::MCRichDigitSummary*> MCRichDigitSummaries;

    /// Typedef for map between RichSmartIDs and MCRichDigitSummary objects
    typedef Rich::Map<const LHCb::RichSmartID, MCRichDigitSummaries> MCRichDigitSummaryMap;

    /// Typedef for mapping from MCParticle to MCRichHits
    typedef Rich::Map<const LHCb::MCParticle*, SmartRefVector<LHCb::MCRichHit>> MCPartToMCRichHits;

    /// Typedef for mapping between RichSmartIDs and MCRichHits
    typedef Rich::Map<const LHCb::RichSmartID, SmartRefVector<LHCb::MCRichHit>> SmartIDToMCRichHits;

    // track linker stuff
    typedef LinkedTo<LHCb::MCParticle>             TrackToMCP;
    typedef Rich::HashMap<std::string, TrackToMCP> TrackLocToMCPs;

  private: // private methods
    /// Returns the linker object for Tracks to MCParticles
    const TrackToMCP* trackToMCPLinks( const LHCb::Track* track = nullptr ) const;

    /// Returns the linker for the given Track location
    const TrackToMCP* trackToMCPLinks( const std::string& loc ) const;

    /// Access the mapping from MCParticles to MCRichHits
    const MCPartToMCRichHits& mcPartToMCRichHitsMap() const;

    /// Access the mapping from RichSmartIDs to MCRichHits
    const SmartIDToMCRichHits& smartIDToMCRichHitsMap() const;

    /// Returns the linker object for MCParticles to MCRichTracks
    MCPartToRichTracks* mcTrackLinks() const;

    /// Returns the linker object for MCRichHits to MCRichOpticalPhotons
    MCRichHitToPhoton* mcPhotonLinks() const;

    /** Loads the MCRichDigits from the TES
     *
     * @return Pointer to the MCRichDigits
     * @retval NULL means no MC information is available
     */
    const LHCb::MCRichDigits* mcRichDigits() const;

    /** Loads the MCRichDigitSummarys from TES
     *
     * @return Pointer to the MCRichDigitSummaryVector
     * @retval NULL means no MC information is available
     */
    const LHCb::MCRichDigitSummarys* mcRichDigitSummaries() const;

    /** Loads the MCRichHits from the TES
     *
     * @return Pointer to the MCRichHits
     * @retval NULL means no MC information is available
     */
    const LHCb::MCRichHits* mcRichHits() const;

    /// Initialise for a new event
    void InitNewEvent();

    /// Access the map between RichSmartIDs and MCRichDigitSummaries
    const MCRichDigitSummaryMap& mcRichDigSumMap() const;

  private: // private data
    /// Flag to say MCRichDigits have been loaded for this event
    mutable bool m_mcRichDigitsDone{false};

    /// Flag to say MCRichDigitSummarys has been loaded for this event
    mutable bool m_mcRichDigitSumsDone{false};

    /// Flag to say mapping between RichSmartIDs and MCRichDigitSummary objects
    /// has been created for this event
    mutable bool m_mcRichDigSumMapDone{false};

    /// Flag to say MCRichHits have been loaded for this event
    mutable bool m_mcRichHitsDone{false};

    /// Pointer to MCRichDigits
    mutable LHCb::MCRichDigits* m_mcRichDigits = nullptr;

    /// Pointer to MCRichDigitSummarys
    mutable LHCb::MCRichDigitSummarys* m_mcRichDigitSums = nullptr;

    /// Pointer to MCRichDigits
    mutable LHCb::MCRichHits* m_mcRichHits = nullptr;

    /// Linker for MCParticles to MCRichTracks
    mutable std::unique_ptr<MCPartToRichTracks> m_mcTrackLinks;

    /// Linker for MCRichHits to MCRichOpticalPhotons
    mutable std::unique_ptr<MCRichHitToPhoton> m_mcPhotonLinks;

    /// mapping from MCParticles to MCRichHits
    mutable MCPartToMCRichHits m_mcPToHits;

    /// mapping for RichSmartIDs to MCRichHits
    mutable SmartIDToMCRichHits m_smartIDsToHits;

    /// Location of MCRichDigits in EDS
    Gaudi::Property<std::string> m_mcRichDigitsLocation{this, "MCRichDigitsLocation",
                                                        LHCb::MCRichDigitLocation::Default};

    /// Location of MCRichDigitSummarys in EDS
    Gaudi::Property<std::string> m_mcRichDigitSumsLocation{this, "MCRichDigitSummariesLocation",
                                                           LHCb::MCRichDigitSummaryLocation::Default};

    /// Location of MCRichHits in EDS
    Gaudi::Property<std::string> m_mcRichHitsLocation{this, "MCRichHitsLocation", LHCb::MCRichHitLocation::Default};

    /// PID information
    Rich::Map<int, Rich::ParticleIDType> m_localID;

    /// Map between RichSmartIDs and MCRichDigitSummary objects
    mutable MCRichDigitSummaryMap m_mcRichDigSumMap;

    /// Empty container for missing links
    SmartRefVector<LHCb::MCRichHit> m_emptyContainer;

    /// Location of Tracks in TES
    Gaudi::Property<std::string> m_trLoc{this, "TrackLocation", "/Event/" + LHCb::TrackLocation::Default};

    /// Linker for Tracks to MCParticles
    mutable TrackLocToMCPs m_trToMCPLinks;
  };

  inline void MCTruthTool::InitNewEvent() {
    m_mcRichDigitsDone    = false;
    m_mcRichDigitSumsDone = false;
    m_mcRichHitsDone      = false;
    m_mcRichDigSumMapDone = false;
    m_mcRichDigitSums     = nullptr;
    m_mcPToHits.clear();
    m_smartIDsToHits.clear();
    m_mcTrackLinks.reset( nullptr );
    m_mcPhotonLinks.reset( nullptr );
    m_trToMCPLinks.clear();
  }

} // namespace Rich::MC

// namespace
using namespace Rich::MC;

DECLARE_COMPONENT( MCTruthTool )

StatusCode MCTruthTool::initialize() {
  // Sets up various tools and services
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  // Force debug output
  // sc = setProperty( "OutputLevel", MSG::VERBOSE );

  // Retrieve particle property service
  LHCb::IParticlePropertySvc* ppSvc = svc<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );

  // Setup the PDG code mappings
  m_localID[0]                                             = Rich::Unknown;
  m_localID[abs( ppSvc->find( "e+" )->pid().pid() )]       = Rich::Electron;
  m_localID[abs( ppSvc->find( "mu+" )->pid().pid() )]      = Rich::Muon;
  m_localID[abs( ppSvc->find( "pi+" )->pid().pid() )]      = Rich::Pion;
  m_localID[abs( ppSvc->find( "K+" )->pid().pid() )]       = Rich::Kaon;
  m_localID[abs( ppSvc->find( "p+" )->pid().pid() )]       = Rich::Proton;
  m_localID[abs( ppSvc->find( "deuteron" )->pid().pid() )] = Rich::Deuteron;

  // Setup incident services
  incSvc()->addListener( this, IncidentType::BeginEvent );

  // initialise
  m_emptyContainer.clear();

  // Make sure we are ready for a new event
  InitNewEvent();

  // release service since it is no longer needed
  sc = release( ppSvc );

  return sc;
}

// Method that handles various Gaudi "software events"
void MCTruthTool::handle( const Incident& /* incident */ ) {
  // As we only subcribe to one sort of incident, no need to check type
  // if ( IncidentType::BeginEvent == incident.type() ) { InitNewEvent(); }
  InitNewEvent();
}

const LHCb::MCParticle* MCTruthTool::mcParticle( const LHCb::Track* track, const double minWeight ) const {
  // Try with linkers
  auto* linker = trackToMCPLinks( track );
  if ( !linker ) {
    // If get here MC association failed
    _ri_debug << "No MC association available for Tracks" << endmsg;
    return nullptr;
  }
  auto range = linker->weightedRange( track );
  if ( range.empty() ) { range = linker->weightedRange( track->key() ); }
  _ri_debug << "Found " << range.size() << " association(s) for Track " << track->key() << " " << track->type() << " "
            << track->history() << endmsg;
  const LHCb::MCParticle* mcp        = nullptr;
  double                  bestWeight = -1;
  for ( auto const& [mcHit, weight] : range ) {
    _ri_debug << " -> Found MCParticle key=" << mcHit.key() << " weight=" << weight << endmsg;
    if ( weight > minWeight && weight > bestWeight ) {
      bestWeight = weight;
      mcp        = &mcHit;
    }
  }
  return mcp;
}

bool MCTruthTool::mcParticles( const Rich::PDPixelCluster&           cluster,
                               std::vector<const LHCb::MCParticle*>& mcParts ) const {
  mcParts.clear();
  // cluster primary ID
  mcParticles( cluster.primaryID(), mcParts );
  // now add secondary IDs
  for ( const auto& S : cluster.secondaryIDs() ) {
    std::vector<const LHCb::MCParticle*> tmp_vect;
    mcParticles( S, tmp_vect );
    mcParts.reserve( mcParts.size() + tmp_vect.size() );
    for ( const auto* P : tmp_vect ) {
      if ( std::find( mcParts.begin(), mcParts.end(), P ) == mcParts.end() ) { mcParts.push_back( P ); }
    }
  }
  return !mcParts.empty();
}

bool MCTruthTool::mcParticles( const LHCb::RichSmartID id, std::vector<const LHCb::MCParticle*>& mcParts ) const {
  // Clean vector
  mcParts.clear();

  // First try via direct MCParticles references in MCRichDigitSummarys
  const auto iEn = mcRichDigSumMap().find( id );
  if ( iEn != mcRichDigSumMap().end() ) {
    for ( const auto* sum : ( *iEn ).second ) {
      const auto* mcP = sum->mcParticle();
      // protect against null references
      if ( !mcP ) continue;
      // Add to vector, once per MCParticle
      const auto iFind = std::find( mcParts.begin(), mcParts.end(), mcP );
      if ( mcParts.end() == iFind ) mcParts.push_back( mcP );
    }
  }

  // return
  return !mcParts.empty();
}

const LHCb::MCRichDigit* MCTruthTool::mcRichDigit( const LHCb::RichSmartID id ) const {
  _ri_debug << "Locating MCRichDigit for RichSmartID " << id << endmsg;
  const auto* mcDigit = ( mcRichDigits() ? mcRichDigits()->object( id ) : nullptr );
  if ( !mcDigit && msgLevel( MSG::DEBUG ) ) {
    debug() << "Failed to locate MCRichDigit from RichSmartID " << id << endmsg;
  }
  return mcDigit;
}

Rich::ParticleIDType MCTruthTool::mcParticleType( const LHCb::MCParticle* mcPart ) const {
  if ( !mcPart ) return Rich::Unknown;
  const auto pid = m_localID.find( abs( mcPart->particleID().pid() ) );
  return ( pid != m_localID.end() ? pid->second : Rich::Unknown );
}

const LHCb::MCRichTrack* MCTruthTool::mcRichTrack( const LHCb::MCParticle* mcPart ) const {
  return mcTrackLinks()->range( mcPart ).try_front();
}

const LHCb::MCRichOpticalPhoton* MCTruthTool::mcOpticalPhoton( const LHCb::MCRichHit* mcHit ) const {
  const auto* links = mcPhotonLinks();
  return links ? links->range( mcHit ).try_front() : nullptr;
}

bool MCTruthTool::isBackground( const Rich::PDPixelCluster& cluster ) const {
  if ( !isBackground( cluster.primaryID() ) ) return false;
  auto const& ids = cluster.secondaryIDs();
  return std::none_of( ids.begin(), ids.end(), [&]( const LHCb::RichSmartID id ) { return !isBackground( id ); } );
}

bool MCTruthTool::isHPDReflection( const Rich::PDPixelCluster& cluster ) const {
  if ( !isHPDReflection( cluster.primaryID() ) ) return false;
  auto const& ids = cluster.secondaryIDs();
  return std::none_of( ids.begin(), ids.end(), [&]( const LHCb::RichSmartID id ) { return !isHPDReflection( id ); } );
}

bool MCTruthTool::isSiBackScatter( const Rich::PDPixelCluster& cluster ) const {
  if ( !isSiBackScatter( cluster.primaryID() ) ) return false;
  auto const& ids = cluster.secondaryIDs();
  return std::none_of( ids.begin(), ids.end(), [&]( const LHCb::RichSmartID id ) { return !isSiBackScatter( id ); } );
}

bool MCTruthTool::isRadScintillation( const Rich::PDPixelCluster& cluster ) const {
  if ( !isRadScintillation( cluster.primaryID() ) ) return false;
  auto const& ids = cluster.secondaryIDs();
  return std::none_of( ids.begin(), ids.end(),
                       [&]( const LHCb::RichSmartID id ) { return !isRadScintillation( id ); } );
}

bool MCTruthTool::isBackground( const LHCb::RichSmartID id ) const {
  // try via summary objects
  const auto iEn = mcRichDigSumMap().find( id );
  if ( iEn != mcRichDigSumMap().end() ) {
    // loop over summaries
    for ( const auto* sum : ( *iEn ).second ) {
      // returns true if hit is only background (no signal contribution)
      if ( sum->history().hasSignal() ) return false;
    }
  } else if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Failed to find MC history for " << id << endmsg;
  }

  // if all else fails, assume background
  return true;
}

bool MCTruthTool::isHPDReflection( const LHCb::RichSmartID id ) const {
  // try via summary objects
  bool       isSignal( false ), isHPDRefl( false );
  const auto iEn = mcRichDigSumMap().find( id );
  if ( iEn != mcRichDigSumMap().end() ) {
    // loop over summaries
    for ( const auto* sum : ( *iEn ).second ) {
      if ( sum->history().isSignal() ) isSignal = true;
      if ( isSignal ) break;
      if ( sum->history().hpdReflection() ) isHPDRefl = true;
    }
  } else if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Failed to find MC history for " << id << endmsg;
  }

  return !isSignal && isHPDRefl;
}

bool MCTruthTool::isSiBackScatter( const LHCb::RichSmartID id ) const {
  // try via summary objects
  bool       isSignal( false ), isSiRefl( false );
  const auto iEn = mcRichDigSumMap().find( id );
  if ( iEn != mcRichDigSumMap().end() ) {
    // loop over summaries
    for ( const auto* sum : ( *iEn ).second ) {
      if ( sum->history().isSignal() ) isSignal = true;
      if ( isSignal ) break;
      if ( sum->history().hpdSiBackscatter() ) isSiRefl = true;
    }
  } else if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Failed to find MC history for " << id << endmsg;
  }

  return ( !isSignal && isSiRefl );
}

bool MCTruthTool::isRadScintillation( const LHCb::RichSmartID id ) const {
  // try via summary objects
  bool       isSignal( false ), isRadScint( false );
  const auto iEn = mcRichDigSumMap().find( id );
  if ( iEn != mcRichDigSumMap().end() ) {
    // loop over summaries
    for ( const auto* sum : ( *iEn ).second ) {
      if ( sum->history().isSignal() ) isSignal = true;
      if ( isSignal ) break;
      if ( sum->history().radScintillation() ) isRadScint = true;
    }
  } else if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Failed to find MC history for " << id << endmsg;
  }

  return ( !isSignal && isRadScint );
}

bool MCTruthTool::isCherenkovRadiation( const Rich::PDPixelCluster& cluster, const Rich::RadiatorType rad ) const {
  return ( isCherenkovRadiation( cluster.primaryID(), rad ) ||
           std::any_of( cluster.secondaryIDs().begin(), cluster.secondaryIDs().end(),
                        [&rad, this]( const auto& S ) { return this->isCherenkovRadiation( S, rad ); } ) );
}

bool MCTruthTool::isCherenkovRadiation( const LHCb::RichSmartID id, const Rich::RadiatorType rad ) const {

  // first, try via summary objects
  const auto iEn = mcRichDigSumMap().find( id );
  if ( iEn != mcRichDigSumMap().end() ) {
    return std::any_of( ( *iEn ).second.begin(), ( *iEn ).second.end(), [&rad]( const auto* sum ) {
      const auto& code = sum->history();
      return ( ( Rich::Aerogel == rad && code.aerogelHit() ) || ( Rich::C4F10 == rad && code.c4f10Hit() ) ||
               ( Rich::CF4 == rad && code.cf4Hit() ) );
    } );
  }

  // finally, assume not
  return false;
}

bool MCTruthTool::getMcHistories( const Rich::PDPixelCluster&                   cluster,
                                  std::vector<const LHCb::MCRichDigitSummary*>& histories ) const {
  bool OK = false;
  // primary ID
  getMcHistories( cluster.primaryID(), histories );
  // add secondary IDs
  for ( const auto& S : cluster.secondaryIDs() ) {
    std::vector<const LHCb::MCRichDigitSummary*> tmp_hists;
    OK = ( getMcHistories( S, tmp_hists ) || OK );
    for ( const auto* sum : tmp_hists ) { histories.push_back( sum ); }
  }
  return OK;
}

bool MCTruthTool::getMcHistories( const LHCb::RichSmartID                       id,
                                  std::vector<const LHCb::MCRichDigitSummary*>& histories ) const {
  // clear histories
  histories.clear();
  if ( mcRichDigitSummaries() ) {
    // try to find summaries
    const auto iEn = mcRichDigSumMap().find( id );
    if ( iEn != mcRichDigSumMap().end() ) {
      // set histories
      histories = ( *iEn ).second;
    }
  }
  // return if any were found
  return !histories.empty();
}

const LHCb::MCRichDigits* MCTruthTool::mcRichDigits() const {
  if ( !m_mcRichDigitsDone ) {
    m_mcRichDigitsDone = true;
    // try and load MCRichDigits
    m_mcRichDigits = getIfExists<LHCb::MCRichDigits>( m_mcRichDigitsLocation );
    if ( m_mcRichDigits ) {
      _ri_debug << "Successfully located " << m_mcRichDigits->size() << " MCRichDigits at " << m_mcRichDigitsLocation
                << endmsg;
    } else {
      _ri_debug << "Failed to locate MCRichDigits at " << m_mcRichDigitsLocation << endmsg;
    }
  }

  return m_mcRichDigits;
}

const LHCb::MCRichDigitSummarys* MCTruthTool::mcRichDigitSummaries() const {
  if ( !m_mcRichDigitSumsDone ) {
    m_mcRichDigitSumsDone = true;
    // try and load MCRichDigitSummarys
    m_mcRichDigitSums = getIfExists<LHCb::MCRichDigitSummarys>( m_mcRichDigitSumsLocation );
    if ( m_mcRichDigitSums ) {
      _ri_debug << "Successfully located " << m_mcRichDigitSums->size() << " MCRichDigitSummaries at "
                << m_mcRichDigitSumsLocation << endmsg;
    } else {
      _ri_debug << "Failed to locate MCRichDigitSummaries at " << m_mcRichDigitSumsLocation << endmsg;
    }
  }
  return m_mcRichDigitSums;
}

const MCTruthTool::MCRichDigitSummaryMap& MCTruthTool::mcRichDigSumMap() const {
  if ( !m_mcRichDigSumMapDone ) {
    m_mcRichDigSumMapDone = true;

    // clear current map
    m_mcRichDigSumMap.clear();

    // loop over summaries
    if ( mcRichDigitSummaries() ) {
      for ( const auto* sum : *mcRichDigitSummaries() ) { m_mcRichDigSumMap[sum->richSmartID()].push_back( sum ); }
    }

    _ri_debug << "Built RichSmartID->MCRichDigitSummary Map for " << m_mcRichDigSumMap.size() << " RichSmartIDs"
              << endmsg;
  }
  return m_mcRichDigSumMap;
}

const LHCb::MCRichHits* MCTruthTool::mcRichHits() const {
  if ( !m_mcRichHitsDone ) {
    m_mcRichHitsDone = true;

    // Try and load MC Rich Hits
    m_mcRichHits = getIfExists<LHCb::MCRichHits>( m_mcRichHitsLocation );
    if ( m_mcRichHits ) {
      _ri_debug << "Successfully located " << m_mcRichHits->size() << " MCRichHits at " << m_mcRichHitsLocation
                << endmsg;
    } else {
      _ri_debug << "Failed to locate MCRichHits at " << m_mcRichHitsLocation << endmsg;
    }
  }

  return m_mcRichHits;
}

MCTruthTool::MCRichHitToPhoton* MCTruthTool::mcPhotonLinks() const {
  if ( !m_mcPhotonLinks.get() ) {
    auto loc   = LHCb::LinksByKey::linkerName( LHCb::MCRichHitLocation::Default + "2MCRichOpticalPhotons" );
    auto links = getIfExists<LHCb::LinksByKey>( loc );
    if ( !links ) {
      _ri_debug << "Linker for MCRichHits to MCRichOpticalPhotons not found at '" << loc << "'" << endmsg;
    }
    m_mcPhotonLinks.reset( new MCRichHitToPhoton( links ) );
  }
  return m_mcPhotonLinks.get();
}

MCTruthTool::MCPartToRichTracks* MCTruthTool::mcTrackLinks() const {
  if ( !m_mcTrackLinks.get() ) {
    const auto loc   = LHCb::LinksByKey::linkerName( LHCb::MCParticleLocation::Default + "2MCRichTracks" );
    auto       links = getIfExists<LHCb::LinksByKey>( loc );
    if ( !links ) { _ri_debug << "Linker for MCParticles to MCRichTracks not found at '" << loc << "'" << endmsg; }
    m_mcTrackLinks.reset( new MCPartToRichTracks( links ) );
  }
  return m_mcTrackLinks.get();
}

const MCTruthTool::MCPartToMCRichHits& MCTruthTool::mcPartToMCRichHitsMap() const {
  if ( m_mcPToHits.empty() ) {
    // loop over all MCRichHits
    // only using signal event here for the time being. Should add spillovers sometime ...
    if ( mcRichHits() ) {
      for ( const auto* hit : *mcRichHits() ) {
        const auto* mcP = hit->mcParticle();
        if ( mcP ) {
          _ri_verbo << "Adding MCRichHit to list for MCParticle " << mcP->key() << endmsg;
          m_mcPToHits[mcP].push_back( hit );
        }
      }
      _ri_debug << "Built MCParticle->MCRichHit Map for " << m_mcPToHits.size() << " RichSmartIDs" << endmsg;
    }
  }
  return m_mcPToHits;
}

const MCTruthTool::SmartIDToMCRichHits& MCTruthTool::smartIDToMCRichHitsMap() const {
  if ( m_smartIDsToHits.empty() ) {
    // loop over all MCRichHits
    // only using signal event here for the time being. Should add spillovers sometime
    if ( mcRichHits() ) {
      for ( const auto* hit : *mcRichHits() ) {
        // For the moment, strip sub-pixel information
        // in the longer term, should do something more clever
        const auto pixelID = hit->sensDetID().pixelID();
        _ri_verbo << "Adding MCRichHit to list for Pixel ID " << pixelID << endmsg;
        m_smartIDsToHits[pixelID].push_back( hit );
        // Add another entry for the PD
        const auto hpdID = pixelID.pdID();
        _ri_verbo << "Adding MCRichHit to list for PD ID " << hpdID << endmsg;
        m_smartIDsToHits[hpdID].push_back( hit );
      }
      _ri_debug << "Built RichSmartID->MCRichHit Map for " << m_smartIDsToHits.size() << " RichSmartIDs" << endmsg;
    }
  }
  return m_smartIDsToHits;
}

const SmartRefVector<LHCb::MCRichHit>& MCTruthTool::mcRichHits( const LHCb::MCParticle* mcp ) const {
  const auto i = mcPartToMCRichHitsMap().find( mcp );
  return ( i != mcPartToMCRichHitsMap().end() ? ( *i ).second : m_emptyContainer );
}

void MCTruthTool::mcRichHits( const Rich::PDPixelCluster& cluster, SmartRefVector<LHCb::MCRichHit>& hits ) const {
  // primary ID
  hits = mcRichHits( cluster.primaryID() );
  // add secondary IDs
  for ( const auto& S : cluster.secondaryIDs() ) {
    const auto& chits = mcRichHits( S );
    hits.reserve( hits.size() + chits.size() );
    for ( const auto& H : chits ) { hits.push_back( H ); }
  }
}

const SmartRefVector<LHCb::MCRichHit>& MCTruthTool::mcRichHits( const LHCb::RichSmartID smartID ) const {
  // try working backwards
  const auto i = smartIDToMCRichHitsMap().find( smartID );
  if ( i != smartIDToMCRichHitsMap().end() ) {
    _ri_verbo << "Found " << ( *i ).second.size() << " MCRichHits for PixelID " << smartID << endmsg;
    return ( *i ).second;
  }

  // MC association failed
  _ri_debug << "Failed to find MCRichHits associated to RichSmartID " << smartID << endmsg;
  return m_emptyContainer;
}

bool MCTruthTool::richMCHistoryAvailable() const { return ( nullptr != mcRichDigitSummaries() ); }

bool MCTruthTool::extendedMCAvailable() const { return ( nullptr != mcPhotonLinks() && nullptr != mcTrackLinks() ); }

const MCTruthTool::TrackToMCP* MCTruthTool::trackToMCPLinks( const LHCb::Track* track ) const {
  if ( !track ) return nullptr;

  // TES location for the container for this track
  const auto& loc = ( track->parent() ? objectLocation( track->parent() ) : m_trLoc.value() );
  _ri_verbo << "trackToMCPLinks for Track " << track->key() << " at TES '" << loc << "'" << endmsg;

  // See if linker has already been loaded for this TES location
  const auto iLinks = m_trToMCPLinks.find( loc );
  if ( m_trToMCPLinks.end() != iLinks ) {
    // return found linker
    return &iLinks->second;
  }
  _ri_verbo << " -> failed to find linker for this location. Attempting to load one" << endmsg;

  // try and load a new linker for this location
  auto* linker = trackToMCPLinks( loc );
  if ( !linker && loc != m_trLoc.value() ) {
    _ri_verbo << "  -> No linker available for Track TES container. Returning default at " << m_trLoc.value() << endmsg;
    linker = trackToMCPLinks( m_trLoc.value() );
  }

  // return this new linker
  return linker;
}

const MCTruthTool::TrackToMCP* MCTruthTool::trackToMCPLinks( const std::string& loc ) const {
  // See if linker has already been loaded for this TES location
  const auto iLinks = m_trToMCPLinks.find( loc );
  if ( m_trToMCPLinks.end() != iLinks ) {
    // return found linker
    return &iLinks->second;
  }
  _ri_verbo << " -> Attempting to load TrackToMCP Linker for '" << loc << "'" << endmsg;

  auto links = SmartDataPtr<LHCb::LinksByKey>{evtSvc(), LHCb::LinksByKey::linkerName( loc )};
  if ( !links ) {
    _ri_debug << "  -> Linker for Tracks to MCParticles not found" << endmsg;
    return nullptr;
  }

  // Save this linker in the map
  auto [linker, ok] = m_trToMCPLinks.emplace( loc, links );

  _ri_debug << "  -> Found OK=" << ok << " SIZE=" << linker->second.size() << " Track to MCParticle associations"
            << endmsg;

  // return this new linker
  return &linker->second;
}
