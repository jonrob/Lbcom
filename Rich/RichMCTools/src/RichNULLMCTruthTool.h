/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

//-----------------------------------------------------------------------------
/** @file RichNULLMCTruthTool.h
 *
 *  Header file for tool : Rich::MC::NULLMCTruthTool
 *
 *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
 *  @date   15/03/2002
 */
//-----------------------------------------------------------------------------

#pragma once

// base class
#include "RichKernel/RichToolBase.h"

// Kernel
#include "Kernel/RichParticleIDType.h"

// RICH Utils
#include "RichUtils/RichMap.h"
#include "RichUtils/RichPixelCluster.h"

// Event model
#include "Event/MCParticle.h"
#include "Event/MCRichDigit.h"
#include "Event/MCRichDigitSummary.h"
#include "Event/MCRichHit.h"
#include "Event/MCRichOpticalPhoton.h"
#include "Event/MCRichSegment.h"
#include "Event/MCRichTrack.h"
#include "Event/MCTruth.h"
#include "Event/Track.h"
#include "Kernel/RichParticleIDType.h"

// Interfaces
#include "MCInterfaces/IRichMCTruthTool.h"

namespace Rich::MC {

  //-----------------------------------------------------------------------------
  /** @class NULLMCTruthTool RichNULLMCTruthTool.h
   *
   *  NULL MC truth tool. Always returns no associations.
   *
   *  @author Chris Jones   Christopher.Rob.Jones@cern.ch
   *  @date   15/03/2002
   */
  //-----------------------------------------------------------------------------

  class NULLMCTruthTool final : public extends<Rich::ToolBase, IMCTruthTool> {

  public: // Methods for Gaudi Framework
    /// Standard constructor
    using extends::extends;

  public: // methods (and doxygen comments) inherited from interface
    // Find best MCParticle association for a given reconstructed Track
    const LHCb::MCParticle* mcParticle( const LHCb::Track* track, const double minWeight = 0.5 ) const override;

    // get MCRichHits for MCParticle
    const SmartRefVector<LHCb::MCRichHit>& mcRichHits( const LHCb::MCParticle* mcp ) const override;

    // Get the MCRichHits associated to a given RichSmartID
    const SmartRefVector<LHCb::MCRichHit>& mcRichHits( const LHCb::RichSmartID smartID ) const override;

    // Get a vector of MCParticles associated to given RichSmartID
    bool mcParticles( const LHCb::RichSmartID id, std::vector<const LHCb::MCParticle*>& mcParts ) const override;

    // Determines the particle mass hypothesis for a given MCParticle
    Rich::ParticleIDType mcParticleType( const LHCb::MCParticle* mcPart ) const override;

    // Finds the MCRichDigit association for a RichSmartID channel identifier
    const LHCb::MCRichDigit* mcRichDigit( const LHCb::RichSmartID id ) const override;

    // Finds the MCRichTrack associated to a given MCParticle
    const LHCb::MCRichTrack* mcRichTrack( const LHCb::MCParticle* mcPart ) const override;

    // Finds the MCRichOpticalPhoton associated to a given MCRichHit
    const LHCb::MCRichOpticalPhoton* mcOpticalPhoton( const LHCb::MCRichHit* mcHit ) const override;

    // Access the bit-pack history object for the given RichSmartID
    bool getMcHistories( const LHCb::RichSmartID                       id,
                         std::vector<const LHCb::MCRichDigitSummary*>& histories ) const override;

    // Checks if the given RichSmartID is the result of a background hit
    bool isBackground( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of a photon which
    // underwent reflections inside the PD
    bool isHPDReflection( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of backscattering
    // of the HPD silicon sensor
    bool isSiBackScatter( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of scintillation
    // of the radiator
    bool isRadScintillation( const LHCb::RichSmartID id ) const override;

    // Checks if the given RichSmartID is the result of true Cherenkov
    bool isCherenkovRadiation( const LHCb::RichSmartID id, const Rich::RadiatorType rad ) const override;

    // Returns true if MC information for the RICH hits are available
    bool richMCHistoryAvailable() const override;

    // Checks if RICH extended MC information (MCRichOpticalPhoton, MCRichSegment etc.)
    bool extendedMCAvailable() const override;

    // new ones

    // Get the MCRichHits associated to a cluster of RichSmartIDs
    void mcRichHits( const Rich::PDPixelCluster& cluster, SmartRefVector<LHCb::MCRichHit>& hits ) const override;

    // Access the bit-pack history objects for the given cluster of RichSmartIDs
    bool getMcHistories( const Rich::PDPixelCluster&                   cluster,
                         std::vector<const LHCb::MCRichDigitSummary*>& histories ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of a background
    bool isBackground( const Rich::PDPixelCluster& cluster ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of a photon which
    // underwent reflections inside the PD
    bool isHPDReflection( const Rich::PDPixelCluster& cluster ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of backscattering
    // of the PD silicon sensor
    bool isSiBackScatter( const Rich::PDPixelCluster& cluster ) const override;

    // Checks if the given cluster of RichSmartIDs is the result of scintillation
    // of the radiator
    bool isRadScintillation( const Rich::PDPixelCluster& cluster ) const override;

    // Checks if the given RichSmartID is the result of true Cherenkov
    bool isCherenkovRadiation( const Rich::PDPixelCluster& cluster, const Rich::RadiatorType rad ) const override;

    // Get a vector of MCParticles associated to given RichSmartID cluster
    bool mcParticles( const Rich::PDPixelCluster&           cluster,
                      std::vector<const LHCb::MCParticle*>& mcParts ) const override;
  };

} // namespace Rich::MC
