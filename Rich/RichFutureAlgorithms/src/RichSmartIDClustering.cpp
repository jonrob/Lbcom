/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

// Gaudi ( must be first ...)
#include "GaudiKernel/ParsersFactory.h"
#include "GaudiKernel/StdArrayAsProperty.h"

// Rich (Future) Kernel
#include "RichFutureKernel/RichAlgBase.h"

// Gaudi Functional
#include "LHCbAlgs/Transformer.h"

// DetDesc
#include "DetDesc/GenericConditionAccessorHolder.h"

// Rich Utils
#include "RichFutureUtils/RichDecodedData.h"
#include "RichUtils/RichPixelCluster.h"
#include "RichUtils/RichSmartIDSorter.h"

// Detectors
#include "RichDetectors/RichPDInfo.h"

// STL
#include <algorithm>
#include <array>
#include <functional>
#include <limits>
#include <memory>
#include <string>
#include <vector>

namespace Rich::Future {

  namespace {
    /// Output data
    using OutData = Rich::PDPixelCluster::Vector;

    /// Sort the RichSmartIDs into the correct order for this algorithm
    inline void sortIDs( LHCb::RichSmartID::Vector& smartIDs ) {
      if ( !smartIDs.empty() ) {
        // Use general sorter for PDs
        SmartIDSorter::sortByRegion( smartIDs );
      }
    }
  } // namespace

  /** @class SmartIDClustering
   *
   *  Creates Rich PD cluster objects from the decoded RICH raw event
   *
   *  @author Chris Jones
   *  @date   2016-09-27
   */
  class SmartIDClustering final
      : public LHCb::Algorithm::Transformer<OutData( const DAQ::DecodedData&, const Detector::PDInfo& ),
                                            LHCb::DetDesc::usesBaseAndConditions<AlgBase<>, Detector::PDInfo>> {

  public:
    /// Standard constructor
    SmartIDClustering( const std::string& name, ISvcLocator* pSvcLocator )
        : Transformer( name, pSvcLocator,
                       // Input decoded data
                       {KeyValue{"DecodedDataLocation", DAQ::DecodedDataLocation::Default},
                        // conditions input
                        KeyValue{"RichPDInfo", Detector::PDInfo::DefaultConditionKey}},
                       // output pixel cluster objects
                       {KeyValue{"RichPixelClustersLocation", Rich::PDPixelClusterLocation::Default}} ) {}

    /// Initialize
    StatusCode initialize() override {
      return Transformer::initialize().andThen( [&] {
        // declare dependency on PD info derived condition
        Detector::PDInfo::addConditionDerivation( this );
        // loop over RICHes
        for ( const auto rich : {Rich::Rich1, Rich::Rich2} ) {
          if ( m_usedDets[rich] ) {
            if ( m_enable4D[rich] ) {
              info() << "Hit times enabled for " << rich << " | Window = "   //
                     << ( m_avHitTime[rich] - m_timeWindow[rich] ) << " to " //
                     << ( m_avHitTime[rich] + m_timeWindow[rich] ) << " ns" << endmsg;
            }
            if ( m_overallMax[rich] < LHCb::RichSmartID::MaPMT::TotalPixels ) {
              info() << "Maximum PD occupancy for " << rich << " = " << m_overallMax[rich] << endmsg;
            }
            m_runClusterSplit[rich] =
                ( m_minClusSize[rich] > 1 || m_maxClusSize[rich] < LHCb::RichSmartID::MaPMT::TotalPixels );
          } else {
            info() << "Pixels for " << rich << " are disabled" << endmsg;
          }
        }
        // force enable debug/verbose messages
        // return setProperty( "OutputLevel", MSG::VERBOSE );
      } );
    }

    /// Algorithm execution via transform
    OutData operator()( const DAQ::DecodedData& data, //
                        const Detector::PDInfo& pdInfo ) const override;

  private:
    /** Check the status of the given RICH PD identifier (RichSmartID)
     *
     *  @param id The PD RichSmartID to check
     *
     *  @return boolean indicating if the given channel is active
     *  @retval true  PD is active
     *  @retval false PD is not in use
     */
    inline bool pdIsOK( const LHCb::RichSmartID id, //
                        const Detector::PDInfo& pdInfo ) const {
      return ( id.isValid() &&                           // Valid PD ID
               m_usedDets[id.rich()] &&                  // This RICH is in use
               ( !m_pdCheck || pdInfo.pdIsActive( id ) ) // If required, check PD is alive
      );
    }

  private:
    // properties

    /// Flags for which RICH detectors to create pixels for
    Gaudi::Property<DetectorArray<bool>> m_usedDets{this, "Detectors", {true, true}};

    /// Flag to turn on or off the explicit checking of the PD status
    Gaudi::Property<bool> m_pdCheck{this, "CheckPDsAreActive", false};

    /** Turn on/off the clustering for each RICH.
     *  If off, a single cluster is made from each channel ID */
    Gaudi::Property<DetectorArray<bool>> m_clusterHits{this, "ApplyPixelClustering", {false, false}};

    /// Minimum cluster size
    Gaudi::Property<DetectorArray<unsigned int>> m_minClusSize{this, "MinClusterSize", {1u, 1u}};

    /// Maximum cluster size
    Gaudi::Property<DetectorArray<unsigned int>> m_maxClusSize{
        this, "MaxClusterSize", {LHCb::RichSmartID::MaPMT::TotalPixels, LHCb::RichSmartID::MaPMT::TotalPixels}};

    /// Allow pixels to be clustered across diagonals
    Gaudi::Property<DetectorArray<bool>> m_allowDiags{this, "AllowDiagonalsInClusters", {false, false}};

    /// Max pixels tracks GEC
    Gaudi::Property<unsigned int> m_maxClusters{this, "MaxClusters", std::numeric_limits<unsigned int>::max()};

    /// Absolute max PD occupancy. If exceeded PD is rejected.
    Gaudi::Property<DetectorArray<std::size_t>> m_overallMax{this, "AbsoluteMaxPDOcc", {99999u, 99999u}};

    /// Sort PD data within a RICH panel
    Gaudi::Property<bool> m_sortPanels{this, "SortPDsInPanels", false};

    // Options for 4D reco

    /// Enabled 4D reconstruction
    Gaudi::Property<DetectorArray<bool>> m_enable4D{this, "Enable4D", {false, false}};

    /// Average expected hit time for signal in each RICH
    Gaudi::Property<DetectorArray<float>> m_avHitTime{
        this, "AvHitTime", {13.03 * Gaudi::Units::ns, 52.94 * Gaudi::Units::ns}};

    /// Time window for each RICH
    Gaudi::Property<DetectorArray<float>> m_timeWindow{
        this, "TimeWindow", {3.0 * Gaudi::Units::ns, 3.0 * Gaudi::Units::ns}};

  private:
    // cached data

    /// Is cluster splitting enabled
    DetectorArray<bool> m_runClusterSplit = {{false, false}};

  private:
    // counters

    /// PD rejected
    mutable DetectorArray<Gaudi::Accumulators::BinomialCounter<>> m_rejPD{
        {{this, "RICH1 PD rejected"}, {this, "RICH2 PD rejected"}}};

  private:
    // messaging

    /// maximum cluster abort
    mutable WarningCounter m_maxClusWarn{this, "Too many clusters. Processing aborted.", 3};

    /// Null PD pointer
    mutable DebugCounter m_nullPD{this, "No DePD object found for decoded channel ID"};
  };

} // namespace Rich::Future

using namespace Rich::Future;

//=============================================================================

OutData SmartIDClustering::operator()( const DAQ::DecodedData& data, //
                                       const Detector::PDInfo& pdInfo ) const {

  // The clusters to fill and return
  OutData clusters;
  clusters.reserve( data.nTotalHits() );

  // cluster builder. Allocated as required.
  std::unique_ptr<PDPixelClustersBuilder> clusterBuilder;

  // Working container of pointers to decoded HPD data objects
  std::vector<const DAQ::PDInfo*> pdDataV;
  pdDataV.reserve( data.nActivePDs() );

  // Vector for sorted IDs, used later on.
  // Defined here so can be shared across different PDs.
  LHCb::RichSmartID::Vector smartIDs;

  // Loop over the raw RICH data
  // Note the data is implicitly sorted by RICH and side

  // local counter buffers
  auto rejPD = std::array{m_rejPD[Rich::Rich1].buffer(), m_rejPD[Rich::Rich2].buffer()};

  // First, RICH detectors
  for ( const auto& rD : data ) {
    // sides per RICH
    for ( const auto& pD : rD ) {

      // clear PD pointer cache
      pdDataV.clear();

      // Get the PD pointers for this RICH and side
      // PD modules per side
      for ( const auto& mD : pD ) {
        // PDs per module
        for ( const auto& pd : mD ) {
          // Which RICH
          const auto rich = pd.pdID().rich();
          // The list of hits
          const auto& IDs = pd.smartIDs();
          // Sanity checks on the PDID
          if ( pdIsOK( pd.pdID(), pdInfo ) && !IDs.empty() ) {
            const bool usePD = ( IDs.size() <= m_overallMax[rich] );
            // only fill counter if PD occ cut is effectively active
            if ( m_overallMax[rich] < LHCb::RichSmartID::MaPMT::TotalPixels ) { rejPD[rich] += !usePD; }
            if ( usePD ) {
              pdDataV.emplace_back( &pd );
            } else {
              _ri_verbo << "Rejected " << pd.pdID() << endmsg;
            }
          }
        }
      }

      // Sort PD data pointer vector by region.
      // Faster than sorting final cluster container
      if ( m_sortPanels ) {
        std::sort( pdDataV.begin(), pdDataV.end(), //
                   []( const auto a, const auto b ) {
                     // Note dataBitsOnly() needed to exclude large PMT flag as this is in the high bits
                     return ( a->pdID().dataBitsOnly().key() < b->pdID().dataBitsOnly().key() );
                   } );
      }

      _ri_verbo << "Selected " << pdDataV.size() << " PDs" << endmsg;

      // Loop over the PD vector and cluster
      for ( const auto PD : pdDataV ) {

        // Which RICH
        const auto rich = PD->pdID().rich();

        // Which panel
        const auto panel = PD->pdID().panel();

        // Get the DePD pointer for this PD
        const auto dePD = pdInfo.dePD( PD->pdID() );
        if ( !dePD ) {
          // No DePD found... Issue warning and skip.
          ++m_nullPD;
          _ri_verbo << "Failed to find DePD for " << PD->pdID() << endmsg;
          continue;
        }

        // Apply 4D time window
        auto timeSel = [&]( const auto id ) {
          bool ok = true; // default to selected if 4D disabled or time info not available
          if ( m_enable4D[rich] && id.adcTimeIsSet() ) {
            ok = ( fabs( id.time() - m_avHitTime[rich] ) <= m_timeWindow[rich] );
          }
          if ( !ok ) { _ri_verbo << "Rejected " << id << endmsg; }
          return ok;
        };

        // is cluster finding active and required (i.e. more than 1 hit) ?
        if ( !m_clusterHits[rich] || PD->smartIDs().size() < 2 ) {

          // just loop over the raw RichSmartIDs and make a 'cluster' for each
          _ri_verbo << "Making " << PD->smartIDs().size() << " hits for " << PD->pdID() << endmsg;
          for ( const auto& ID : PD->smartIDs() ) {
            if ( timeSel( ID ) ) { clusters.emplace_back( rich, panel, ID, dePD ); }
          }

        } else {

          // perform clustering on the pixels in this PD

          // Need to make a copy of the hits, as need to sort them and
          // we cannot change the raw data.
          smartIDs = LHCb::RichSmartID::Vector( PD->smartIDs().begin(), PD->smartIDs().end() );

          // make sure pixels are sorted ok
          // this should be automatic via decoding but can do it here to be sure
          // as because the vector should be in the correct order, it should be v fast
          sortIDs( smartIDs );

          // initialise the builder
          if ( !clusterBuilder.get() ) {
            // Do we need a PMT or HPD builder ?
            if ( LHCb::RichSmartID::IDType::MaPMTID == PD->pdID().idType() ) {
              clusterBuilder = std::make_unique<PMTPixelClustersBuilder>();
            } else {
              clusterBuilder = std::make_unique<HPDPixelClustersBuilder>();
            }
          }
          clusterBuilder->initialise( smartIDs );

          // loop over pixels
          // requires them to be sorted by row then column
          _ri_verbo << "Clustering with " << smartIDs.size() << " RichSmartIDs" << endmsg;
          for ( const auto& S : smartIDs ) {

            // time sel
            if ( !timeSel( S ) ) { continue; }

            // Print the input hits
            _ri_verbo << " -> " << S << endmsg;

            // get row and column data
            const auto col     = clusterBuilder->colNumber( S );
            const auto row     = clusterBuilder->rowNumber( S );
            const auto lastrow = row - 1;
            const auto lastcol = col - 1;
            const auto nextcol = col + 1;

            // Null cluster pointer
            PDPixelClusters::Cluster* clus( nullptr );

            // check neighbouring pixels

            // last row and last column
            clus = ( m_allowDiags[rich] ? clusterBuilder->getCluster( lastrow, lastcol ) : nullptr );

            // last row and same column
            {
              auto newclus1 = clusterBuilder->getCluster( lastrow, col );
              if ( newclus1 && newclus1->size() < m_maxClusSize[rich] ) {
                clus = ( clus && clus != newclus1 && ( newclus1->size() + clus->size() ) < m_maxClusSize[rich]
                             ? clusterBuilder->mergeClusters( clus, newclus1 )
                             : newclus1 );
              }
            }

            // last row and next column
            {
              auto newclus2 = ( m_allowDiags[rich] ? clusterBuilder->getCluster( lastrow, nextcol ) : nullptr );
              if ( newclus2 && newclus2->size() < m_maxClusSize[rich] ) {
                clus = ( clus && clus != newclus2 && ( newclus2->size() + clus->size() ) < m_maxClusSize[rich]
                             ? clusterBuilder->mergeClusters( clus, newclus2 )
                             : newclus2 );
              }
            }

            // this row and last column
            {
              auto newclus3 = clusterBuilder->getCluster( row, lastcol );
              if ( newclus3 && newclus3->size() < m_maxClusSize[rich] ) {
                clus = ( clus && clus != newclus3 && ( newclus3->size() + clus->size() ) < m_maxClusSize[rich]
                             ? clusterBuilder->mergeClusters( clus, newclus3 )
                             : newclus3 );
              }
            }

            // Did we find a neighbouring pixel cluster ?
            // If not, this is a new cluster
            if ( !clus ) { clus = clusterBuilder->createNewCluster(); }

            // assign final cluster to this pixel
            clusterBuilder->setCluster( S, row, col, clus );

          } // pixel loop

          // the pixel data object filled by the builder
          auto& pixelData = clusterBuilder->data();

          // Do we need to break up clusters ?
          if ( m_runClusterSplit[rich] ) {
            // make a local working object
            PDPixelClusters::Cluster::PtnVector clustersToSplit;
            for ( auto& C : pixelData.clusters() ) {
              if ( C.size() < m_minClusSize[rich] || //
                   C.size() > m_maxClusSize[rich] ) {
                clustersToSplit.push_back( &C );
              }
            }
            if ( !clustersToSplit.empty() ) {
              // split the selected clusters
              clusterBuilder->splitClusters( clustersToSplit );
            }
          }

          // Print the cluster results
          _ri_verbo << *( clusterBuilder.get() ) << endmsg;

          // Note : Using move from here on to move the data from the builder

          // loop over the final clusters and save them
          for ( auto& clus : pixelData.clusters() ) {
            // Skip empty clusters
            if ( !clus.pixels().empty() ) {
              if ( clus.pixels().secondaryIDs().empty() ) {
                // If we have no secondary pixels, use optimised constructor.
                clusters.emplace_back( rich, panel, clus.pixels().primaryID(), dePD );
              } else {
                // Use the move constructor
                clusters.emplace_back( std::move( clus.pixels() ) );
                clusters.back().setDePD( dePD );
              }
            }
          }

        } // run clustering

      } // Selected PD loop

    } // panels
  }   // RICHes

  // GEC cut
  if ( clusters.size() > m_maxClusters ) {
    // Too many hits. Clear to prevent event processing.
    clusters.clear();
    ++m_maxClusWarn;
  }

  // return the found clusters
  return clusters;
}

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( SmartIDClustering )
