#!/usr/bin/env gaudirun.py
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# =============================================================================
## Confurable for Calorimeter MC-truth associations
#  @author Vanya BELYAEV Ivan.Belyaev@nikhe.nl
#  @date 2008-07-17
# =============================================================================
"""
Configurable for Calorimeter MC-truth associations
"""
from __future__ import print_function
# =============================================================================
__author__ = "Vanya BELYAEV Ivan.Belyaev@nikhef.nl"
__version__ = "1.1"
# =============================================================================
__all__ = ('CaloAssociatorsConf', )
# =============================================================================
from LHCbKernel.Configuration import *

from CaloKernel.ConfUtils import (printOnDemand, setTheProperty)

import logging
_log = logging.getLogger('CaloAssociators')


# =============================================================================
## @class CaloAssociatorsConf
#  Configurable for Calorimeter MC-truth associators
#  @author Vanya BELYAEV Ivan.Belyaev@nikhef.nl
#  @date 2008-07-17
class CaloAssociatorsConf(LHCbConfigurableUser):
    """
    Configurable for Calorimeter MC-truth associators
    """
    __slots__ = {
        ##
        "Context": "Offline"  # The context to run
        ,
        "MeasureTime": True  # Measure the time for sequencers
        ,
        "OutputLevel": INFO  # The global output level
        ##
        ,
        'Sequence': None  # The sequencer to add the CALO Associations
        ,
        'EnableMCOnDemand': True  # Enable Digits-On-Demand
        ,
        'Digits': 'Ecal'
        ##
    }
    ## documentation lines
    _propertyDocDct = {
        ##
        "Context": """ The context to run """,
        "MeasureTime": """ Measure the time for sequencers """,
        "OutputLevel": """ The global output level """
        ##
        ,
        'Sequence': """ The sequencer to add the CALO Associations """,
        'EnableMCOnDemand': """ Enable MC-On-Demand """,
        'Digits': """ Allowed sub-detector"""
        ##
    }

    # the basic definition of MC-truth
    def mcTruth(self):
        """
        The basic definition of MC-truth
        """
        from CaloAssociators.MCTruthOnDemand import caloMCTruth
        main = caloMCTruth(
            self.getProp('Context'), self.getProp('EnableMCOnDemand'),
            self.getProp('Digits'))
        setTheProperty(main, 'MeasureTime', self.getProp('MeasureTime'))
        if self.isPropertySet("OutputLevel"):
            setTheProperty(main, 'OutputLevel', self.getProp('OutputLevel'))

        if self.isPropertySet('Sequence'):
            CaloDigitsConf(
                Context=self.getProp('Context'),
                MeasureTime=self.getProp('MeasureTime'),
                OutputLevel=self.getProp('OutputLevel'),
                EnableDigitsOnDemand=self.getProp('EnableMCOnDemand'),
                Sequence=self.getProp('Sequence'),
                Digits=self.getProp('Digits'))
            sequence.Members += [main]

        return main

    # actualy apply the configuration
    def applyConf(self):
        """
        Apply the configuration
        """
        _log.info('Apply CaloAssociators Configuration')
        _log.info(self)

        self.mcTruth()

        if self.getProp('EnableMCOnDemand'):
            _log.info(printOnDemand())


# =============================================================================
if '__main__' == __name__:
    print(__doc__)
    print(__author__)
    print(__version__)

# =============================================================================
# The END
# =============================================================================
