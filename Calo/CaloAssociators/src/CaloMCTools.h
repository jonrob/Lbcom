/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once
#include "CaloFutureUtils/CaloFutureMCTools.h"

namespace CaloMCTools {
  using CaloFutureMCTools::EnergyFromMCParticle;
  using CaloFutureMCTools::energyFromMCParticle;
  using CaloFutureMCTools::FromMCParticle;
  using CaloFutureMCTools::fromMCParticle;
  using CaloFutureMCTools::isParent;
  using CaloFutureMCTools::LargestDeposition;
  using CaloFutureMCTools::largestDeposition;
  using CaloFutureMCTools::ParticlePair;
} // namespace CaloMCTools

// ============================================================================
