/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "CaloMCTools.h"
#include "Event/CaloDigits_v2.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "LHCbAlgs/Transformer.h"
#include "Relations/RelationWeighted1D.h"

namespace LHCb::Calo::Associators {
  /**
   *  Helper algorithm to "decode" linker object
   *  into the relation table for Monte Carlo associatiations
   *  from CaloDigits to MCParticle
   *
   *  @author Vanya BELYAEV Ivan.Belyaev@lapp.in2p3.fr
   *  @date 2004-05-03
   */
  class Digit2MCParticle : public LHCb::Algorithm::Transformer<LHCb::CaloFuture2MC::DigitTable(
                               const LHCb::Event::Calo::Digits&, const LHCb::MCParticles& )> {

    mutable Gaudi::Accumulators::AveragingCounter<unsigned int> m_cd2mcLinkscounter{this, "#CD2MC links"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::DEBUG>         m_emptyTable{this, "Empty Relation table"};

  public:
    /** standard constructor
     *  @param name algorithm instance name
     *  @param pSvc pointer to service locator
     */
    Digit2MCParticle( const std::string& name, ISvcLocator* pSvc )
        : Transformer( name, pSvc,
                       {KeyValue{"CaloDigits", LHCb::CaloDigitLocation::Ecal},
                        KeyValue{"MCParticles", LHCb::MCParticleLocation::Default}},
                       KeyValue{"Output", "Relations/" + LHCb::CaloDigitLocation::Default} ) {}

    /// algorithm execution
    LHCb::CaloFuture2MC::DigitTable operator()( const LHCb::Event::Calo::Digits& digits,
                                                const LHCb::MCParticles&         mcParticles ) const override {
      // create the relation table
      LHCb::CaloFuture2MC::DigitTable table{3000};

      for ( const auto& digit : digits ) {
        // use the auxillary container to be immune agains
        // potential bugs (if any..), like duplicated entries, etc
        // in the Linkers
        if ( m_links.exist() ) {
          auto const*                               links = m_links.get();
          std::map<const LHCb::MCParticle*, double> mcMap;
          links->applyToLinks( Containers::key_traits<LHCb::Detector::Calo::CellID>::identifier( digit.cellID() ),
                               [&mcMap, &mcParticles]( unsigned int, unsigned int tgtIndex, float weight ) {
                                 const LHCb::MCParticle* particle =
                                     static_cast<const LHCb::MCParticle*>( mcParticles.containedObject( tgtIndex ) );
                                 auto [it, ok] = mcMap.try_emplace( particle, weight );
                                 if ( !ok ) it->second += weight;
                               } );

          // copy contents of the auxillary container into Relation Table
          for ( auto [particle, energy] : mcMap ) {
            // use fast i_push method!
            table.i_push( digit.cellID(), particle, energy ); // NB !!
          }
        }
      }

      // mandatory operation after "i_push"!
      table.i_sort(); // NB !!

      m_emptyTable += table.relations().empty();
      m_cd2mcLinkscounter += table.relations().size();
      return table;
    }

  private:
    DataObjectReadHandle<LHCb::LinksByKey> m_links{this, "Link", "Link/" + LHCb::CaloDigitLocation::Ecal};
  };

  DECLARE_COMPONENT_WITH_ID( Digit2MCParticle, "CaloFutureDigit2MCLinks2Table" )

} // namespace LHCb::Calo::Associators
