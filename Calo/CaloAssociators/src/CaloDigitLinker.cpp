/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Location.h"
#include "Event/CaloDigits_v2.h"
#include "Event/LinksByKey.h"
#include "Event/MCCaloDigit.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "LHCbAlgs/Transformer.h"
#include <map>

#include "Associators/Detail/Mixin.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"

#include "CaloDet/DeCalorimeter.h"
#include "DetDesc/Condition.h"
#include "DetDesc/GenericConditionAccessorHolder.h"

/** @class CaloDigitLinker CaloDigitLinker.h
 *  This algorithm creates association tables between the cell IDs of
 *  the Calo digits and the corresponding MC hits and particles.
 *
 */

namespace LHCb::Calo::Algorithm {

  class CaloDigitLinker
      : public LHCb::Algorithm::MultiTransformer<std::tuple<LHCb::LinksByKey, LHCb::LinksByKey>(
                                                     const LHCb::Event::Calo::Digits&, const LHCb::MCCaloDigits&,
                                                     const DeCalorimeter& ),
                                                 LHCb::DetDesc::usesConditions<DeCalorimeter>>,
        Detail::LinkerMixin<LHCb::Detector::Calo::CellID> {

  public:
    // Standard constructor
    CaloDigitLinker( const std::string& name, ISvcLocator* pSvcLocator );
    std::tuple<LHCb::LinksByKey, LHCb::LinksByKey>
    operator()( LHCb::Event::Calo::Digits const&, LHCb::MCCaloDigits const&, DeCalorimeter const& ) const override;

  private:
    // only cells with ET over this threshold will be considered
    Gaudi::Property<double> m_cellET{this, "MinDigitET", 1 * Gaudi::Units::MeV};
    // only cells with  E over this threshold will be considered
    Gaudi::Property<double> m_cellE{this, "MinDigitEnergy", 50 * Gaudi::Units::MeV};
    // minimal transverse energy deposition by particle
    Gaudi::Property<double> m_minET{this, "MinParticleETdeposit", 1 * Gaudi::Units::MeV};
    // minimal            energy deposition by particle
    Gaudi::Property<double> m_minE{this, "MinParticleEdeposit", 50 * Gaudi::Units::MeV};
    // minimal relative   energy deposition by particle
    Gaudi::Property<double> m_minFr{this, "MinParticleFraction", 1 * Gaudi::Units::perCent};
    // minimal particle energy
    Gaudi::Property<double> m_minPE{this, "MinParticleEnergy", 50 * Gaudi::Units::MeV};

    mutable Gaudi::Accumulators::StatCounter<> m_nMCLinks{this, "Number of MC-links #"};
  };

  DECLARE_COMPONENT_WITH_ID( CaloDigitLinker, "CaloDigitLinker" )

  //=============================================================================
  // Constructor
  //=============================================================================
  CaloDigitLinker::CaloDigitLinker( const std::string& name, ISvcLocator* svcLoc )
      : MultiTransformer(
            name, svcLoc,
            {KeyValue{"DigitLocation", LHCb::CaloDigitLocation::Default},
             KeyValue{"MCDigitLocation", LHCb::MCCaloDigitLocation::Ecal},
             KeyValue{"DetectorLocation", DeCalorimeterLocation::Ecal}},
            {KeyValue{"HitLinkLocation", Links::location( LHCb::CaloDigitLocation::Default + "2MCHits" )},
             KeyValue{"ParticleLinkLocation", Links::location( LHCb::CaloDigitLocation::Default + "2MCParticles" )}} ) {
  }

  //=============================================================================
  // Execution
  //=============================================================================
  std::tuple<LHCb::LinksByKey, LHCb::LinksByKey> CaloDigitLinker::operator()( LHCb::Event::Calo::Digits const& digits,
                                                                              LHCb::MCCaloDigits const&        mcdigits,
                                                                              DeCalorimeter const& calo ) const {

    // Create association tables.
    auto hitLinks      = outputLinks<LHCb::MCCaloHit>();
    auto particleLinks = outputLinks<LHCb::MCParticle>();

    // scale factor for recalculation of eActive into eTotal
    const double activeToTotal = calo.activeToTotal();

    int nLinks = 0;

    // Loop over the digits.
    for ( const auto digit : digits ) {

      // skip extra small energy depositions
      if ( !( digit.energy() > m_cellE ) &&
           !( digit.energy() * std::sin( calo.cellCenter( digit.cellID() ).Theta() ) > m_cellET ) )
        continue;

      // coefficient to transform energy to transverse energy
      const double sinTheta = calo.cellSine( digit.cellID() );

      // threshold value
      const double threshold = m_minFr * digit.energy();

      // Get the MC digit with the same channel ID.
      const auto mcDigit = mcdigits.object( digit.cellID() );
      if ( !mcDigit ) continue;
      // Get the MC hits associated to the MC digit and their weights.
      const auto& hits = mcDigit->hits();

      // Make a map of the MC hits/particles and weights.
      std::map<const LHCb::MCCaloHit*, double>  hitMap;
      std::map<const LHCb::MCParticle*, double> particleMap;

      for ( auto hit : hits ) {

        if ( hit.data() == static_cast<LHCb::MCCaloHit*>( nullptr ) )
          continue; // spillover MCHit contributed to this digit

        hitMap[hit] += hit.data()->activeE();
        particleMap[hit.data()->particle()] += hit.data()->activeE();

        const LHCb::MCParticle* particle = hit.data()->particle();

        // Add the energy deposition from the given particle to ALL parents
        while ( 0 != particle ) {
          const LHCb::MCVertex* vertex = particle->originVertex();
          if ( !vertex ) continue;
          particle = vertex->mother();
          if ( !particle ) continue;
          particleMap[particle] += hit.data()->activeE();
        }
      }

      // Make the associations
      hitLinks.link( digit.cellID().index(), hitMap );

      for ( auto [mcp, weight] : particleMap ) {
        // use only more or less  "energetic" particles
        if ( mcp->momentum().e() < m_minPE ) continue;

        double energy = weight * activeToTotal;
        // skip very small energy depositions
        if ( energy < m_minE && energy * sinTheta < m_minET && energy < threshold ) continue;

        particleLinks.link( digit.cellID().index(), mcp, energy );
        ++nLinks;
      }
    }
    m_nMCLinks += nLinks;
    return {std::move( hitLinks ), std::move( particleLinks )};
  }

} // namespace LHCb::Calo::Algorithm
