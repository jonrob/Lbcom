/*****************************************************************************\
* (c) Copyright 2020 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "CaloDet/DeCalorimeter.h"
#include "CaloFutureUtils/CaloFuture2MC.h"
#include "CaloFutureUtils/CaloFutureAlgUtils.h"
#include "CaloFutureUtils/ClusterFunctors.h"
#include "DetDesc/Condition.h"
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/CaloCluster.h"
#include "Event/CaloClusters_v2.h"
#include "Event/CaloDataFunctor.h"
#include "Event/CaloHypos_v2.h"
#include "Event/MCParticle.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"
#include "Relations/RelationWeighted1D.h"

using ICaloTable = LHCb::CaloFuture2MC::ClusterTable;
namespace {

  std::optional<float> checkConversion( const LHCb::MCParticle*            matchParticle,
                                        const LHCb::Detector::Calo::CellID currentCluster, const ICaloTable& table ) {
    auto allParticles = table.relations( currentCluster );

    switch ( matchParticle->particleID().pid() ) {
    case 111:
      // pi0: if it decays to photons, check conversion for each daughter photon
      // (if many are converted, minimum zConv is chosen)
      return std::accumulate( allParticles.begin(), allParticles.end(), std::optional<float>{},
                              [&]( std::optional<float> zConv, const auto& match ) {
                                auto part = match.to();
                                if ( part->mother() && part->mother() == matchParticle &&
                                     part->particleID().pid() == 22 ) {
                                  auto zConvThis = checkConversion( part, currentCluster, table );
                                  if ( zConvThis && ( !zConv || *zConv > *zConvThis ) ) return zConvThis;
                                }
                                return zConv;
                              } );
    case 22: {
      // photons
      bool                  flagEle  = false;
      bool                  flagPos  = false;
      bool                  flagOth  = false;
      const LHCb::MCVertex* convVert = nullptr;
      for ( auto it_range : allParticles ) {
        auto part = it_range.to();
        if ( part->originVertex()->position().Z() > 12000.0 ) continue;
        if ( part->mother() && part->mother() == matchParticle ) {
          if ( part->particleID().pid() == 11 ) {
            flagEle  = !flagEle;
            convVert = part->originVertex();
          } else if ( part->particleID().pid() == -11 ) {
            flagPos  = !flagPos;
            convVert = part->originVertex();
          } else {
            flagOth = true;
            break;
          }
        }
      }
      if ( ( flagEle || flagPos ) && ( !flagOth ) ) return convVert->position().Z();
      return std::nullopt;
    }
    default:
      return std::nullopt;
    }
  }

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
  double RetrieveZPosition( LHCb::Event::Calo::Clusters::const_reference<simd, behaviour> in ) {
    return in.position().z();
  }

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
  double RetrieveZPosition( LHCb::Event::Calo::Hypotheses::const_reference<simd, behaviour> in ) {
    return RetrieveZPosition( in.clusters()[0] );
  }

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
  StatusCode RetrieveUncorrectedEXY( LHCb::Event::Calo::Clusters::const_reference<simd, behaviour> in,
                                     const DeCalorimeter& calo, double& E, double& X, double& Y ) {
    auto ret = LHCb::Calo::Functor::calculateClusterEXY( in.entries(), &calo );
    if ( ret ) {
      E = ret->Etot;
      X = ret->x;
      Y = ret->y;
      return StatusCode::SUCCESS;
    } else {
      return StatusCode::FAILURE;
    }
  }

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
  StatusCode RetrieveUncorrectedEXY( LHCb::Event::Calo::Hypotheses::const_reference<simd, behaviour> in,
                                     const DeCalorimeter& calo, double& E, double& X, double& Y ) {
    auto clusters = in.clusters();
    if ( clusters.size() == 1 ) {
      auto ret = LHCb::Calo::Functor::calculateClusterEXY( in.clusters()[0].entries(), &calo );
      if ( ret ) {
        E = ret->Etot;
        X = ret->x;
        Y = ret->y;
        return StatusCode::SUCCESS;
      } else {
        return StatusCode::FAILURE;
      }
    } else
      return StatusCode::FAILURE;
  }

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
  LHCb::CaloPosition::Parameters
  RetrieveParameters( LHCb::Event::Calo::Clusters::const_reference<simd, behaviour> in ) {
    return {in.position().x(), in.position().y(), in.e()};
  }

  template <SIMDWrapper::InstructionSet simd, LHCb::Pr::ProxyBehaviour behaviour>
  LHCb::CaloPosition::Parameters
  RetrieveParameters( LHCb::Event::Calo::Hypotheses::const_reference<simd, behaviour> in ) {
    return RetrieveParameters( in.clusters()[0] );
  }
} // namespace

namespace LHCb::Calo {
  /**
   *  Class to monitor energy and position resolution of clusters and hypos by
   *  comparing their parameters with the corresponding parameters of MC particles
   *  matched to them. It uses the relation table produced by CaloClusterMCTruth
   *  and works for relation tables with either CaloCluster or CaloHypo.
   *
   *  @author Sasha Zenaiev oleksandr.zenaiev@cern.ch
   *  @date   2020-01-29
   */
  template <typename CaloTYPE>
  class ClusterResolution
      : public LHCb::Algorithm::Consumer<void( const DeCalorimeter&, const CaloTYPE&, const ICaloTable& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeCalorimeter>> {

  public:
    using base_type = LHCb::Algorithm::Consumer<void( const DeCalorimeter&, const CaloTYPE&, const ICaloTable& ),
                                                LHCb::DetDesc::usesBaseAndConditions<GaudiTupleAlg, DeCalorimeter>>;

    using KeyValue = typename base_type::KeyValue;

    ClusterResolution( const std::string& name, ISvcLocator* pSvc )
        : base_type{name,
                    pSvc,
                    {KeyValue{"Detector", DeCalorimeterLocation::Ecal},
                     KeyValue{"input", LHCb::CaloClusterLocation::Default},
                     KeyValue{"inputRelations", "Relations/" + LHCb::CaloClusterLocation::Default}}} {}

    void operator()( const DeCalorimeter& calo, const CaloTYPE& input, const ICaloTable& table ) const override {

      auto bufferMatched     = m_counterMatched.buffer();
      auto bufferDeltaEOverE = m_counterDeltaEOverE.buffer();
      auto buffetDeltaX      = m_counterDeltaX.buffer();
      auto bufferDeltaY      = m_counterDeltaY.buffer();

      auto tuple = this->nTuple( m_tuplePrefix );

      for ( const auto& cluster : input.scalar() ) {
        auto currentSeed = Functor::cellID( cluster );

        const auto& [matchParticle, matchFraction] = bestMatch( cluster, table.relations( currentSeed ) );
        bufferMatched += ( matchParticle != nullptr );
        if ( !matchParticle ) continue;

        auto currentZPos = RetrieveZPosition( cluster );
        auto currentPars = RetrieveParameters( cluster );
        // check photon conversion
        auto   zConv    = checkConversion( matchParticle, currentSeed, table );
        auto   vertex   = matchParticle->originVertex();
        auto   deltaZ   = currentZPos - vertex->position().Z();
        auto   mom      = matchParticle->momentum();
        auto   xExtrap  = vertex->position().X() + deltaZ * ( mom.Px() / mom.Pz() );
        auto   yExtrap  = vertex->position().Y() + deltaZ * ( mom.Py() / mom.Pz() );
        auto   seedPos  = calo.cellCenter( currentSeed );
        double CellSize = calo.cellSize( currentSeed );

        auto  energy        = matchParticle->momentum().E();
        auto  energyCluster = currentPars[2];
        float deltaEoverE   = ( energyCluster - energy ) / energy;

        bufferDeltaEOverE += deltaEoverE;
        buffetDeltaX += xExtrap - currentPars[0];
        bufferDeltaY += yExtrap - currentPars[1];

        double uncorE( -999. ), uncorX( -999. ), uncorY( -999. );
        if ( !RetrieveUncorrectedEXY( cluster, calo, uncorE, uncorX, uncorY ).isSuccess() )
          uncorE = uncorX = uncorY = -999.;

        auto sc = tuple->column( "maxMatchFraction", matchFraction );
        sc &= tuple->column( "energy", energy );
        sc &= tuple->column( "energyCluster", energyCluster );
        sc &= tuple->column( "xCluster", currentPars[0] );
        sc &= tuple->column( "yCluster", currentPars[1] );
        sc &= tuple->column( "zCluster", currentZPos );
        sc &= tuple->column( "energyClusterUncor", uncorE );
        sc &= tuple->column( "xClusterUncor", uncorX );
        sc &= tuple->column( "yClusterUncor", uncorY );
        sc &= tuple->column( "xExtrap", xExtrap );
        sc &= tuple->column( "yExtrap", yExtrap );
        sc &= tuple->column( "xOrigVertex", vertex->position().X() );
        sc &= tuple->column( "yOrigVertex", vertex->position().Y() );
        sc &= tuple->column( "zOrigVertex", vertex->position().Z() );
        sc &= tuple->column( "flagConv", zConv.has_value() );
        sc &= tuple->column( "zConv", zConv.value_or( -999.0 ) );
        sc &= tuple->column( "area", currentSeed.area() );
        sc &= tuple->column( "cellSize", CellSize );
        sc &= tuple->column( "cellCol", currentSeed.col() );
        sc &= tuple->column( "cellRow", currentSeed.row() );
        sc &= tuple->column( "xSeed", seedPos.x() );
        sc &= tuple->column( "ySeed", seedPos.y() );
        sc &= tuple->column( "px", matchParticle->momentum().Px() );
        sc &= tuple->column( "py", matchParticle->momentum().Py() );
        sc &= tuple->column( "pz", matchParticle->momentum().Pz() );
        sc.andThen( [&] { return tuple->write(); } ).orElse( [&] { ++m_writefailed; } ).ignore();
      }
    }

  private:
    template <typename Cluster, typename Range>
    auto bestMatch( const Cluster& cluster, const Range& range ) const {
      using Result = std::pair<const MCParticle*, float>;
      return std::accumulate(
          range.begin(), range.end(), Result{nullptr, m_minMatchFraction}, [&]( Result current, const auto& match ) {
            const MCParticle* particle = match.to();
            if ( particle->momentum().e() < m_minEnergy ) return current;
            if ( !m_PDGID.empty() &&
                 find( m_PDGID.begin(), m_PDGID.end(), particle->particleID().pid() ) == m_PDGID.end() )
              return current;
            if ( !m_PDGIDMother.empty() &&
                 ( !particle->mother() || find( m_PDGIDMother.begin(), m_PDGIDMother.end(),
                                                particle->mother()->particleID().pid() ) == m_PDGIDMother.end() ) )
              return current;
            auto  weight        = match.weight(); // "weight" of the relation
            auto  pars          = RetrieveParameters( cluster );
            float matchFraction = weight / particle->momentum().e() * weight / pars[2];
            return matchFraction <= current.second ? current : Result{particle, matchFraction};
          } );
    }

    // properties
    Gaudi::Property<std::vector<int>> m_PDGID{this, "PDGID", {}};
    Gaudi::Property<std::vector<int>> m_PDGIDMother{this, "PDGIDMother", {}};
    Gaudi::Property<float>            m_minMatchFraction{this, "minMatchFraction", 0.0};
    Gaudi::Property<float>            m_minEnergy{this, "minEnergy", 0.0};
    Gaudi::Property<std::string>      m_tuplePrefix{this, "tuplePrefix", "matchedClusters"};

    // counters
    mutable Gaudi::Accumulators::BinomialCounter<int>     m_counterMatched{this, "Matched"};
    mutable Gaudi::Accumulators::StatCounter<float>       m_counterDeltaEOverE{this, "DeltaEOverE"};
    mutable Gaudi::Accumulators::StatCounter<float>       m_counterDeltaX{this, "DeltaX"};
    mutable Gaudi::Accumulators::StatCounter<float>       m_counterDeltaY{this, "DeltaY"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_writefailed{this, "Cannot write tuple"};
  };

  DECLARE_COMPONENT_WITH_ID( ClusterResolution<Event::Calo::Clusters>, "CaloFutureClusterResolution" )
  DECLARE_COMPONENT_WITH_ID( ClusterResolution<Event::Calo::Hypotheses>, "CaloFutureHypoResolution" )
} // namespace LHCb::Calo
