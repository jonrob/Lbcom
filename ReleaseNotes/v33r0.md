2021-07-07 Lbcom v33r0
===

This version uses
LHCb [v53r0](../../../../LHCb/-/tags/v53r0),
Gaudi [v36r0](../../../../Gaudi/-/tags/v36r0) and
LCG [100](http://lcginfo.cern.ch/release/100/) with ROOT 6.24.00.

This version is released on `master` branch.
Built relative to Lbcom [v32r2](/../../tags/v32r2), with the following changes:

### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~Build | Rewrite CMake configuration in "modern CMake", !551 (@clemenci)
