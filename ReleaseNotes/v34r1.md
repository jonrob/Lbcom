2022-10-26 Lbcom v34r1
===

This version uses
LHCb [v54r1](../../../../LHCb/-/tags/v54r1),
Detector [v1r5](../../../../Detector/-/tags/v1r5),
Gaudi [v36r8](../../../../Gaudi/-/tags/v36r8) and
This version uses LCG [101a](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on `master` branch.
Built relative to Lbcom [v34r0](/../../tags/v34r0), with the following changes:


### Code cleanups and changes to tests ~modernisation ~cleanup ~testing

- ~RICH | Dropped unused Rich code (RichAlgorithms and RichMCMonitors packages), !634 (@sponce)
- ~Core | Adapted to DD4hep support in TransportSvc, !629 (@sponce)
- ~RICH | RichSmartIDClustering - Degrade missing PD error message to debug, !632 (@jonrob)
