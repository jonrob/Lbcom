2022-11-10 Lbcom v34r1p1
===

This version uses
LHCb [v54r1p1](../../../../LHCb/-/tags/v54r1p1),
Detector [v1r5p1](../../../../Detector/-/tags/v1r5p1),
Gaudi [v36r8](../../../../Gaudi/-/tags/v36r8) and
This version uses LCG [101a](http://lcginfo.cern.ch/release/101a_LHCB_7/) with ROOT 6.24.08.

This version is released on `master` branch.
Identical to Lbcom [v34r1](/../../tags/v34r1).
