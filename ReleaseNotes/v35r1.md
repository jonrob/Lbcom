2024-02-13 Lbcom v35r1
===

This version uses
LHCb [v55r1](../../../../LHCb/-/tags/v55r1),
Detector [v1r26](../../../../Detector/-/tags/v1r26),
Gaudi [v38r0](../../../../Gaudi/-/tags/v38r0) and
LCG [105a](http://lcginfo.cern.ch/release/105a/) .

This version is released on the `master` branch.
Built relative to Lbcom [v35r0](/../../tags/v35r0), with the following changes:
