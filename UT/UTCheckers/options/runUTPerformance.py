###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#-------------------------------------------------------------------------------
# Run ZS monitoring for UT
#
# Andy Beiter (based on code by Mark Tobin)
# 2018-09-04
#-------------------------------------------------------------------------------
from Gaudi.Configuration import *
from Configurables import LHCbApp
#from Configurables import *

#importOptions( 'DecodeRawEvent.py' )

## Import default LHCb application options
#importOptions( '$UTDOPTS/LHCbApplication.opts' )

mainSeq = GaudiSequencer('MainSeq')
mainSeq.MeasureTime = True

appMgr = ApplicationMgr()
#appMgr.EvtMax = 100000
appMgr.EvtMax = 1
#appMgr.EvtMax = -1
#appMgr.EvtSel= "NONE"
appMgr.TopAlg.append(mainSeq)

#appMgr.HistogramPersistency = 'ROOT'
#HistogramPersistencySvc().OutputFile = 'ROOT-250V-50481.root'
#HistogramPersistencySvc().OutputFile = 'ROOT.root'

from Configurables import LHCbApp
from Configurables import EventClockSvc

EventDataSvc().RootCLID = 1
EventDataSvc().EnableFaultHandler = True
EventDataSvc().ForceLeaves = True
EventPersistencySvc().CnvServices = ['LHCb::RawDataCnvSvc']
EventClockSvc().EventTimeDecoder = 'OdinTimeDecoder'

AuditorSvc().Auditors = ['ChronoAuditor']

# DataBase - leave blank for latest tags
LHCbApp().DDDBtag = ''
LHCbApp().CondDBtag = ''
#LHCbApp().DataType = '2010'
LHCbApp().OutputLevel = 3
#LHCbApp().SkipEvents = 2500
MessageSvc().Format = "% F%30W%S%7W%R%T %0W%M"

from Configurables import UTPerformanceMonitor

appMgr.TopAlg.append(UTPerformanceMonitor("UTPerformanceMonitor"))

ApplicationMgr().HistogramPersistency = 'ROOT'
HistogramPersistencySvc().OutputFile = 'statusNOW.root'
