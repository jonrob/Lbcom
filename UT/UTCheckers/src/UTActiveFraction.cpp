/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "TGraph.h"
#include "TH2D.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiHistoAlg.h"

#include <mutex>
#include <vector>

/**
 *  Class to work out the active fraction of the detector from the
 *  conditions database as a function of time.  This makes use of the
 *  FakeEventTime tool in the EventClockSvc.  The start and step time
 *  used in the tool are required inputs for this algorithm.
 *  The input options require a start time and step size given in
 *  ns since the epoch and the total number of steps required
 *
 *  An example python scripts is available in options.
 *  Usage: gaudirun.py $UTCHECKERSROOT/options/runUTActiveTrend.py
 *
 *  @author Andy Beiter (based on code by Mark Tobin)
 *  @date   2018-09-04
 */
namespace UT {
  class UTActiveFraction
      : public LHCb::Algorithm::Consumer<void( const EventContext&, DeUTDetector const& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>> {
  public:
    UTActiveFraction( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name, pSvcLocator, {"UTLocation", DeUTDetLocation::location()}} {}

    StatusCode initialize() override;
    void       operator()( EventContext const& evtCtx, DeUTDetector const& ) const override;
    StatusCode finalize() override;

  private:
    Gaudi::Property<unsigned int> m_nSteps{this, "Steps", 0};
    Gaudi::Property<long long>    m_startTime{this, "StartTime", 0};
    Gaudi::Property<long long>    m_timeStep{this, "TimeStep", 0};
    // the next 3 members should be replaced by new Gaudi histograms
    mutable std::vector<double> m_time;
    mutable std::vector<double> m_active;
    mutable std::mutex          m_mutex;
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( UTActiveFraction )
} // namespace UT

StatusCode UT::UTActiveFraction::initialize() {
  return GaudiHistoAlg::initialize().andThen( [&] {
    if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
    return StatusCode::SUCCESS;
  } );
}

void UT::UTActiveFraction::operator()( EventContext const& evtCtx, DeUTDetector const& det ) const {
  // active fraction
  info() << evtCtx.evt() << " fraction active " << det.fractionActive() << " @ "
         << ( m_startTime + evtCtx.evt() * m_timeStep ) / 1000000000 << endmsg;
  std::scoped_lock lock( m_mutex );
  m_time.push_back( ( m_startTime + evtCtx.evt() * m_timeStep ) / 1000000000 );
  m_active.push_back( det.fractionActive() * 100. );
}

StatusCode UT::UTActiveFraction::finalize() {
  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Finalize" << endmsg;
  TH2D* h = new TH2D( "FrameUT", "", 100, m_time[0], m_time.back(), 10, 95., 100. );
  h->GetXaxis()->SetTimeDisplay( 1 );
  h->GetXaxis()->SetTimeOffset( -m_time[0] );
  h->GetYaxis()->SetTitle( "Fraction active (%)" );
  auto* g = new TGraph( m_time.size(), &m_time[0], &m_active[0] );
  g->SetName( "FractionVsTimeUT" );
  g->Write();
  return GaudiHistoAlg::finalize(); // must be called after all other actions
}
