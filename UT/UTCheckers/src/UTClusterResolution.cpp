/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/UTCluster.h"
#include "Kernel/ITrajPoca.h"
#include "Kernel/LineTraj.h"
#include "Kernel/UTLexicalCaster.h"
#include "Linker/LinkedTo.h"
#include "MCInterfaces/IMCParticleSelector.h"
#include "Math/VectorUtil.h"
#include "TrackInterfaces/IUTClusterPosition.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"
#include "UTDet/DeUTSensor.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiHistoAlg.h"

#include <cmath>

using namespace LHCb;

/**
 *  Class for plotting the resolution of UTClusters. It makes the following
 *  histograms for 1, 2, 3 and 4 strip clusters:
 *  - Offline resolution (rec. minus true position)
 *  - Offline pull
 *  - Online resolution
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

class UTClusterResolution
    : public LHCb::Algorithm::Consumer<void( LHCb::UTClusters const&, DeUTDetector const&, DeMagnet const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector, DeMagnet>> {

public:
  UTClusterResolution( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void       operator()( LHCb::UTClusters const&, DeUTDetector const&, DeMagnet const& ) const override;

private:
  virtual void fillHistograms( const LHCb::UTCluster* aCluster, const LHCb::MCHit* aHit, DeUTDetector const& det,
                               DeMagnet const& magnet ) const;

  // selector
  ToolHandle<IMCParticleSelector> m_selector{this, "Selector", "MCParticleSelector"};

  // job options
  Gaudi::Property<bool> m_mergeSplitClusters{this, "MergeSplitClusters",
                                             false}; ///< Consider only one UTCluster in split cluster
  Gaudi::Property<bool> m_skipSplitClusters{this, "SkipSplitClusters",
                                            false}; ///< Ignore all UTClusters in a split cluster

  PublicToolHandle<ITrajPoca>    m_poca{"TrajPoca"}; ///< Pointer to the ITrajPoca interface
  ToolHandle<IUTClusterPosition> m_positionTool{this, "PositionToolName", "UTOfflinePosition"};

// warnings
#ifdef USE_DD4HEP
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_DD4HEPNoSensorFound{this, "Failed to find sensor", 0};
#else
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_DetDescNoSensorFound{this, "Failed to find sensor", 0};
#endif
};

DECLARE_COMPONENT( UTClusterResolution )

//--------------------------------------------------------------------
//
//  UTClusterResolution : Resolution plots
//
//--------------------------------------------------------------------
namespace {
  int histoId( const int clusterSize ) {
    // determine which histogram to fill
    return ( clusterSize <= 9 ? clusterSize : 9 );
  }

  double calculateUTrue( const MCHit* aHit, const DeUTSensor& aSensor ) {
    Gaudi::XYZPoint localPoint = aSensor.toLocal( aHit->midPoint() );
    return localPoint.x();
  }
} // namespace

UTClusterResolution::UTClusterResolution( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {{"InputData", UTClusterLocation::UTClusters},
                {"UTLocation", DeUTDetLocation::location()},
                {"DeMagnetLocation", LHCb::Det::Magnet::det_path}}} {}

StatusCode UTClusterResolution::initialize() {
  if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
  return Consumer::initialize();
}

void UTClusterResolution::operator()( LHCb::UTClusters const& clusters, DeUTDetector const& det,
                                      DeMagnet const& magnet ) const {
  // linker
  auto* links = get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( inputLocation<UTClusters>() + "2MCHits" ) );
  if ( !links ) throw GaudiException( "Failed to find table", "UTClusterResolution::operator", StatusCode::FAILURE );
  links->resolveLinks( evtSvc() );
  const LinkedTo<LHCb::MCHit> aTable{links};

  // histos per digit
  auto iterClus = clusters.begin();
  while ( iterClus != clusters.end() ) {
    // Get the next cluster
    auto jterClus = std::next( iterClus );
    // Find first cluster that is not a direct neighbour
    if ( m_mergeSplitClusters.value() || m_skipSplitClusters.value() ) {
      int lastChannel = ( *iterClus )->lastChannel();
      while ( jterClus != clusters.end() && ( *jterClus )->firstChannel() - lastChannel == 1 ) {
        lastChannel = ( *jterClus )->lastChannel();
        ++jterClus;
      }
    }
    // Skip all the merged clusters
    if ( m_skipSplitClusters.value() && jterClus - iterClus != 1 ) iterClus = jterClus;
    // get MC truth for this cluster
    if ( iterClus != clusters.end() ) {
      const MCHit* mcHit = aTable.range( *iterClus ).try_front();
      if ( mcHit ) { fillHistograms( *iterClus, mcHit, det, magnet ); }
    }
    // Move to next cluster
    iterClus = jterClus;
  } // loop iterClus
}

void UTClusterResolution::fillHistograms( const UTCluster* aCluster, const MCHit* aHit, DeUTDetector const& det,
                                          DeMagnet const& magnet ) const {
  // fill histograms
  if ( aHit && ( m_selector->accept( aHit->mcParticle() ) ) ) {

    // get true u - need stereoangle/z info from channel
    const Detector::UT::ChannelID aChan   = aCluster->channelID();
    auto                          aSector = det.findSector( aChan );

#ifdef USE_DD4HEP
    auto aSensor = aSector.findSensor( aHit->midPoint() );
    if ( !aSensor.isValid() ) {
      ++m_DD4HEPNoSensorFound;
      return;
    }
#else
    auto sensor = aSector->findSensor( aHit->midPoint() );
    if ( !sensor ) {
      ++m_DetDescNoSensorFound;
      return;
    }
    auto& aSensor = *sensor;
#endif

    const double uTrue = calculateUTrue( aHit, aSensor );

    // rec u - offline
    IUTClusterPosition::Info measVal = m_positionTool->estimate( det, magnet, aCluster );
    const double             uRec    = aSensor.localU( measVal.strip.strip(), measVal.fractionalPosition );

    // determine which histos to fill based on cluster size
    const int id = histoId( (int)measVal.clusterSize );

    // fill double error = measVal.second;
    const double error = measVal.fractionalError * aSensor.pitch();

    // Fill offline resolution and pull
    std::string histTitle = "UT " + UT::toString( id ) + "-strip clusters";
    plot( uRec - uTrue, 10 + id, "Resolution " + histTitle, -0.25, 0.25, 100 );
    plot( ( uRec - uTrue ) / error, 20 + id, "Pull " + histTitle, -10., 10., 100 );

    // Fill true fraction position versus reconstructed frac position
    double trueFracPos = uTrue - aSensor.localU( measVal.strip.strip(), 0 );
    int    sign        = ( aSensor.localU( 2 ) - aSensor.localU( 1 ) > 0 ) ? 1 : -1;
    trueFracPos        = double( sign ) * trueFracPos / aSensor.pitch();
    profile1D( measVal.fractionalPosition, trueFracPos, 50 + id, "True frac position " + histTitle + " vs rec frac pos",
               -0.005, 1.005, 101 );

    // Plot the cluster size versus angle in each station (Lorentz deflection)
    profile1D( aHit->dxdz(), aCluster->size(), 60 + aChan.station(),
               "Cluster size vs dx/dz in station " + UT::toString( aChan.station() ), -0.2, 0.2, 50 );

    // Plot total number and number of symmetric two-strip clusters versus
    // angle in each station (the ratio shows the Lorentz deflection)
    if ( id == 2 && fabs( measVal.fractionalPosition - 0.5 ) < 0.2 )
      plot( aHit->dxdz(), 70 + aChan.station(),
            "Num sym 2-strip clus vs dxdz in station " + UT::toString( aChan.station() ), -0.2, 0.2, 50 );
    plot( aHit->dxdz(), 80 + aChan.station(), "Num of clus vs dxdz in station " + UT::toString( aChan.station() ), -0.2,
          0.2, 50 );

    // now the online version
    double uOnline = aSensor.localU( aChan.strip(), aCluster->interStripFraction() );
    plot( uOnline - uTrue, 30 + id, "Online resolution " + histTitle, -0.25, 0.25, 100 );

    // Do the same with trajectories
    auto clusTraj = aSensor.trajectory( measVal.strip.strip(), measVal.fractionalPosition );

    // Get the true trajectory
    const LineTraj<double> mcTraj( aHit->entry(), aHit->exit() );

    // poca !
    Gaudi::XYZVector distance;
    double           s1 = 0.0;
    double           s2 = clusTraj.muEstimate( mcTraj.position( s1 ) );
    m_poca->minimize( mcTraj, s1, clusTraj, s2, distance, 0.005 )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    double residual = std::copysign( distance.R(), distance.x() );

    // Resolution and pull plots
    plot( residual, 110, "Resolution UT (using Trajectories)", -0.25, 0.25, 100 );
    plot( residual / error, 120, "Pull UT (using Trajectories)", -10., 10., 100 );
    histTitle += " (using Trajectories)";
    plot( residual, 110 + id, "Resolution " + histTitle, -0.25, 0.25, 100 );
    plot( residual / error, 120 + id, "Pull " + histTitle, -10., 10., 100 );

  } // aHit
  // end
}
