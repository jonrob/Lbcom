/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/UTCluster.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "Kernel/IParticlePropertySvc.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/ParticleProperty.h"
#include "Kernel/SiLandauFun.h"
#include "Linker/LinkedTo.h"
#include "MCInterfaces/IMCParticleSelector.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators.h"
#include "GaudiAlg/GaudiHistoAlg.h"

using namespace LHCb;

/**
 *  Checking class to plot S/N and charge of UTClusters for each readout sector.
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

class UTClusterChecker
    : public LHCb::Algorithm::Consumer<void( const UTClusters&, const LHCb::LinksByKey&, const DeUTDetector& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>> {

public:
  UTClusterChecker( const std::string& name, ISvcLocator* svcloc )
      : Consumer{name,
                 svcloc,
                 {{"clusterLocation", UTClusterLocation::UTClusters},
                  {"clusterLinkLocation", LHCb::LinksByKey::linkerName( UTClusterLocation::UTClusters + "2MCHits" )},
                  {"UTLocation", DeUTDetLocation::location()}}} {}
  StatusCode initialize() override;
  void       operator()( const UTClusters&, const LHCb::LinksByKey&, const DeUTDetector& ) const override;

private:
  void fillHistograms( const LHCb::UTCluster& aCluster, const LHCb::MCHit* aHit, const DeUTDetector& det ) const;

  ToolHandle<IMCParticleSelector>                       m_selector{this, "Selector", "MCParticleSelector"};
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_invalidNoise{this, "Invalid noise value found in DeUTSector",
                                                                       10};

  double m_betaMin{};
  double m_gammaMin{};
};

DECLARE_COMPONENT( UTClusterChecker )

StatusCode UTClusterChecker::initialize() {
  if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
  return GaudiHistoAlg::initialize().andThen( [&] {
    auto                          pid      = LHCb::ParticleID( 211 );
    auto                          pp       = service<LHCb::IParticlePropertySvc>( "LHCb::ParticlePropertySvc", true );
    const LHCb::ParticleProperty* partProp = pp->find( pid );
    const double                  mPion    = partProp->mass();

    const double betaGammaMin = 4.8;
    const double P            = betaGammaMin * mPion;
    const double E            = sqrt( P * P + mPion * mPion );
    m_betaMin                 = P / E;
    m_gammaMin                = E / mPion;
    return StatusCode::SUCCESS;
  } );
}

void UTClusterChecker::operator()( const UTClusters& clusterCont, const LHCb::LinksByKey& links,
                                   const DeUTDetector& det ) const {
  // linker
  const_cast<LHCb::LinksByKey&>( links ).resolveLinks( evtSvc() );
  const LinkedTo<LHCb::MCHit> aTable{&links};

  if ( !aTable ) throw GaudiException( "Failed to find table", "UTClusterChecker::operator()", StatusCode::FAILURE );
  // histos per cluster
  for ( const auto& clus : clusterCont ) {
    // get MC truth for this cluster
    if ( !clus ) continue;
    const LHCb::MCHit* mchit = aTable.range( clus ).try_front();
    if ( mchit ) fillHistograms( *clus, mchit, det );
  } // loop clus
}

void UTClusterChecker::fillHistograms( const UTCluster& aCluster, const MCHit* aHit, const DeUTDetector& det ) const {
  // fill histograms
  if ( aHit && m_selector->accept( aHit->mcParticle() ) ) {
    // histo cluster size for physics tracks

#ifdef USE_DD4HEP
    auto                  sector   = det.findSector( aCluster.channelID() );
    std::optional<double> noiseOpt = sector.noise( aCluster.channelID() );
    if ( !noiseOpt ) {
      ++m_invalidNoise;
    } else {
      auto noise = noiseOpt.value();
#else
    auto& sector = *det.findSector( aCluster.channelID() );
    auto  noise  = sector.noise( aCluster.channelID() );
    {
#endif
      std::string dType = sector.type();
      plot( aCluster.totalCharge(), dType + "/1", "Charge", 0., 200., 200 );
      const double signalToNoise = aCluster.totalCharge() / noise;
      plot( signalToNoise, dType + "/2", "S/N", 0., 100., 100 );
      double mpv  = SiLandauFun::MPV( aHit->mcParticle()->beta(), aHit->mcParticle()->gamma(), aHit->pathLength() );
      double norm = SiLandauFun::MPV( m_betaMin, m_gammaMin, sector.thickness() );
      plot( signalToNoise * norm / mpv, dType + "/3", "S/N (scaled)", 0., 100., 100 );
    }

  } // aHit && accepted
}
