/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "Gaudi/Accumulators.h"
#include "Gaudi/MonitoringHub.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IDataProviderSvc.h"
#include "GaudiKernel/IHistogramSvc.h"
#include "Kernel/UTIDMapping.h"
#include "Kernel/UTLexicalCaster.h"
#include "LHCbAlgs/Consumer.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"
#include "MCInterfaces/IMCParticleSelector.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTLayer.h"
#include <GAUDI_VERSION.h>
#include <Gaudi/Accumulators/Histogram.h>

#include <map>
#include <memory>
#include <mutex>

using namespace LHCb;

namespace {
  template <typename T, typename OWNER>
  void map_emplace( T& t, typename T::key_type key, int offset, OWNER* owner, std::string const& title,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1 ) {
    t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
               std::forward_as_tuple( owner, UT::toString( key + offset ), title, axis1 ) );
  }
  template <typename T, typename OWNER>
  void map_emplace( T& t, typename T::key_type key, int offset, OWNER* owner, std::string const& title,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis2 ) {
    t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
               std::forward_as_tuple( owner, UT::toString( key + offset ), title, axis1, axis2 ) );
  }

  template <typename T>
  nlohmann::json x2j( const T& value ) {
#if GAUDI_MAJOR_VERSION < 37
    return value.toJSON();
#else
    return value;
#endif
  }
} // namespace

/**
 *  Class for checking UT efficiencies. It produces the following plots:
 *  - x and y distributions of all MCHits in a UT layer.
 *  - x vs y distribution of all MCHits in a UT layer.
 *  - x and y distributions of MCHits which make an UTCluster.
 *  - x vs y distribution of MCHits which make an UTCluster.
 *  By dividing these histograms one gets the efficiency per layer.
 *
 *  In the finalize method, a summary of the efficiency per layer is printed.
 *
 *  @author Xuhao Yuan based on code by:

 *    @author A.Beiter
 *    @author M.Needham
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2021-04-02
 */

class UTEffChecker final
    : public LHCb::Algorithm::Consumer<void( MCParticles const&, LHCb::LinksByKey const&, DeUTDetector const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>> {

public:
  UTEffChecker( const std::string& name, ISvcLocator* svcloc );
  StatusCode initialize() override;
  void       operator()( MCParticles const&, LHCb::LinksByKey const&, DeUTDetector const& ) const override;
  StatusCode finalize() override;

private:
  void initHistogramsForChannel( unsigned int uniqueID ) const;

  void layerEff( LHCb::MCParticle const*, DeUTDetector const&, LinkedFrom<LHCb::UTDigit> const&,
                 LinkedTo<LHCb::MCHit> const& ) const;

  bool isInside( const DeUTLayer*, const LHCb::MCHit*, DeUTDetector const& ) const;

  int findHistoId( unsigned int aLayerId );
  int uniqueHistoID( const LHCb::Detector::UT::ChannelID aChannel ) const;

  Gaudi::Property<std::string> m_asctLocation{this, "asctLocation", UTClusterLocation::UTClusters + "2MCHits"};

  Gaudi::Property<bool>           m_includeGuardRings{this, "IncludeGuardRings", false};
  Gaudi::Property<bool>           m_pEff{this, "PrintEfficiency", true};
  ToolHandle<IMCParticleSelector> m_selector{this, "MCParticleSelector", "MCParticleSelector"};

  const std::string hpath = "";
  template <typename HistoType>
  using HistoMap = std::map<unsigned int, std::unique_ptr<HistoType>>;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<1>> m_xLayerHistos;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<1>> m_yLayerHistos;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<2>> m_xyLayerHistos;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<1>> m_effXLayerHistos;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<1>> m_effYLayerHistos;
  mutable std::map<unsigned int, Gaudi::Accumulators::Histogram<2>> m_effXYLayerHistos;
  mutable std::mutex                                                m_mutex;
  const unsigned int                                                m_layerNum = 4u;
};

DECLARE_COMPONENT( UTEffChecker )

UTEffChecker::UTEffChecker( const std::string& name, ISvcLocator* svcloc )
    : Consumer{name,
               svcloc,
               {{"InputData", MCParticleLocation::Default},
                {"hitTableLocation", LHCb::LinksByKey::linkerName( LHCb::MCParticleLocation::Default + "2MCUTHits" )},
                {"UTLocation", DeUTDetLocation::location()}}} {
  setProperty( "HistoPrint", false ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
}

StatusCode UTEffChecker::initialize() {
  return Consumer::initialize().andThen( [&] {
    if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
  } );
}

void UTEffChecker::initHistogramsForChannel( unsigned int uniqueID ) const {
  // determined side
  // Create layer name
  std::string layerName = ", layer " + UT::toString( (int)uniqueID );
  // Book x distribution histogram MCHits
  map_emplace( m_xLayerHistos, static_cast<int>( uniqueID ), 5000, this, "X Distribution Hits" + layerName,
               {400, -1000., 1000.} );
  // Book y distribution histogram MCHits
  map_emplace( m_yLayerHistos, static_cast<int>( uniqueID ), 6000, this, "Y Distribution Hits" + layerName,
               {400, -1000., 1000.} );
  // Book x distribution histogram UTDigits
  map_emplace( m_effXLayerHistos, static_cast<int>( uniqueID ), 7000, this, "X Distribution Ass. Clusters" + layerName,
               {400, -1000., 1000.} );
  // Book y distribution histogram UTDigits
  map_emplace( m_effYLayerHistos, static_cast<int>( uniqueID ), 8000, this, "Y Distribution Ass. Clusters" + layerName,
               {400, -1000., 1000.} );
  // Book x vs y distribution histogram MCHits
  map_emplace( m_xyLayerHistos, static_cast<int>( uniqueID ), 9000, this, "X Vs Y Distribution Hits" + layerName,
               {200, -1000., 1000.}, {200, -1000., 1000.} );
  // Book x vs y distribution histogram UTDigits
  map_emplace( m_effXYLayerHistos, static_cast<int>( uniqueID ), 10000, this,
               "X Vs Y Distribution Ass. Clusters" + layerName, {200, -1000., 1000.}, {200, -1000., 1000.} );
}

void UTEffChecker::operator()( const MCParticles& particles, const LHCb::LinksByKey& hitLinks,
                               DeUTDetector const& det ) const {
  // Get the UTCluster to MCHit associator
  auto associator = get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( m_asctLocation ) );
  auto table      = LinkedFrom<LHCb::UTDigit>( associator );
  if ( !table ) throw GaudiException( "Failed to find table", "UTEffChecker::operator()", StatusCode::FAILURE );

  // Get the MCParticle to MCHit associator
  auto hitTable = LinkedTo<LHCb::MCHit>{&hitLinks};
  for ( const auto& particle : particles ) {
    if ( m_selector->accept( particle ) ) layerEff( particle, det, table, hitTable );
  }
}

StatusCode UTEffChecker::finalize() {
  // init the message service
  if ( m_pEff ) {
    info() << " -------Efficiency %--------- " << endmsg;
  } else {
    info() << " -------InEfficiency %--------- " << endmsg;
  }

  // print out efficiencys
  for ( auto const& [histoId, xLayerHisto] : m_xLayerHistos ) {
    // eff for this layer
    // Find number of entries in xLayerHisto
    nlohmann::json xlayerj       = x2j( xLayerHisto );
    auto           nEntriesLayer = xlayerj.at( "nEntries" ).get<unsigned long>();
    int            nAcc          = (int)nEntriesLayer;
    nlohmann::json xefflayerj    = x2j( m_effXLayerHistos.at( histoId ) );
    auto           nEntriesEff   = xefflayerj.at( "nEntries" ).get<unsigned long>();
    int            nFound        = (int)nEntriesEff;
    double         eff           = 9999.;
    double         err           = 9999.;
    if ( nAcc > 0 ) {
      eff = 100.0 * (double)nFound / (double)nAcc;
      err = sqrt( ( eff * ( 100.0 - eff ) ) / (double)nAcc );
      if ( !m_pEff ) eff = 1 - eff;
    }
    info() << LHCb::UTNames::UniqueLayerToString( ( histoId / 100 - 1 ) * 2 + histoId % 100 - 1 ) << " eff " << eff
           << " +/- " << err << endmsg;
  } // layer

  info() << " -----------------------" << endmsg;

  // hack to prevent crash
  StatusCode sc = GaudiHistoAlg::finalize();
  if ( sc.isFailure() ) return sc;

  if ( !fullDetail() ) {}

  return sc;
}

void UTEffChecker::layerEff( MCParticle const* aParticle, DeUTDetector const& det,
                             LinkedFrom<LHCb::UTDigit> const& table, LinkedTo<LHCb::MCHit> const& hitTable ) const {
  std::scoped_lock lock( m_mutex );
  // find all MC hits for this particle
  auto      hits      = hitTable.range( aParticle );
  const int utIDupper = 0x600000L;
  if ( hits.empty() ) return;

  std::array<std::vector<const MCHit*>, 4> layerHits;

  det.applyToAllLayers( [this, &hits, &table, &det, &layerHits]( DeUTLayer const& layer ) {
    // look for MCHit in this layer.....
    for ( const auto& aHit : hits ) {
      auto hitChan = ( aHit.sensDetID() > utIDupper )
                         ? Detector::UT::ChannelID( LHCb::UTIDMapping::ReconvertID( aHit.sensDetID() ) )
                         : Detector::UT::ChannelID( aHit.sensDetID() );
      if ( uniqueHistoID( hitChan ) == uniqueHistoID( layer.elementID() ) && isInside( &layer, &aHit, det ) ) {
        layerHits[layer.elementID().layer()].push_back( &aHit );
      }
    };

    if ( ( ( det.version() == ( DeUTDetector::GeoVersion::v0 ) ) ||
           ( det.version() == ( DeUTDetector::GeoVersion::v1 ) && layer.elementID().side() == 1 ) ) &&
         !layerHits[layer.elementID().layer()].empty() ) {
      bool found =
          std::any_of( layerHits[layer.elementID().layer()].begin(), layerHits[layer.elementID().layer()].end(),
                       [&]( const MCHit* h ) { return !table.range( h ).empty(); } );
      auto histoId = uniqueHistoID( layer.elementID() );

      // create histos if needed
      if ( m_xLayerHistos.find( histoId ) == m_xLayerHistos.end() ) { initHistogramsForChannel( histoId ); }

      const MCHit*          aHit     = layerHits[layer.elementID().layer()].front();
      const Gaudi::XYZPoint midPoint = aHit->midPoint();

      // histo vs x
      ++m_xLayerHistos.at( histoId )[midPoint.x()];
      // histo vs y
      ++m_yLayerHistos.at( histoId )[midPoint.y()];
      //  xy
      ++m_xyLayerHistos.at( histoId )[{midPoint.x(), midPoint.y()}];

      if ( found ) {
        if ( m_pEff ) {
          ++m_effXYLayerHistos.at( histoId )[{midPoint.x(), midPoint.y()}];
          ++m_effXLayerHistos.at( histoId )[midPoint.x()];
          ++m_effYLayerHistos.at( histoId )[midPoint.y()];
        }
      } else {
        if ( !m_pEff ) {
          ++m_effXYLayerHistos.at( histoId )[{midPoint.x(), midPoint.y()}];
          ++m_effXLayerHistos.at( histoId )[midPoint.x()];
          ++m_effYLayerHistos.at( histoId )[midPoint.y()];
        }
      }
    } // if
  } );
}

int UTEffChecker::uniqueHistoID( const Detector::UT::ChannelID aChan ) const {
  return aChan.station() * 100 + aChan.layer() % 2 + 1u;
}

bool UTEffChecker::isInside( const DeUTLayer* aLayer, const MCHit* aHit, DeUTDetector const& det ) const {
  // check if expect hit to make cluster
  if ( !aLayer->isInside( aHit->midPoint() ) ) return false;
  if ( m_includeGuardRings ) return true;
#ifdef USE_DD4HEP

  auto aSector = det.findSector( aHit->midPoint() );
  //  return aSector.globalInActive( aHit->midPoint() );
  return aSector.isValid() && aSector.globalInActive( aHit->midPoint() );

#else
  auto aSector = det.findSector( aHit->midPoint() );
  return aSector && aSector->globalInActive( aHit->midPoint() );
#endif
}
