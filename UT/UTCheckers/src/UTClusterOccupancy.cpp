/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "UTDet/DeUTSector.h"
#include "UTOccupancy.h"

using UTClusterOccupancy = UTOccupancy<LHCb::UTCluster>;

template <>
const std::string& UTClusterOccupancy::dataLocation() const {
  return LHCb::UTClusterLocation::UTClusters;
}

template <>
const std::string UTClusterOccupancy::histoDirName() const {
  return "UTClusterOccupancy";
}

template <>
double UTClusterOccupancy::defaultThreshold() const {
  return 0.0; // no threshold for clusters
}

template <>
unsigned int UTClusterOccupancy::weight( const LHCb::UTCluster* obj ) const {
  return obj->size();
}

template <>
std::optional<double> UTClusterOccupancy::SN( const LHCb::UTCluster* obj, DeUTDetector const& det ) const {
#ifdef USE_DD4HEP
  auto                  sector = det.findSector( obj->channelID() );
  std::optional<double> noise  = sector.noise( obj->channelID() );
  return noise ? std::optional<double>{obj->totalCharge() / noise.value()} : std::nullopt;
#else
  auto sector = det.findSector( obj->channelID() );
  return std::optional<double>{obj->totalCharge() / sector->noise( obj->channelID() )};
#endif
}

// template class UTOccupancy<LHCb::UTCluster>;

DECLARE_COMPONENT_WITH_ID( UTClusterOccupancy, "UTClusterOccupancy" )
