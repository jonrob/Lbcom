/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/MCUTDeposit.h"
#include "Kernel/UTNames.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiHistoAlg.h"

using namespace LHCb;

/**
 *  Make plots for MCUTDeposits
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */
class MCUTDepositMonitor : public LHCb::Algorithm::Consumer<void( const MCUTDeposits& ),
                                                            Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  MCUTDepositMonitor( const std::string& name, ISvcLocator* svcloc );
  StatusCode initialize() override;
  void       operator()( const MCUTDeposits& depositsCont ) const override;

private:
  void fillHistograms( const LHCb::MCUTDeposit& aDeposit ) const;
};

DECLARE_COMPONENT( MCUTDepositMonitor )

MCUTDepositMonitor::MCUTDepositMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"InputData", MCUTDepositLocation::UTDeposits}} {}

StatusCode MCUTDepositMonitor::initialize() {
  if ( "" == histoTopDir() ) setHistoTopDir( "UT/" );
  return Consumer::initialize();
}

void MCUTDepositMonitor::operator()( const MCUTDeposits& depositsCont ) const {
  // number of digits
  plot( (double)depositsCont.size(), 1, "Number of deposits", 0., 10000., 200 );
  // histos per digit
  for ( const auto& i : depositsCont ) fillHistograms( *i );
}

void MCUTDepositMonitor::fillHistograms( const MCUTDeposit& aDeposit ) const {
  // Plot deposited charge
  plot( aDeposit.depositedCharge(), 2, "Deposited charge", 0., 100., 100 );

  // Plot number of deposits per station
  const int iStation = aDeposit.channelID().station();
  plot( (double)iStation, 3, "Number of deposits per station", -0.5, 4.5, 5 );

  // by layer
  plot( (double)( 10 * iStation + aDeposit.channelID().layer() ), 4, "Number of deposits per layer", -0.5, 40.5, 41 );

  // detailed level histos
  if ( fullDetail() ) {
    const MCHit* aHit = aDeposit.mcHit();
    if ( aHit ) {
      // take midPoint
      Gaudi::XYZPoint impactPoint = aHit->midPoint();

      // fill x vs y scatter plots
      std::string title = "x vs y " + UTNames::StationToString( aDeposit.channelID() );
      plot2D( impactPoint.x(), impactPoint.y(), 200 + iStation, title, -1000., 1000., -1000., 1000., 50, 50 );
    }
  }
}
