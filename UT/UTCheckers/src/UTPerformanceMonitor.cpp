/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTSummary.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/SiChargeFun.h"
#include "Kernel/UTDetectorPlot.h"
#include "Kernel/UTTell1ID.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiHistoAlg.h"

#include <range/v3/algorithm/for_each.hpp>
#include <range/v3/view/concat.hpp>
#include <range/v3/view/span.hpp>

#include <string>

using namespace LHCb;

namespace {
  // empty struct used as derived condition and used to log and create histos
  // at the time of loading geometry
  struct InitLogger {};
} // namespace

/**
 *  Class for monitoring UTPerformances
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */
class UTPerformanceMonitor
    : public LHCb::Algorithm::Consumer<void( EventContext const&, UTSummary const&, UTClusters const&,
                                             DeUTDetector const&, InitLogger const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector, InitLogger>> {

public:
  UTPerformanceMonitor( const std::string& name, ISvcLocator* svcloc );
  StatusCode initialize() override;
  void       operator()( EventContext const&, UTSummary const&, UTClusters const&, DeUTDetector const&,
                   InitLogger const& ) const override;

private:
  void geometryLogging( DeUTDetector const& det );

  Gaudi::Property<unsigned int> m_expectedEvents{this, "ExpectedEvents", 100000};
  ToolHandle<IUTReadoutTool>    m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};
};

DECLARE_COMPONENT( UTPerformanceMonitor )

UTPerformanceMonitor::UTPerformanceMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {{"summaryLocation", UTSummaryLocation::UTSummary},
                {"InputData", UTClusterLocation::UTClusters},
                {"UTLocation", DeUTDetLocation::location()},
                {"InitLogger", "UTPerformanceMonitorInitLogger"}}} {}

StatusCode UTPerformanceMonitor::initialize() {
  // Set the top directory to UT.
  if ( "" == histoTopDir() ) setHistoTopDir( "UT/" );
  return Consumer::initialize().andThen( [&] {
    // This derived condition is empty but logs and creates plots when loaded
    addConditionDerivation( {DeUTDetLocation::location()}, this->template inputLocation<InitLogger>(),
                            [this]( DeUTDetector const& det ) -> InitLogger {
                              geometryLogging( det );
                              return {};
                            } );
    return StatusCode::SUCCESS;
  } );
}

// function deriving InitLogger. Essentially logging and creating histos
void UTPerformanceMonitor::geometryLogging( DeUTDetector const& det ) {
  // get the disabled sectors
  const auto& disabledSec = det.disabledSectors();
  if ( disabledSec.empty() ) {
    info() << "All sectors enabled " << endmsg;
  } else {
    for ( const auto& i : disabledSec ) {
#ifdef USE_DD4HEP
      info() << "disabled " << i.nickname() << endmsg;
#else
      info() << "disabled " << i->nickname() << endmsg;
#endif
    } // for
  }
  // get the disabled beetles
  const auto& disabledB = det.disabledBeetles();
  if ( disabledB.empty() ) {
    info() << "All beetles enabled " << endmsg;
  } else {
    for ( const auto& iterB : disabledB ) { info() << "disabled " << det.uniqueBeetle( iterB ) << endmsg; }
  }
  // active fraction
  info() << "fraction active " << det.fractionActive() << endmsg;
  UT::UTDetectorPlot noiseProp( "noise", "Noise fraction" );
  UT::UTDetectorPlot activeProp( "fracActive", "Active fraction" );
  det.applyToAllSectors( [&]( DeUTSector const& sector ) {
    // 2-D plot what is active in the detector element
    UT::UTDetectorPlot::Bins bins = activeProp.toBins( sector );
#ifdef USE_DD4HEP
    auto secNoise = sector.sectorNoise().value();
#else
    auto secNoise = sector.sectorNoise();
#endif
    for ( int yBin = bins.beginBinY; yBin != bins.endBinY; ++yBin ) {
      plot2D( bins.xBin, yBin, activeProp.name(), activeProp.title(), activeProp.minBinX(), activeProp.maxBinX(),
              activeProp.minBinY(), activeProp.maxBinY(), activeProp.nBinX(), activeProp.nBinY(),
              sector.fractionActive() );
      plot2D( bins.xBin, yBin, noiseProp.name(), noiseProp.title(), noiseProp.minBinX(), noiseProp.maxBinX(),
              noiseProp.minBinY(), noiseProp.maxBinY(), noiseProp.nBinX(), noiseProp.nBinY(), secNoise );
    } // loop bin Y
  } );
}

void UTPerformanceMonitor::operator()( EventContext const& evtCtx, UTSummary const& summary,
                                       UTClusters const& clusterCont, DeUTDetector const& det,
                                       InitLogger const& ) const {
  double frac = 0.0;
  // loop over missing sectors
  // some are lost because of errors in decoding, some were lost [ie tell1 not readout]
  ranges::for_each(
      ranges::views::concat( ranges::span( summary.corruptedBanks() ), ranges::span( summary.missingBanks() ) ),
      [this, &frac]( int i ) {
        m_readoutTool->applyToAllBoardSectors( UTTell1ID( i ),
                                               [&frac]( DeUTSector const& s ) { frac += s.fractionActive(); } );
      } );
  frac /= det.nSectors();
  plot( frac, 1, "active fraction", 0., 1., 200 ); // plot active fraction

  plot2D( evtCtx.evt(), frac, 11, "active fraction versus time", 0., m_expectedEvents.value(), 0., 1., 50, 200 );

  // get the occupancy
  const double occ = clusterCont.size() / ( det.nStrip() * frac );
  plot( occ, 2, "occupancy", 0., 1., 200 );
  plot2D( evtCtx.evt(), occ, 12, "occ versus time", 0., m_expectedEvents.value(), 0., 0.1, 50, 200 );

  // get the modal charge
  double shorth = SiChargeFun::shorth( clusterCont.begin(), clusterCont.end() );
  plot( shorth, "shorth", 0., 100., 200 );

  // get the modal charge
  double tm = SiChargeFun::truncatedMean( clusterCont.begin(), clusterCont.end() );
  plot( tm, "tm", 0., 100., 200 );
  plot2D( evtCtx.evt(), tm, 13, "tms versus time", 0., m_expectedEvents.value(), 0., 100, 200, 200 );

  double lms = SiChargeFun::LMS( clusterCont.begin(), clusterCont.end() );
  plot( lms, "lms", 0., 100., 200 );

  double gm = SiChargeFun::generalizedMean( clusterCont.begin(), clusterCont.end() );
  plot( gm, "gm", 0., 100., 200 );
}
