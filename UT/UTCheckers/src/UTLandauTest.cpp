/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"

/** class UTLandauTest, package ITCheckers
 *  Algorithm for checking Landau generator behave as they should
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

class UTLandauTest : public GaudiHistoAlg {

public:
  /// Constructer
  using GaudiHistoAlg::GaudiHistoAlg;

  /// intialize
  StatusCode initialize() override {
    return GaudiHistoAlg::initialize().andThen( [&] {
      // Initialize the Landau function
      SmartIF<IRndmGen> landauDist = randSvc()->generator( Rndm::Landau( 0.226, 1. ) );

      // Sample the Landau funtion 10M times and plot it
      for ( int i = 0; i < 1e7; ++i ) { plot( 90 + ( 5 * landauDist->shoot() ), "Landau", 0., 200., 2000 ); }
    } );
  }

  StatusCode execute() override { return StatusCode::SUCCESS; }
};

DECLARE_COMPONENT( UTLandauTest )
