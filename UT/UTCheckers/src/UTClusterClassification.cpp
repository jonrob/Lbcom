/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/UTCluster.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "LHCbAlgs/Consumer.h"
#include "Linker/LinkedTo.h"
#include "UTDet/DeUTDetector.h"

#include <mutex>

using namespace LHCb;

/**
 *  This algorithm counts the total number of UTClusters from
 *  - a primary interaction
 *  - a secondary interaction
 *  - noise
 *  - spillover
 *  - unknown sources.
 *  It prints out the statistics in the finalize method.
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */
class UTClusterClassification
    : public LHCb::Algorithm::Consumer<void( const UTClusters& ),
                                       Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {

public:
  UTClusterClassification( const std::string& name, ISvcLocator* svcloc )
      : Consumer{name, svcloc, {"clusterLocation", UTClusterLocation::UTClusters}} {}
  void       operator()( const UTClusters& ) const override;
  StatusCode finalize() override;

private:
  StatusCode  fillInfo( const LHCb::MCHit* aHit ) const;
  std::string findSpill( const LHCb::MCHit* aHit ) const;

  unsigned int tCount() const;

  std::vector<std::string>                  m_spillNames; // full name of spills
  Gaudi::Property<std::vector<std::string>> m_spillVector{
      this, "SpillVector", {"/", "/Prev/", "/PrevPrev/", "/Next/"}, [this]( auto& ) {
        // construct container names once
        // path in Transient data store
        m_spillNames.clear();
        std::transform( m_spillVector.begin(), m_spillVector.end(), std::back_inserter( m_spillNames ),
                        [this]( const auto& sn ) { return "/Event" + sn + m_hitLocation; } );
      }}; // short names of spills

  Gaudi::Property<std::string> m_asctLocation{this, "asctLocation", UTClusterLocation::UTClusters + "2MCHits"};
  Gaudi::Property<std::string> m_hitLocation{this, "hitLocation", MCHitLocation::UT};

  mutable std::atomic<int>                    m_nEvent = 0;
  mutable std::map<std::string, unsigned int> m_infoMap;
  mutable std::mutex                          m_mutex;
};

DECLARE_COMPONENT( UTClusterClassification )

void UTClusterClassification::operator()( const UTClusters& clusterCont ) const {
  ++m_nEvent;

  // linker
  auto* links = get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( m_asctLocation ) );
  if ( !links )
    throw GaudiException( "Failed to find table", "UTClusterClassification::operator()", StatusCode::FAILURE );
  links->resolveLinks( evtSvc() );
  const LinkedTo<LHCb::MCHit> aTable{links};

  // histos per digit
  auto lock = std::unique_lock{m_mutex};
  for ( const auto& i : clusterCont ) {
    auto range = aTable.range( i );
    if ( range.empty() ) {
      ++m_infoMap["noise"];
    } else {
      this->fillInfo( range.try_front() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    }
  }
}

StatusCode UTClusterClassification::finalize() {
  // normalization
  unsigned     total      = tCount();
  unsigned int eventSpill = m_infoMap["primary"] + m_infoMap["secondary"] + m_infoMap["unknown"];
  unsigned int spillover  = total - eventSpill - m_infoMap["noise"];

  info() << "--- Cluster Classification " << endmsg;
  info() << "Event Spill " << eventSpill / double( total ) << endmsg;
  info() << "   |---> Primary " << m_infoMap["primary"] / double( total ) << endmsg;
  info() << "   |---> Secondary " << m_infoMap["secondary"] / double( total ) << endmsg;
  info() << "   |---> Unknown " << m_infoMap["unknown"] / double( total ) << endmsg;
  info() << "Noise " << m_infoMap["noise"] / double( total ) << " # per event " << m_infoMap["noise"] / m_nEvent
         << endmsg;
  info() << "Spillover " << spillover / double( total ) << endmsg;

  for ( const auto& i : m_infoMap ) {
    if ( ( i.first != "noise" ) && ( i.first != "primary" ) && ( i.first != "unknown" ) &&
         ( i.first != "secondary" ) ) {
      info() << i.first + " " << i.second / double( total ) << endmsg;
    }
  }
  info() << "----------------------------------------" << endmsg;

  return GaudiHistoAlg::finalize();
}

unsigned int UTClusterClassification::tCount() const {
  return std::accumulate( m_infoMap.begin(), m_infoMap.end(), 0u,
                          []( unsigned int total, const auto& p ) { return total + p.second; } );
}

StatusCode UTClusterClassification::fillInfo( const MCHit* aHit ) const {
  // find out which spill came from....
  std::string spill = this->findSpill( aHit );

  if ( spill == "/" ) {
    // event spill...
    if ( const MCParticle* particle = aHit->mcParticle(); particle ) {
      if ( const MCVertex* origin = particle->originVertex(); origin ) {
        if ( ( origin->type() < MCVertex::MCVertexType::HadronicInteraction ) && ( origin->type() != 0 ) ) {
          ++m_infoMap["primary"];
        } else if ( origin->type() == 0 ) {
          ++m_infoMap["unknown"];
        } else {
          ++m_infoMap["secondary"];
        }
      } // vertex
    }   // particle
  } else {
    ++m_infoMap[spill];
  }
  return StatusCode::SUCCESS;
}

std::string UTClusterClassification::findSpill( const MCHit* aHit ) const {
  const IRegistry* reg = aHit->parent()->registry();
  auto             i   = std::find( m_spillNames.begin(), m_spillNames.end(), reg->identifier() );
  return i != m_spillNames.end() ? *i : "Unknown";
}
