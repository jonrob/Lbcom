/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/UTCluster.h"
#include "Event/UTSummary.h"
#include "LHCbAlgs/Consumer.h"
#include <string>

namespace LHCb {

  /**
   *  Class for printing out the UT Event
   *
   *  @author A.Beiter (based on code by M.Needham)
   *  @date   2018-09-04
   */
  class UTDumpEvent : public LHCb::Algorithm::Consumer<void( const UTClusters&, const UTSummary&, const ODIN& )> {

  public:
    UTDumpEvent( const std::string& name, ISvcLocator* pSvcLocator )
        : Consumer{name,
                   pSvcLocator,
                   {KeyValue{"clusterLocation", UTClusterLocation::UTClusters},
                    KeyValue{"summaryLocation", UTSummaryLocation::UTSummary},
                    KeyValue{"ODINLocation", ODINLocation::Default}}} {}

    void operator()( const UTClusters& clusterCont, const UTSummary& summary, const ODIN& odin ) const override {
      info() << "*** UT Information for run " << odin.runNumber() << " event " << odin.eventNumber() << endmsg;
      info() << summary << endmsg;
      if ( m_fullDetail.value() ) {
        info() << "Cluster Information:" << endmsg;
        for ( const auto& clus : clusterCont ) info() << *clus << endmsg;
      }
      info() << "End of Event Info " << endmsg;
    }

  private:
    Gaudi::Property<bool> m_fullDetail{this, "FullDetail", false};
  };

  DECLARE_COMPONENT_WITH_ID( UTDumpEvent, "UTDumpEvent" )

} // namespace LHCb
