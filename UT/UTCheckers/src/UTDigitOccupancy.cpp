/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTDigit.h"
#include "UTOccupancy.h"

using UTDigitOccupancy = UTOccupancy<LHCb::UTDigit>;

template <>
const std::string& UTDigitOccupancy::dataLocation() const {
  return LHCb::UTDigitLocation::UTDigits;
}

template <>
const std::string UTDigitOccupancy::histoDirName() const {
  return "UTDigitOccupancy";
}

template <>
double UTDigitOccupancy::defaultThreshold() const {
  return 3.0; // no threshold for clusters
}

// template class ITOccupancy<LHCb::UTDigit>;

DECLARE_COMPONENT_WITH_ID( UTDigitOccupancy, "UTDigitOccupancy" )
