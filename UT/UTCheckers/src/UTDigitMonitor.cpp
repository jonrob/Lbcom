/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTDigit.h"
#include "Kernel/UTDigitFun.h"
#include "UTDet/DeUTDetector.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiHistoAlg.h"

using namespace LHCb;

/** @class UTDigitMonitor UTDigitMonitor.h
 *
 *  Class for monitoring UTDigits
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */

class UTDigitMonitor
    : public LHCb::Algorithm::Consumer<void( UTDigits const&, DeUTDetector const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>> {

public:
  /// constructer
  UTDigitMonitor( const std::string& name, ISvcLocator* svcloc )
      : Consumer{
            name, svcloc, {{"InputData", UTDigitLocation::UTDigits}, {"UTLocation", DeUTDetLocation::location()}}} {}

  /// initialize
  StatusCode initialize() override {
    return Consumer::initialize().andThen( [&] {
      // Set the top directory to UT
      if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
    } );
  }

  /// execute
  void operator()( const UTDigits& digitsCont, DeUTDetector const& det ) const override {
    // number of digits
    plot( (double)digitsCont.size(), 1, "Number of digits", 0., 10000., 500 );
    // histos per digit
    for ( const auto& d : digitsCont ) fillHistograms( d, det );

    double shorth = SiChargeFun::shorth( digitsCont.begin(), digitsCont.end() );
    plot( shorth, "shorth", 0., 100., 200 );
  }

private:
  void fillHistograms( const LHCb::UTDigit* aDigit, DeUTDetector const& det ) const {
    // histogram by station
    const int iStation = aDigit->station();
    plot( (double)iStation, 2, "Number of digits per station", -0.5, 4.5, 5 );

    // by layer
    const int iLayer = aDigit->layer();
    plot( (double)( 10 * iStation + iLayer ), 3, "Number of digits per layer", -0.5, 40.5, 41 );

    if ( fullDetail() ) {
      auto const sector = det.findSector( aDigit->channelID() );
#ifdef USE_DD4HEP
      plot( aDigit->depositedCharge(), ( sector.isValid() ? sector.type() : "Unknown" ) + "/1", "Deposited charge ", 0.,
            128., 128 );
#else
      plot( aDigit->depositedCharge(), ( sector ? sector->type() : "Unknown" ) + "/1", "Deposited charge ", 0., 128.,
            128 );
#endif
    }
  }
};

DECLARE_COMPONENT( UTDigitMonitor )
