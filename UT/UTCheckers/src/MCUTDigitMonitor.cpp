/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCUTDigit.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiHistoAlg.h"

using namespace LHCb;

/**
 *  Make plots for MCUTDigits
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */
class MCUTDigitMonitor : public LHCb::Algorithm::Consumer<void( const MCUTDigits& ),
                                                          Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  MCUTDigitMonitor( const std::string& name, ISvcLocator* svcloc );
  StatusCode initialize() override;
  void       operator()( const MCUTDigits& ) const override;

private:
  void fillHistograms( const LHCb::MCUTDigit& aDigit ) const;
};

DECLARE_COMPONENT( MCUTDigitMonitor )

MCUTDigitMonitor::MCUTDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"InputData", MCUTDigitLocation::UTDigits}} {}

StatusCode MCUTDigitMonitor::initialize() {
  // Set the top directory to IT or TT.
  if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
  return Consumer::initialize();
}

void MCUTDigitMonitor::operator()( const MCUTDigits& digitsCont ) const {
  // number of digits
  plot( (double)digitsCont.size(), 1, "Number of MCDigits", 0., 10000., 100 );
  // histos per digit
  for ( const auto& i : digitsCont ) fillHistograms( *i );
}

void MCUTDigitMonitor::fillHistograms( const MCUTDigit& aDigit ) const {
  // number of deposits that contribute
  plot( (double)aDigit.mcDeposit().size(), 2, "Number of deposits per digit", -0.5, 10.5, 11 );

  // histogram by station
  const int iStation = aDigit.channelID().station();
  plot( (double)iStation, 3, "Number of digits per station", -0.5, 4.5, 11 );

  // by layer
  const int iLayer = aDigit.channelID().layer();
  plot( (double)( 10 * iStation + iLayer ), "Number of digits per layer", -0.5, 40.5, 41 );
}
