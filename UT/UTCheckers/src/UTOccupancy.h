/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#pragma once

#include "Kernel/LHCbConstants.h"
#include "Kernel/UTLexicalCaster.h"
#include "Kernel/UTNames.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiHistoAlg.h"

using namespace LHCb;

/**
 *  Class for plotting the occupancy of UTDigits
 *
 *  @author A.Beiter based on code by:
 *    @author M.Needham
 *    @author J. van Tilburg
 *  @date   2018-09-04
 */
template <class PBASE>
class UTOccupancy
    : public LHCb::Algorithm::Consumer<void( typename PBASE::Container const&, DeUTDetector const& ),
                                       LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>> {
  using Base = LHCb::Algorithm::Consumer<void( typename PBASE::Container const&, DeUTDetector const& ),
                                         LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector>>;

public:
  UTOccupancy( const std::string& name, ISvcLocator* svcloc )
      : Base{name, svcloc, {{"DataLocation", dataLocation()}, {"UTLocation", DeUTDetLocation::location()}}} {}
  StatusCode initialize() override;
  void       operator()( typename PBASE::Container const&, DeUTDetector const& ) const override;

private:
  const std::string&    dataLocation() const;
  const std::string     histoDirName() const;
  double                defaultThreshold() const;
  void                  fillHistograms( const PBASE* obj, DeUTDetector const& det ) const;
  unsigned int          weight( const PBASE* obj ) const;
  std::optional<double> SN( const PBASE* obj, DeUTDetector const& det ) const;

  int                  m_nBins;
  static constexpr int m_hMax = 512 * 8;

  Gaudi::Property<std::vector<double>> m_threshold{
      this, "Threshold", {defaultThreshold(), defaultThreshold()}}; ///< List of threshold values
  Gaudi::Property<int>         m_binSize{this, "BinSize", 32};      ///< Number of channels in each bin
  Gaudi::Property<std::string> m_histoLocation{this, "HistoLocation", histoDirName()};
};

template <class PBASE>
StatusCode UTOccupancy<PBASE>::initialize() {
  // Set the top directory to IT or UT.
  this->setHistoDir( m_histoLocation );
  if ( "" == this->histoTopDir() ) this->setHistoTopDir( "UT/" );
  // Initialize GaudiHistoAlg
  return GaudiHistoAlg::initialize().andThen( [&] {
    m_nBins = m_hMax / m_binSize;
    return StatusCode::SUCCESS;
  } );
}

template <class PBASE>
void UTOccupancy<PBASE>::operator()( typename PBASE::Container const& objCont, DeUTDetector const& det ) const {
  unsigned int nClus = 0;

  std::map<unsigned int, unsigned int> SectorMap;
  std::map<std::string, unsigned int>  BeetleMap;
  std::map<std::string, unsigned int>  PortMap;

  // histos per digit
  auto iterObj = objCont.begin();
  for ( ; iterObj != objCont.end(); ++iterObj ) {
    const std::optional<double> signalToNoise = SN( *iterObj, det );
    if ( !signalToNoise or signalToNoise.value() < m_threshold[( *iterObj )->station() - det.firstStation()] ) continue;

    ++nClus;

    this->fillHistograms( *iterObj, det );
    auto chan = ( *iterObj )->channelID();
    SectorMap[chan.uniqueSector()] += weight( *iterObj );
    BeetleMap[det.uniqueBeetle( chan )] += weight( *iterObj );
    PortMap[det.uniquePort( chan )] += weight( *iterObj );
  } // loop iterDigit

  // fill histogram of sector occupancy
  for ( auto iterS = SectorMap.begin(); iterS != SectorMap.end(); ++iterS ) {
    double occ = iterS->second / double( det.sector( 0 ).nStrip() );
    this->plot( occ, "module occupancy", -0.005, 1.005, 101 );
  } // iter

  // fill histogram of beetle occupancy
  for ( auto iterS = BeetleMap.begin(); iterS != BeetleMap.end(); ++iterS ) {
    double occ = iterS->second / double( LHCbConstants::nStripsInBeetle );
    this->plot( occ, "beetle occupancy", -0.005, 1.005, 101 );
  } // iter

  // fill histogram of port occupancy
  for ( auto iterP = PortMap.begin(); iterP != PortMap.end(); ++iterP ) {
    const double occ = iterP->second / double( LHCbConstants::nStripsInPort );
    this->plot( occ, "port occupancy", -0.005, 1.005, 101 );
  } // iter

  this->plot( nClus, "nClus", 0., 2000., 500 );
}

template <class PBASE>
void UTOccupancy<PBASE>::fillHistograms( const PBASE* obj, DeUTDetector const& det ) const {

  const Detector::UT::ChannelID aChan = obj->channelID();

#ifdef USE_DD4HEP
  auto utSector = det.findSector( aChan );
#else
  auto& utSector = *det.findSector( aChan );
#endif

  const unsigned int nstrips = utSector.nStrip();

  this->plot( aChan.station(), "station", -0.5, 5.5, 6 );

  int offset;

  if ( aChan.detRegion() == static_cast<unsigned int>( UTNames::detRegion::RegionC ) ) {
    offset = utSector.column() - 1;
  } else if ( aChan.detRegion() == static_cast<unsigned int>( UTNames::detRegion::RegionB ) ) {
    offset = utSector.column() - 7;
  } else if ( aChan.detRegion() == static_cast<unsigned int>( UTNames::detRegion::RegionA ) ) {
    aChan.station() == static_cast<unsigned int>( UTNames::Station::UTa ) ? offset = utSector.column() - 10
                                                                          : offset = utSector.column() - 12;
  } else {
    this->warning() << "Unknown region " << aChan.detRegion() << endmsg;
    return;
  }

  unsigned int row = utSector.row();

  std::string rowString = UT::toString( row );
  this->plot( (double)aChan.strip() - 1. + ( nstrips * offset ),
              "N_" + LHCb::UTNames::UniqueRegionToString( aChan ) + "_Row_" + rowString, 0., m_hMax, m_nBins );
}

template <class PBASE>
unsigned int UTOccupancy<PBASE>::weight( const PBASE* ) const {
  return 1;
}

template <class PBASE>
std::optional<double> UTOccupancy<PBASE>::SN( const PBASE* obj, DeUTDetector const& det ) const {
#ifdef USE_DD4HEP
  auto                  sector = det.findSector( obj->channelID() );
  std::optional<double> noise  = sector.noise( obj->channelID() );
  return noise ? std::optional<double>{obj->depositedCharge() / noise.value()} : std::nullopt;
#else
  auto  sector   = det.findSector( obj->channelID() );
  return std::optional<double>{obj->depositedCharge() / sector->noise( obj->channelID() )};
#endif
}
