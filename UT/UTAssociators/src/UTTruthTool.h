/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#ifndef UTTRUTHTOOL_H
#define UTTRUTHTOOL_H 1

/** @class UTTruthTool UTTruthTool.h
 *
 *  Namespace for following links.....
 *
 *  @author Andy Beiter (based on code by Matt Needham)
 *  @date   2018-09-04
 */

#include "GaudiKernel/SystemOfUnits.h"
#include <map>

namespace LHCb {
  class MCParticle;
  class MCHit;
  class UTDigit;
} // namespace LHCb

namespace UTTruthTool {

  // return map + foundCharge
  std::pair<std::map<const LHCb::MCHit*, double>, double> associateToTruth2( const LHCb::UTDigit* aDigit );

  std::map<const LHCb::MCParticle*, double> associateToTruth( const LHCb::UTDigit* aDigit );

  void removeDeltaRays( const std::map<const LHCb::MCHit*, double>& inputMap,
                        std::map<const LHCb::MCHit*, double>&       hitMap );

  constexpr static double drayTol = 0.02 * Gaudi::Units::mm;

} // namespace UTTruthTool

#endif // UTTRUTHTOOL_H
