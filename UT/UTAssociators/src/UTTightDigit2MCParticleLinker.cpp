/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "Linker/LinkedTo.h"

#include "LHCbAlgs/Transformer.h"

/**
 *  @author Andy Beiter (based on code by Matt Needham)
 *  @date   2018-09-04
 */
class UTTightDigit2MCParticleLinker : public LHCb::Algorithm::Transformer<LHCb::LinksByKey(
                                          const LHCb::UTDigits&, const LHCb::MCParticles&, LHCb::LinksByKey const& )> {
public:
  UTTightDigit2MCParticleLinker( const std::string& name, ISvcLocator* pSvc )
      : Transformer{
            name,
            pSvc,
            {{"InputData", LHCb::UTDigitLocation::UTTightDigits},
             {"MCParticleLocation", LHCb::MCParticleLocation::Default},
             {"ClusterASCTlocation", LHCb::LinksByKey::linkerName( LHCb::UTClusterLocation::UTClusters + "2MCHits" )}},
            {"OutputData", LHCb::LinksByKey::linkerName( LHCb::UTClusterLocation::UTClusters )}} {}

  LHCb::LinksByKey operator()( const LHCb::UTDigits&, const LHCb::MCParticles&,
                               const LHCb::LinksByKey& ) const override;

private:
  using PartPair    = std::pair<const LHCb::MCParticle*, double>;
  using ParticleMap = std::map<const LHCb::MCParticle*, double>;
  std::vector<PartPair> refsToRelate( const ParticleMap&, LHCb::MCParticles const& ) const;

  ParticleMap associateToTruth( const LHCb::UTDigit*, const LinkedTo<LHCb::MCHit>& ) const;

  Gaudi::Property<bool>   m_addSpillOverHits{this, "AddSpillOverHits", false, "Flag to add spill-over to linker table"};
  Gaudi::Property<double> m_minFrac{this, "Minfrac", 0.2, "Minimal charge fraction to link to MCParticle"};
  Gaudi::Property<bool>   m_oneRef{this, "OneRef", false, "Flag to allow only 1 link for each cluster"};
};

DECLARE_COMPONENT( UTTightDigit2MCParticleLinker )

LHCb::LinksByKey UTTightDigit2MCParticleLinker::
                 operator()( const LHCb::UTDigits& clusterCont, const LHCb::MCParticles& mcParts, const LHCb::LinksByKey& links ) const {
  LinkedTo<LHCb::MCHit> aTable{&links};
  // create a linker
  auto aLinker = LHCb::LinksByKey{std::in_place_type<LHCb::UTDigit>, std::in_place_type<LHCb::MCParticle>,
                                  LHCb::LinksByKey::Order::decreasingWeight};
  aLinker.reset();

  // loop and link UTClusters to MC truth
  for ( auto iterClus : clusterCont ) {
    // find all particles
    ParticleMap partMap = associateToTruth( iterClus, aTable );

    // select references to add to table
    auto selectedRefs = refsToRelate( partMap, mcParts );
    if ( !selectedRefs.empty() ) {
      if ( !m_oneRef.value() ) {
        aLinker.link( iterClus, selectedRefs );
      } else {
        auto [p, w] = selectedRefs.back();
        aLinker.link( iterClus, p, w );
      }
    } // refsToRelate != empty
  }   // loop iterClus

  return aLinker;
}

std::vector<UTTightDigit2MCParticleLinker::PartPair>
UTTightDigit2MCParticleLinker::refsToRelate( const ParticleMap& partMap, LHCb::MCParticles const& particles ) const {
  std::vector<PartPair> selRefs;
  for ( auto const& [aParticle, w] : partMap ) {
    if ( aParticle && w > m_minFrac && ( m_addSpillOverHits.value() || &particles == aParticle->parent() ) ) {
      selRefs.emplace_back( aParticle, w );
    }
  }
  return selRefs;
}

UTTightDigit2MCParticleLinker::ParticleMap
UTTightDigit2MCParticleLinker::associateToTruth( const LHCb::UTDigit*         aCluster,
                                                 const LinkedTo<LHCb::MCHit>& aTable ) const {

  ParticleMap partMap;
  double      foundCharge = 0;
  for ( const auto& [hit, weight] : aTable.weightedRange( aCluster ) ) {
    const LHCb::MCParticle* aParticle = hit.mcParticle();
    partMap[aParticle] += weight;
    foundCharge += weight;
  }

  // difference between depEnergy and total cluster charge = noise (due to norm)
  partMap[nullptr] += 1.0 - foundCharge;

  return partMap;
}
