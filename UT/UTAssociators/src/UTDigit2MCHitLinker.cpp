/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCTruth.h"
#include "Event/MCUTDigit.h"
#include "Event/UTDigit.h"
#include "LHCbAlgs/Transformer.h"
#include "UTTruthTool.h"

/**
 *  @author Andy Beiter (based on code by Matt Needham)
 *  @date   2018-09-04
 */
class UTDigit2MCHitLinker
    : public LHCb::Algorithm::Transformer<LHCb::LinksByKey( const LHCb::MCHits&, LHCb::UTDigits const& )> {

public:
  UTDigit2MCHitLinker( const std::string& name, ISvcLocator* pSvc )
      : Transformer( name, pSvc,
                     {{"hitLocation", LHCb::MCHitLocation::UT}, {"InputData", LHCb::UTDigitLocation::UTDigits}},
                     {"OutputData", LHCb::UTDigitLocation::UTDigits + "2MCHits"} ) {}
  LHCb::LinksByKey operator()( const LHCb::MCHits&, LHCb::UTDigits const& ) const override;

private:
  typedef std::pair<const LHCb::MCHit*, double> HitPair;
  typedef std::map<const LHCb::MCHit*, double>  HitMap;
  std::vector<HitPair>                          refsToRelate( const HitMap& hitMap, LHCb::MCHits const& hits ) const;

  Gaudi::Property<bool>   m_addSpillOverHits{this, "AddSpillOverHits", false, "Flag to add spill-over to linker table"};
  Gaudi::Property<double> m_minFrac{this, "Minfrac", 0.05, "Minimal charge fraction to link to MCParticle"};
  Gaudi::Property<bool>   m_oneRef{this, "OneRef", false, "Flag to allow only 1 link for each digit"};
};

DECLARE_COMPONENT( UTDigit2MCHitLinker )
namespace {
  template <typename Container>
  double totalCharge( const Container& hitMap ) {
    return std::accumulate( hitMap.begin(), hitMap.end(), 0.,
                            []( double charge, const auto& p ) { return charge + std::fabs( p.second ); } );
  }
} // namespace

LHCb::LinksByKey UTDigit2MCHitLinker::operator()( LHCb::MCHits const& mcHits, LHCb::UTDigits const& digitCont ) const {
  // create an association table
  LHCb::LinksByKey myLink{std::in_place_type<LHCb::UTDigit>, std::in_place_type<LHCb::MCHit>,
                          LHCb::LinksByKey::Order::decreasingWeight};

  // loop and link UTDigits to MC truth
  for ( const auto* iterDigit : digitCont ) {
    // find all hits
    auto [hitMap, foundCharge] = UTTruthTool::associateToTruth2( iterDigit );
    hitMap[nullptr] += ( iterDigit->depositedCharge() - foundCharge );

    // select references to add to table
    auto selectedRefs = refsToRelate( hitMap, mcHits );
    if ( ( !selectedRefs.empty() ) ) {
      if ( !m_oneRef.value() ) {
        myLink.link( iterDigit, selectedRefs );
      } else {
        auto [p, w] = selectedRefs.back();
        myLink.link( iterDigit, p, w );
      }
    } // refsToRelate ! empty
  }   // loop iterDigit

  return myLink;
}

std::vector<UTDigit2MCHitLinker::HitPair> UTDigit2MCHitLinker::refsToRelate( const HitMap&       hitMap,
                                                                             LHCb::MCHits const& mchits ) const {
  std::vector<HitPair> selectedRefs;
  selectedRefs.reserve( hitMap.size() );
  double totCharge = totalCharge( hitMap );
  // iterate over map
  for ( auto [hit, charge] : hitMap ) {
    double frac = ( totCharge > 0.0 ? std::abs( charge / totCharge ) : -1.0 );
    if ( hit && frac > m_minFrac ) {
      if ( m_addSpillOverHits.value() || &mchits == hit->parent() ) { selectedRefs.emplace_back( hit, frac ); }
    }
  }
  return selectedRefs;
}
