/*****************************************************************************\
* (c) Copyright 2018 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
// Event
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCTruth.h"
#include "Event/MCUTDeposit.h"
#include "Event/MCUTDigit.h"
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "GaudiKernel/Point3DTypes.h"
#include "LHCbMath/GeomFun.h"
#include "LHCbMath/LineTypes.h"

// local
#include "UTTruthTool.h"

using namespace LHCb;
namespace UTTruthTool {

  std::pair<std::map<const MCHit*, double>, double> associateToTruth2( const UTDigit* aDigit ) {
    auto out                    = std::pair<std::map<const MCHit*, double>, double>{};
    auto& [hitMap, foundCharge] = out;
    // make link to truth  to MCHit from Digit
    const MCUTDigit* mcDigit = mcTruth<MCUTDigit>( aDigit );
    if ( mcDigit ) {
      // link to deposits
      std::map<const MCHit*, double> tempMap;
      for ( const auto& iterDep : mcDigit->mcDeposit() ) {
        const MCHit* aHit = iterDep->mcHit();
        foundCharge += iterDep->depositedCharge();
        tempMap[aHit] += iterDep->depositedCharge();
      }
      // clean out the delta-rays
      removeDeltaRays( tempMap, hitMap );
    }
    return out;
  }

  void removeDeltaRays( const std::map<const MCHit*, double>& inputMap, std::map<const MCHit*, double>& hitMap ) {

    // separate into delta ray and not
    std::map<const MCHit*, double> deltaRays;
    for ( auto iter : inputMap ) {
      const MCHit* hit = iter.first;
      if ( hit ) {
        const MCParticle* aParticle = hit->mcParticle();
        if ( aParticle && aParticle->originVertex() != nullptr &&
             aParticle->originVertex()->type() == LHCb::MCVertex::MCVertexType::DeltaRay ) {
          deltaRays.insert( iter );
        } else {
          hitMap.insert( iter );
        }
      } else {
        hitMap.insert( iter );
      }
    } // iter

    // for the delta rays add directly if they are far away from and not related to an existing
    // MCHit, else merge with existing hit

    for ( auto& deltaRay : deltaRays ) {

      auto iter2 = std::find_if( hitMap.begin(), hitMap.end(), [&]( const auto& p ) {
        const MCHit* aHit = p.first;
        return aHit && aHit->mcParticle() == deltaRay.first->mcParticle();
      } );

      if ( iter2 == hitMap.end() ) {

        // no reasonable candidate --> insert
        hitMap.insert( deltaRay );
      } else {
        using namespace Gaudi::Math;
        Gaudi::XYZPoint dPoint     = deltaRay.first->midPoint();
        XYZLine         hitLine    = XYZLine( iter2->first->entry(), iter2->first->exit() );
        const double    distToLine = impactParameter( dPoint, hitLine );
        if ( std::abs( distToLine ) < drayTol ) {
          // its close, merge
          hitMap[iter2->first] += deltaRay.second;
        } else {
          hitMap.insert( deltaRay );
        }
      }
    } // delta rays
  }

  std::map<const MCParticle*, double> associateToTruth( const UTDigit* aDigit ) {
    // make truth link to MCParticle from Digit
    auto [hitMap, foundCharge] = associateToTruth2( aDigit );
    hitMap[nullptr] += aDigit->depositedCharge() - foundCharge;
    std::map<const MCParticle*, double> particleMap;
    for ( auto [aHit, charge] : hitMap ) {
      const MCParticle* aParticle = ( aHit ? aHit->mcParticle() : nullptr );
      particleMap[aParticle] += charge;
    }
    return particleMap;
  }
} // namespace UTTruthTool
