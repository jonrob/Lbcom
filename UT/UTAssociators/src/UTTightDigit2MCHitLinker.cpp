/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "LHCbAlgs/Transformer.h"
#include "Linker/LinkedTo.h"

using namespace LHCb;

/**
 *  @author Andy Beiter (based on code by Matt Needham)
 *  @date   2018-09-04
 */

class UTTightDigit2MCHitLinker : public LHCb::Algorithm::Transformer<LHCb::LinksByKey(
                                     const UTDigits&, const UTDigits&, const MCHits&, const LHCb::LinksByKey& )> {
public:
  UTTightDigit2MCHitLinker( std::string const& name, ISvcLocator* pSvc )
      : Transformer{name,
                    pSvc,
                    {{"InputData", UTDigitLocation::UTTightDigits},
                     {"DigitLocation", UTDigitLocation::UTDigits},
                     {"hitLocation", "/Event/" + MCHitLocation::UT},
                     {"DigitASCTlocation", UTDigitLocation::UTDigits + "2MCHits"}},
                    {"OutputData", LHCb::LinksByKey::linkerName( UTClusterLocation::UTClusters + "2MCHits" )}} {}

  LHCb::LinksByKey operator()( const UTDigits&, const UTDigits&, const MCHits&,
                               const LHCb::LinksByKey& ) const override;

private:
  typedef std::pair<const LHCb::MCHit*, double> HitPair;
  typedef std::map<const LHCb::MCHit*, double>  HitMap;
  std::vector<HitPair>                          refsToRelate( const HitMap& hitMap, const LHCb::MCHits& hits ) const;
  HitMap associateToTruth( const LHCb::UTDigits&, const LHCb::UTDigit&, const LinkedTo<LHCb::MCHit>& ) const;

  Gaudi::Property<bool>   m_addSpillOverHits{this, "AddSpillOverHits", false, "Flag to add spill-over to linker table"};
  Gaudi::Property<double> m_minFrac{this, "Minfrac", 0.2, "Minimal charge fraction to link to MCParticle"};
  Gaudi::Property<bool>   m_oneRef{this, "OneRef", false, "Flag to allow only 1 link for each cluster"};
};

DECLARE_COMPONENT( UTTightDigit2MCHitLinker )

LHCb::LinksByKey UTTightDigit2MCHitLinker::operator()( const UTDigits& clusterCont, const UTDigits& digitCont,
                                                       const MCHits& mcHits, const LHCb::LinksByKey& links ) const {
  // create a linker
  auto myLink = LHCb::LinksByKey{std::in_place_type<UTDigit>, std::in_place_type<MCHit>,
                                 LHCb::LinksByKey::Order::decreasingWeight};

  // make link to truth  to MCHit from cluster
  // TODO: can we really delete this? This algo does require MCHits as input, so they must exist... :
  // links->resolveLinks( evtSvc() );
  const auto aTable = LinkedTo<MCHit>{&links};

  // loop and link UTClusters to MC truth
  for ( auto iterClus : clusterCont ) {
    // find all hits
    // select references to add to table
    auto selectedRefs = refsToRelate( associateToTruth( digitCont, *iterClus, aTable ), mcHits );
    if ( !selectedRefs.empty() ) {
      if ( !m_oneRef.value() ) {
        myLink.link( iterClus, selectedRefs );
      } else {
        auto [p, w] = selectedRefs.back();
        myLink.link( iterClus, p, w );
      }
    } // refsToRelate ! empty
  }   // loop iterClus
  return myLink;
}

std::vector<UTTightDigit2MCHitLinker::HitPair> UTTightDigit2MCHitLinker::refsToRelate( const HitMap& hitMap,
                                                                                       const MCHits& hits ) const {
  std::vector<HitPair> selRefs;
  for ( auto [aHit, weight] : hitMap ) {
    if ( aHit && weight > m_minFrac ) {
      if ( m_addSpillOverHits.value() || &hits == aHit->parent() ) { selRefs.emplace_back( aHit, weight ); }
    }
  }
  return selRefs;
}

UTTightDigit2MCHitLinker::HitMap
UTTightDigit2MCHitLinker::associateToTruth( const LHCb::UTDigits& digitCont, const UTDigit& aCluster,
                                            const LinkedTo<LHCb::MCHit>& aTable ) const {
  HitMap         hitMap;
  auto           id     = aCluster.channelID();
  const UTDigit* aDigit = digitCont.object( id );
  if ( aDigit ) {
    double foundCharge = 0.;
    for ( const auto& [aHit, weight] : aTable.weightedRange( aDigit ) ) {
      hitMap[&aHit] += weight * aDigit->depositedCharge();
      foundCharge += weight * aDigit->depositedCharge();
    }
    hitMap[nullptr] += aDigit->depositedCharge() - foundCharge;
  } // Digit

  // renormalize to 1
  for ( auto& [_, w] : hitMap ) w /= aCluster.depositedCharge();

  return hitMap;
}
