/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/UTDigit.h"
#include "LHCbAlgs/Transformer.h"
#include "UTTruthTool.h"

/**
 *  @author Andy Beiter (based on code by Matt Needham)
 *  @date   2018-09-04
 */

class UTDigit2MCParticleLinker
    : public LHCb::Algorithm::Transformer<LHCb::LinksByKey( LHCb::MCParticles const&, LHCb::UTDigits const& )> {

public:
  UTDigit2MCParticleLinker( const std::string& name, ISvcLocator* pSvc )
      : Transformer(
            name, pSvc,
            {{"MCParticleLcoation", LHCb::MCParticleLocation::Default}, {"InputData", LHCb::UTDigitLocation::UTDigits}},
            {"OutputData", LHCb::UTDigitLocation::UTDigits} ) {}

  LHCb::LinksByKey operator()( LHCb::MCParticles const&, LHCb::UTDigits const& ) const override;

private:
  typedef std::pair<const LHCb::MCParticle*, double> PartPair;
  typedef std::map<const LHCb::MCParticle*, double>  ParticleMap;
  std::vector<PartPair>&                             refsToRelate( const ParticleMap& hitMap, const double totCharge,
                                                                   LHCb::MCParticles* particles ) const;

  Gaudi::Property<bool>   m_addSpillOverHits{this, "AddSpillOverHits", false, "Flag to add spill-over to linker table"};
  Gaudi::Property<double> m_minFrac{this, "Minfrac", 0.05, "Minimal charge fraction to link to MCParticle"};
  Gaudi::Property<bool>   m_oneRef{this, "OneRef", false, "Flag to allow only 1 link for each digit"};
};

DECLARE_COMPONENT( UTDigit2MCParticleLinker )
namespace {
  template <typename Container>
  double totalCharge( const Container& partMap ) {
    return std::accumulate( partMap.begin(), partMap.end(), 0.0,
                            []( double charge, const auto& p ) { return charge + std::abs( p.second ); } );
  }
} // namespace

LHCb::LinksByKey UTDigit2MCParticleLinker::operator()( LHCb::MCParticles const& mcParts,
                                                       LHCb::UTDigits const&    digitCont ) const {
  // creating a linker
  auto myLink = LHCb::LinksByKey{std::in_place_type<LHCb::UTDigits>, std::in_place_type<LHCb::MCParticle>,
                                 LHCb::LinksByKey::Order::decreasingWeight};

  // loop and link UTDigits to MC truth
  for ( auto iterDigit : digitCont ) {
    // find all particles
    auto partMap = UTTruthTool::associateToTruth( iterDigit );
    // select references to add to table
    double                totCharge = totalCharge( partMap );
    std::vector<PartPair> selectedRefs;
    selectedRefs.reserve( partMap.size() );
    for ( auto [particle, charge] : partMap ) {
      double frac = ( totCharge > 0.0 ? charge / totCharge : -1.0 );
      if ( particle && frac > m_minFrac ) {
        if ( m_addSpillOverHits.value() || &mcParts == particle->parent() ) {
          selectedRefs.emplace_back( particle, frac );
        }
      }
    } // iterMap
    if ( selectedRefs.empty() ) continue;
    if ( !m_oneRef.value() ) {
      myLink.link( iterDigit, selectedRefs );
    } else {
      auto [p, w] = selectedRefs.back();
      myLink.link( iterDigit, p, w );
    }
  } // loop iterDigit
  return myLink;
}
