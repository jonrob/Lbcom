/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "GaudiAlg/GaudiTool.h"
#include "Kernel/UTFun.h"
#include "LHCbMath/LHCbMath.h"
#include "TrackInterfaces/IUTClusterPosition.h"
#include <cmath>

using namespace LHCb;

/** @class UTOnlinePosition UTOnlinePosition.h
 *
 *  Tool for calculating offline cluster position
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTOnlinePosition : public extends<GaudiTool, IUTClusterPosition> {

public:
  using extends::extends;

  IUTClusterPosition::Info estimate( const DeUTDetector&, const DeMagnet& magnet,
                                     const LHCb::UTCluster* aCluster ) const override;
  IUTClusterPosition::Info estimate( const DeUTDetector&, const DeMagnet& magnet,
                                     const SmartRefVector<LHCb::UTDigit>& digits ) const override;
  double                   error( const unsigned int nStrips ) const override;

private:
  // job options
  Gaudi::Property<std::vector<double>> m_errorVec{this, "ErrorVec", {0.22, 0.14, 0.25}}; ///< Error parametrized by
                                                                                         ///< cluster size

  /// alignment
  Gaudi::Property<double> m_APE{this, "APE", 0.0};
};

DECLARE_COMPONENT( UTOnlinePosition )

IUTClusterPosition::Info UTOnlinePosition::estimate( const DeUTDetector&, const DeMagnet&,
                                                     const LHCb::UTCluster* aCluster ) const {
  double stripNum      = UTFun::position( aCluster->stripValues() ).first;
  double interStripPos = UTFun::stripFraction( stripNum - floor( stripNum ) );

  // Increase strip number by one when interstrip fraction equals 1
  if ( interStripPos > 0.99 ) {
    stripNum += 1;
    interStripPos = 0;
  }

  Detector::UT::ChannelID firstChan = aCluster->firstChannel();
  Detector::UT::ChannelID theChan( firstChan.channelID() + (unsigned int)stripNum );

  IUTClusterPosition::Info theInfo;
  theInfo.strip              = theChan;
  theInfo.fractionalPosition = interStripPos;
  theInfo.fractionalError    = error( aCluster->size() );
  theInfo.clusterSize        = aCluster->size();
  return theInfo;
}

IUTClusterPosition::Info UTOnlinePosition::estimate( const DeUTDetector&, const DeMagnet&,
                                                     const SmartRefVector<LHCb::UTDigit>& digits ) const {
  double                        stripNum      = UTFun::position( digits ).first;
  double                        interStripPos = UTFun::stripFraction( stripNum - floor( stripNum ) );
  LHCb::Detector::UT::ChannelID firstChan     = digits.front()->channelID();

  // Increase strip number by one when interstrip fraction equals 1
  if ( interStripPos > 0.99 ) {
    stripNum += 1;
    interStripPos = 0;
  }

  Detector::UT::ChannelID theChan( firstChan.channelID() - firstChan.strip() + (unsigned int)stripNum );

  IUTClusterPosition::Info theInfo;
  theInfo.strip              = theChan;
  theInfo.fractionalPosition = interStripPos;
  theInfo.fractionalError    = error( digits.size() );
  theInfo.clusterSize        = digits.size();
  return theInfo;
}

double UTOnlinePosition::error( const unsigned int nStrips ) const {
  // estimate of error
  double eValue = ( nStrips < m_errorVec.size() ? m_errorVec[nStrips - 1] : m_errorVec.value().back() );
  if ( m_APE > 0.0 ) { eValue = sqrt( eValue * eValue + m_APE * m_APE ); }
  return eValue;
}
