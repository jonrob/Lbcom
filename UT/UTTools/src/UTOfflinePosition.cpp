/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "GaudiAlg/GaudiTool.h"
#include "GaudiKernel/Point3DTypes.h"
#include "GaudiKernel/SystemOfUnits.h"
#include "GaudiKernel/Vector3DTypes.h"
#include "Kernel/ILHCbMagnetSvc.h"
#include "Kernel/UTDataFunctor.h"
#include "Kernel/UTFun.h"
#include "LHCbMath/Lomont.h"
#include "Magnet/DeMagnet.h"
#include "TrackInterfaces/IUTClusterPosition.h"
#include "UTDet/DeUTSector.h"
#include <cmath>

using namespace LHCb;
using namespace LHCb::Math;

/** @class UTOfflinePosition UTOfflinePosition.h
 *
 *  Tool for calculating offline cluster position
 *
 *  @author A. Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTOfflinePosition : public extends<GaudiTool, IUTClusterPosition> {

public:
  using extends::extends;

  StatusCode initialize() override;

  IUTClusterPosition::Info estimate( const DeUTDetector&, const DeMagnet& magnet,
                                     const LHCb::UTCluster* aCluster ) const override;
  IUTClusterPosition::Info estimate( const DeUTDetector&, const DeMagnet& magnet,
                                     const SmartRefVector<LHCb::UTDigit>& digits ) const override;
  double                   error( const unsigned int nStrips ) const override;

private:
  double stripFraction( const double stripNum, const unsigned int clusterSize ) const;

  void lorentzShift( const DeUTDetector&, const DeMagnet& magnet, const LHCb::Detector::UT::ChannelID& chan,
                     double& fracPosition ) const;

  /// UTCluster container, needed to merge splitted clusters
  DataObjectReadHandle<LHCb::UTClusters> m_clusters{this, "InputData", LHCb::UTClusterLocation::UTClusters};

  // job options
  Gaudi::Property<std::vector<double>> m_errorVec{
      this, "ErrorVec", {0.257, 0.245, 0.277, 0.208}, "Error parametrized by cluster size"};
  Gaudi::Property<double> m_linSharingCorr2{this, "LinSharingCorr2", 0.370, "Corr factor linear term 2-strip clusters"};
  Gaudi::Property<double> m_cubicSharingCorr2{this, "CubicSharingCorr2", 15.4,
                                              "Corr factor cubic term 2-strip clusters"};
  Gaudi::Property<double> m_cubicSharingCorr3{this, "CubicSharingCorr3", 4.433,
                                              "Corr factor cubic term 3-strip clusters"};
  Gaudi::Property<double> m_linSharingCorr4{this, "LinSharingCorr4", 0.564, "Corr factor linear term 4-strip clusters"};
  Gaudi::Property<int>    m_maxNtoCorr{this, "MaxNtoCorr", 4, "Maximum size of cluster for S-shape corr"};
  Gaudi::Property<double> m_trim{this, "trim", 0.0, "Trimming value to suppress cap. coupling"};
  Gaudi::Property<bool>   m_mergeClusters{this, "MergeClusters", false, "Flag to merge split clusters"};
  Gaudi::Property<double> m_APE{this, "APE", 0.0, "Alignment Precision Error"};

  Gaudi::Property<bool>   m_applyLorentzCorrection{this, "applyLorentzCorrection", true};
  Gaudi::Property<double> m_lorentzFactor{this, "lorentzFactor", 0.025 / Gaudi::Units::tesla};
};

DECLARE_COMPONENT( UTOfflinePosition )

StatusCode UTOfflinePosition::initialize() {
  return GaudiTool::initialize().andThen( [&] {
    info() << "APE set to " << m_APE << endmsg;
    info() << "Error Vec ";
    for ( double val : m_errorVec ) info() << val << " ";
    info() << endmsg;
  } );
}

IUTClusterPosition::Info UTOfflinePosition::estimate( const DeUTDetector& det, const DeMagnet& magnet,
                                                      const UTCluster* aCluster ) const {
  UTCluster::ADCVector    adcVector;
  Detector::UT::ChannelID firstChan;

  // Merge adc values of neighbouring clusters
  if ( m_mergeClusters ) {

    // Get the clusters
    const LHCb::UTClusters* clusters = m_clusters.get();

    // Get the iterator belonging to the current cluster
    auto iterClus = std::lower_bound( clusters->begin(), clusters->end(), aCluster,
                                      UTDataFunctor::Less_by_Channel<const UTCluster*>() );

    // Find the left neighbour
    int firstChannel = ( *iterClus )->firstChannel();
    --iterClus;
    while ( iterClus >= clusters->begin() && firstChannel - ( *iterClus )->lastChannel() == 1 ) {
      firstChannel = ( *iterClus )->firstChannel();
      --iterClus;
    }
    ++iterClus;

    // Add the adc values and save the first channel
    adcVector = ( *iterClus )->stripValues();
    firstChan = ( *iterClus )->firstChannel();

    // Add the right neighbours
    int lastChannel = ( *iterClus )->lastChannel();
    ++iterClus;
    while ( iterClus != clusters->end() && ( *iterClus )->firstChannel() - lastChannel == 1 ) {
      adcVector.insert( adcVector.end(), ( *iterClus )->stripValues().begin(), ( *iterClus )->stripValues().end() );
      lastChannel = ( *iterClus )->lastChannel();
      ++iterClus;
    }

  } else { // No merging of neighbouring clusters
    adcVector = aCluster->stripValues();
    firstChan = aCluster->firstChannel();
  }

  auto   info     = UTFun::position( adcVector, m_trim );
  double stripNum = info.first;

  Detector::UT::ChannelID theChan( firstChan.channelID() + ( (unsigned int)stripNum ) );

  double stripFrac = stripFraction( stripNum - floor( stripNum ), info.second );

  if ( m_applyLorentzCorrection.value() ) lorentzShift( det, magnet, theChan, stripFrac );

// FIXME: C++20: remove pragmas
// designated initializers are part of C99 (so both clang and gcc support them)
// and C++20, but when using C++17, they generate a '-Wpedantic' warning.
#pragma GCC diagnostic push
#if __GNUC__ >= 12
#  pragma GCC diagnostic ignored "-Wc++20-extensions"
#else
#  pragma GCC diagnostic ignored "-Wpedantic"
#endif
  return IUTClusterPosition::Info{.strip              = theChan,
                                  .fractionalPosition = stripFrac,
                                  .fractionalError    = error( info.second ),
                                  .clusterSize        = info.second};
#pragma GCC diagnostic pop
}

IUTClusterPosition::Info UTOfflinePosition::estimate( const DeUTDetector& det, const DeMagnet& magnet,
                                                      const SmartRefVector<UTDigit>& digits ) const {
  auto   info     = UTFun::position( digits, m_trim );
  double stripNum = info.first;

  Detector::UT::ChannelID firstChan = digits.front()->channelID();
  Detector::UT::ChannelID theChan( firstChan.channelID() - firstChan.strip() + (unsigned int)stripNum );

  double stripFrac = stripFraction( stripNum - floor( stripNum ), info.second );

  if ( m_applyLorentzCorrection.value() ) lorentzShift( det, magnet, theChan, stripFrac );

// FIXME: C++20: remove pragmas
// designated initializers are part of C99 (so both clang and gcc support them)
// and C++20, but when using C++17, they generate a '-Wpedantic' warning.
#pragma GCC diagnostic push
#if __GNUC__ >= 12
#  pragma GCC diagnostic ignored "-Wc++20-extensions"
#else
#  pragma GCC diagnostic ignored "-Wpedantic"
#endif
  return IUTClusterPosition::Info{.strip              = theChan,
                                  .fractionalPosition = stripFrac,
                                  .fractionalError    = error( info.second ),
                                  .clusterSize        = info.second};
#pragma GCC diagnostic pop
}

double UTOfflinePosition::error( const unsigned int nStrips ) const {
  // estimate of error
  double eValue = ( nStrips < m_errorVec.size() ? m_errorVec[nStrips - 1] : m_errorVec.value().back() );
  if ( m_APE > 0.0 ) eValue = sqrt( eValue * eValue + m_APE * m_APE );
  return eValue;
}

double UTOfflinePosition::stripFraction( const double stripNum, const unsigned int clusterSize ) const {
  // 'S- shape correction' for non-linear charge sharing
  double corStripPos = stripNum - floor( stripNum );
  if ( ( clusterSize > 1 ) && ( (int)clusterSize <= m_maxNtoCorr ) ) {

    // Charge sharing correction
    if ( clusterSize == 2 ) {
      // Linear plus cubic term
      corStripPos =
          ( corStripPos - 0.5 ) * m_linSharingCorr2 + pow( ( corStripPos - 0.5 ), 3.0 ) * m_cubicSharingCorr2 + 0.5;
    } else if ( clusterSize == 3 ) {
      // Cubic term
      corStripPos = pow( ( corStripPos - 0.5 ), 3.0 ) * m_cubicSharingCorr3 + 0.5;
    } else if ( clusterSize == 4 ) {
      // Linear term only
      corStripPos = ( corStripPos - 0.5 ) * m_linSharingCorr4 + 0.5;
    }
  }

  // Corrected position should be between 0 and 1
  if ( corStripPos < 0.0 ) corStripPos = 0.0;
  if ( corStripPos > 1.0 ) corStripPos = 1.0;

  return corStripPos;
}

void UTOfflinePosition::lorentzShift( const DeUTDetector& det, const DeMagnet& magnet,
                                      const Detector::UT::ChannelID& chan, double& fracPosition ) const {
  // Get the local By
#ifdef USE_DD4HEP
  auto sector = det.findSector( chan );
#else
  auto& sector = *det.findSector( chan );
#endif
  Gaudi::XYZVector field   = magnet.fieldVector( sector.globalCentre() );
  double           localBy = field.Dot( sector.normalY().Unit() );

  // note fracPosition is in "global" frame (pitch units),
  // while lorentz shift is in local frame (mm)
  double dx = 0.5 * sector.thickness() * localBy * m_lorentzFactor / sector.pitch();
  if ( !sector.xInverted() ) dx *= -1.0;
  fracPosition += dx;
}
