/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <DetDesc/GenericConditionAccessorHolder.h>
#include <Detector/UT/ChannelID.h>
#include <GaudiAlg/GaudiTool.h>
#include <Kernel/IUTChannelIDSelector.h>
#include <Kernel/IUTReadoutTool.h>
#include <algorithm>
#include <vector>

/** @class UTSelectChannelIDByBoard UTSelectChannelIDByBoard.h
 *
 *  Tool for selecting clusters using a list of service boxes
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectChannelIDByBoard
    : public extends<LHCb::DetDesc::ConditionAccessorHolder<GaudiTool>, IUTChannelIDSelector> {

public:
  using extends::extends;
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

private:
  Gaudi::Property<std::vector<unsigned int>>   m_boards{this, "Boards", {}};
  ConditionAccessor<std::vector<unsigned int>> m_sectors{this, name() + "-CachedSectorIDs"};
  ToolHandle<IUTReadoutTool>                   m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};
};

DECLARE_COMPONENT( UTSelectChannelIDByBoard )

StatusCode UTSelectChannelIDByBoard::initialize() {
  return extends::initialize().andThen( [&] {
    addConditionDerivation(
        {m_readoutTool->getReadoutInfoKey()}, m_sectors.key(), [&]( const IUTReadoutTool::ReadoutInfo& roInfo ) {
          // to save time get a list of all the ids on this box
          std::vector<unsigned int> sectors( 8 * m_boards.size() );
          for ( unsigned int id : m_boards ) {
            const auto& boardSectors = m_readoutTool->sectorIDs( UTTell1ID( id ), &roInfo );
            std::transform( boardSectors.begin(), boardSectors.end(), std::back_inserter( sectors ),
                            []( const LHCb::Detector::UT::ChannelID& chan ) { return chan.uniqueSector(); } );
          } // for each board
          std::sort( sectors.begin(), sectors.end() );
          return sectors;
        } );
  } );
}

bool UTSelectChannelIDByBoard::select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const {
  return ( *this )( id, det );
}

bool UTSelectChannelIDByBoard::operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const {
  const auto& sectors = m_sectors.get();
  return std::binary_search( sectors.begin(), sectors.end(), id.uniqueSector() );
}
