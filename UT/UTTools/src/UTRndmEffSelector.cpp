/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Detector/UT/ChannelID.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"
#include "Kernel/IUTChannelIDSelector.h"
#include "UTDet/DeUTSector.h"

#include "GaudiAlg/GaudiTool.h"

/**
 *  Tool for selecting clusters at random using the measured eff
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTRndmEffSelector : public extends<GaudiTool, IUTChannelIDSelector> {
public:
  using extends::extends;
  StatusCode initialize() override;

  /**  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to calo cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& ) const override;

private:
  // smart interface to generator
  SmartIF<IRndmGen> m_uniformDist;

  // errors
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_GeneratorInitFailure{this, "Failed to init generator"};
};

DECLARE_COMPONENT( UTRndmEffSelector )

StatusCode UTRndmEffSelector::initialize() {
  return extends::initialize().andThen( [&]() -> StatusCode {
    /// initialize, flat generator...
    auto tRandNumSvc = service<IRndmGenSvc>( "RndmGenSvc", true );
    m_uniformDist    = tRandNumSvc->generator( Rndm::Flat( 0., 1.0 ) );
    if ( !m_uniformDist ) ++m_GeneratorInitFailure;
    return StatusCode::SUCCESS;
  } );
}

bool UTRndmEffSelector::select( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const {
  return ( *this )( id, det );
}

bool UTRndmEffSelector::operator()( LHCb::Detector::UT::ChannelID const& id, DeUTDetector const& det ) const {
  auto sector = det.findSector( id );
#ifdef USE_DD4HEP
  auto fractionToReject = 1.0 - sector.measEff();
#else
  auto fractionToReject = 1.0 - sector->measEff();
#endif
  return m_uniformDist->shoot() < fractionToReject;
}
