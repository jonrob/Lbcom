/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "GaudiKernel/IRndmGen.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "GaudiKernel/SmartIF.h"
#include "Kernel/UTFun.h"
#include "LHCbMath/LHCbMath.h"
#include "TrackInterfaces/IUTClusterPosition.h"

#include "GaudiAlg/GaudiTool.h"

#include <cmath>

using namespace LHCb;

/** @class UTSmearedPosition UTSmearedPosition.h
 *
 *  Tool for smearing cluster position, it takes as input the
 *  result of the online or offline tool and then adds an error
 *  in quadrature based on the cluster size
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSmearedPosition : public extends<GaudiTool, IUTClusterPosition> {

public:
  using extends::extends;

  StatusCode initialize() override;

  IUTClusterPosition::Info estimate( const DeUTDetector&, const DeMagnet& magnet,
                                     const LHCb::UTCluster* aCluster ) const override;
  IUTClusterPosition::Info estimate( const DeUTDetector&, const DeMagnet& magnet,
                                     const SmartRefVector<LHCb::UTDigit>& digits ) const override;
  double                   error( const unsigned int nStrips ) const override;

private:
  Gaudi::Property<std::string> m_baseToolName{this, "BaseToolName", "UTClusterPosition"};
  Gaudi::Property<std::string> m_baseToolType{this, "BaseToolType", "UTOfflinePosition"};
  IUTClusterPosition*          m_baseTool{nullptr};

  Gaudi::Property<std::vector<double>> m_corrections{this, "CorrectionsVec", {0.0, 0.0, 0.0, 0.0}};

  SmartIF<IRndmGen> m_gaussDist;

  double applySmear( double value, unsigned int nStrips ) const;

  // errors
  mutable Gaudi::Accumulators::MsgCounter<MSG::ERROR> m_GeneratorInitFailure{this, "Failed to init generator"};
};

DECLARE_COMPONENT( UTSmearedPosition )

StatusCode UTSmearedPosition::initialize() {
  StatusCode sc = extends::initialize();
  if ( sc.isFailure() ) return sc;

  // the underlying tool
  m_baseTool = tool<IUTClusterPosition>( m_baseToolType, m_baseToolName );

  // get the gaussian generator
  auto tRandNumSvc = service<IRndmGenSvc>( "RndmGenSvc", true );
  m_gaussDist      = tRandNumSvc->generator( Rndm::Gauss( 0., 1.0 ) );
  if ( !m_gaussDist ) ++m_GeneratorInitFailure;

  return sc;
}

double UTSmearedPosition::applySmear( double value, unsigned int nStrips ) const {
  double corrValue = 0.0;
  corrValue        = ( nStrips < m_corrections.size() ? m_corrections[nStrips - 1] : m_corrections.value().back() );
  corrValue *= m_gaussDist->shoot();
  return sqrt( corrValue * corrValue + value * value );
}

IUTClusterPosition::Info UTSmearedPosition::estimate( const DeUTDetector& det, const DeMagnet& magnet,
                                                      const UTCluster* aCluster ) const {
  IUTClusterPosition::Info theInfo    = m_baseTool->estimate( det, magnet, aCluster );
  const double             newFracPos = applySmear( theInfo.fractionalPosition, theInfo.clusterSize );
  theInfo.fractionalPosition          = newFracPos;
  return theInfo;
}

IUTClusterPosition::Info UTSmearedPosition::estimate( const DeUTDetector& det, const DeMagnet& magnet,
                                                      const SmartRefVector<LHCb::UTDigit>& digits ) const {

  IUTClusterPosition::Info theInfo    = m_baseTool->estimate( det, magnet, digits );
  const double             newFracPos = applySmear( theInfo.fractionalPosition, theInfo.clusterSize );
  theInfo.fractionalPosition          = newFracPos;
  return theInfo;
}

double UTSmearedPosition::error( const unsigned int nStrips ) const {
  // estimate of error
  double corrValue =
      ( nStrips < m_corrections.value().size() ? m_corrections[nStrips - 1] : m_corrections.value().back() );

  // get the value from the underlying tool
  const double eValue = m_baseTool->error( nStrips );

  return sqrt( eValue * eValue + corrValue * corrValue );
}
