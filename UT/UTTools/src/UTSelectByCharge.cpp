/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Kernel/IUTClusterSelector.h"
#include "boost/numeric/conversion/bounds.hpp"

#include "GaudiAlg/GaudiTool.h"

/**
 *  Tool for selecting clusters by charge
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTSelectByCharge : public extends<GaudiTool, IUTClusterSelector> {

public:
  using extends::extends;
  StatusCode initialize() override;

  /**  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool select( LHCb::UTCluster const& cluster, DeUTDetector const& ) const override;

  /** "select"/"preselect" method (functor interface)
   *  @param  cluster pointer to UT cluster object to be selected
   *  @return true if cluster is selected
   */
  bool operator()( LHCb::UTCluster const& cluster, DeUTDetector const& ) const override;

private:
  Gaudi::Property<double> m_minCharge{this, "minCharge", 0.0};
  Gaudi::Property<double> m_maxCharge{this, "maxCharge", boost::numeric::bounds<double>::highest()};
};

DECLARE_COMPONENT( UTSelectByCharge )

StatusCode UTSelectByCharge::initialize() {
  return extends::initialize().andThen(
      [&] { info() << "Min Charge set to " << m_minCharge << " / max charge set to " << m_maxCharge << endmsg; } );
}

bool UTSelectByCharge::select( LHCb::UTCluster const& cluster, DeUTDetector const& det ) const {
  return ( *this )( cluster, det );
}

bool UTSelectByCharge::operator()( LHCb::UTCluster const& cluster, DeUTDetector const& ) const {
  const double charge = cluster.totalCharge();
  return charge > m_minCharge && charge < m_maxCharge;
}
