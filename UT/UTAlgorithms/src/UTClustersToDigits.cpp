/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "UTDet/DeUTDetector.h"

#include "Event/UTCluster.h"
#include "Event/UTDigit.h"
#include "Event/UTLiteCluster.h"

#include "LHCbAlgs/Transformer.h"

#include <algorithm>

/**
 *  simple class for stripping digits from clusters
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */
class UTClustersToDigits
    : public LHCb::Algorithm::Transformer<LHCb::UTDigit::Container( LHCb::UTClusters const&, DeUTDetector const& ),
                                          LHCb::DetDesc::usesConditions<DeUTDetector>> {

public:
  UTClustersToDigits( const std::string& name, ISvcLocator* pSvcLocator )
      : Transformer( name, pSvcLocator,
                     {KeyValue{"inputLocation", LHCb::UTClusterLocation::UTClusters},
                      KeyValue{"DeUTLocation", DeUTDetLocation::location()}},
                     KeyValue{"outputLocation", LHCb::UTDigitLocation::UTDigits} ) {}

  LHCb::UTDigit::Container operator()( LHCb::UTClusters const&, DeUTDetector const& ) const override;

private:
  void addNeighbours( LHCb::UTCluster* cluster, LHCb::UTDigit::Container& digitCont, DeUTDetector const& deUT ) const;
  Gaudi::Property<bool> m_addNeighbourInfo{this, "addNeighbourInfo", true};
};

LHCb::UTDigit::Container UTClustersToDigits::operator()( LHCb::UTClusters const& clusterCont,
                                                         DeUTDetector const&     deUT ) const {
  LHCb::UTDigit::Container dCont{};
  dCont.reserve( clusterCont.size() * 4u );

  for ( const auto& iterC : clusterCont ) {
    // get vector of channels + adc
    const auto& vec = iterC->channels();
    for ( unsigned int iStrip = 0; iStrip < vec.size(); ++iStrip ) {
      auto key = vec[iStrip];
      if ( dCont.object( key ) == 0 ) { dCont.insert( new LHCb::UTDigit( iterC->adcValue( iStrip ) ), key ); }
    } // loop channels
    // add neighbour info
    if ( m_addNeighbourInfo.value() ) addNeighbours( iterC, dCont, deUT );
  } // loop clusters

  std::sort( dCont.begin(), dCont.end(),
             []( const auto* l, const auto* r ) { return l->channelID() < r->channelID(); } );

  return dCont;
}

inline void UTClustersToDigits::addNeighbours( LHCb::UTCluster* clus, LHCb::UTDigit::Container& digitCont,
                                               DeUTDetector const& deUT ) const {
  // use the neighbour sum to add information on the neighbouring strips..

  // left + right neighbour
  LHCb::Detector::UT::ChannelID leftChan  = deUT.nextLeft( clus->firstChannel() );
  LHCb::Detector::UT::ChannelID rightChan = deUT.nextRight( clus->firstChannel() );

  double neighbourSum = clus->neighbourSum();
  if ( leftChan != 0u && rightChan != 0u ) {
    neighbourSum *= 0.5;
    if ( !digitCont.object( leftChan ) ) { digitCont.insert( new LHCb::UTDigit( neighbourSum ), leftChan ); }
    if ( !digitCont.object( rightChan ) ) { digitCont.insert( new LHCb::UTDigit( neighbourSum ), rightChan ); }
  } else if ( leftChan != 0u ) {
    if ( !digitCont.object( leftChan ) ) { digitCont.insert( new LHCb::UTDigit( neighbourSum ), leftChan ); }
  } else if ( rightChan != 0u ) {
    if ( !digitCont.object( rightChan ) ) { digitCont.insert( new LHCb::UTDigit( neighbourSum ), rightChan ); }
  } else {
    // nothing
  }
}

DECLARE_COMPONENT_WITH_ID( UTClustersToDigits, "UTClustersToDigits" )
