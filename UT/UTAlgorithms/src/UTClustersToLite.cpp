/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/UTCluster.h"
#include "Event/UTLiteCluster.h"

#include "LHCbAlgs/Transformer.h"

/**
 *  simple class for stripping liteCluster from clusters
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */
struct UTClustersToLite
    : Gaudi::Functional::Transformer<LHCb::UTLiteCluster::UTLiteClusters( const LHCb::UTClusters& )> {
  UTClustersToLite( const std::string& name, ISvcLocator* pSvcLocator );
  LHCb::UTLiteCluster::UTLiteClusters operator()( const LHCb::UTClusters& ) const override;
};

LHCb::UTLiteCluster::UTLiteClusters UTClustersToLite::operator()( const LHCb::UTClusters& clusterCont ) const {
  LHCb::UTLiteCluster::UTLiteClusters fCont;
  fCont.reserve( clusterCont.size() );
  std::transform( clusterCont.begin(), clusterCont.end(), std::back_inserter( fCont ),
                  []( const auto& c ) { return c->liteCluster(); } );
  return fCont;
}

UTClustersToLite::UTClustersToLite( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator, {KeyValue{"inputLocation", LHCb::UTClusterLocation::UTClusters}},
                   KeyValue{"outputLocation", LHCb::UTLiteClusterLocation::UTClusters} ) {}

DECLARE_COMPONENT( UTClustersToLite )
