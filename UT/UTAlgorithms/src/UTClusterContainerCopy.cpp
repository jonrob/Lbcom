/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Detector/UT/ChannelID.h"
#include "Event/UTCluster.h"
#include "Kernel/IUTClusterSelector.h"
#include "UTDet/DeUTDetector.h"

#include "LHCbAlgs/Transformer.h"

#include "Gaudi/Accumulators.h"

#include <string>

/**
 *  Class for UTClusterContainerCopy
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 */

class UTClusterContainerCopy
    : public LHCb::Algorithm::Transformer<LHCb::UTClusters( const LHCb::UTClusters&, DeUTDetector const& ),
                                          LHCb::DetDesc::usesConditions<DeUTDetector>> {

public:
  UTClusterContainerCopy( const std::string& name, ISvcLocator* pSvcLocator );
  LHCb::UTClusters operator()( const LHCb::UTClusters& clusterCont, DeUTDetector const& ) const override;
  StatusCode       finalize() override;

private:
  PublicToolHandle<IUTClusterSelector> m_clusterSelector{this, "Selector", "UTSelectClustersByChannel/UTSelector"};
  mutable Gaudi::Accumulators::BinomialCounter<> m_selected{this, "fraction of clusters selected"};
};

DECLARE_COMPONENT( UTClusterContainerCopy )

UTClusterContainerCopy::UTClusterContainerCopy( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer{name,
                  pSvcLocator,
                  {{"InputLocation", LHCb::UTClusterLocation::UTClusters}, {"UTLocation", DeUTDetLocation::location()}},
                  {"OutputLocation", LHCb::UTClusterLocation::UTClusters + "Selected"}} {}

LHCb::UTClusters UTClusterContainerCopy::operator()( const LHCb::UTClusters& clusterCont,
                                                     DeUTDetector const&     det ) const {
  LHCb::UTClusters outputCont;
  auto             selected = m_selected.buffer();
  for ( const auto& iterC : clusterCont ) {
    const bool select = ( *m_clusterSelector )( *iterC, det );
    selected += select;
    if ( select ) outputCont.insert( iterC->clone(), iterC->channelID() );
  }
  return outputCont;
}

StatusCode UTClusterContainerCopy::finalize() {
  info() << "Fraction of clusters selected " << 100 * m_selected.efficiency() << " %" << endmsg;
  return Transformer::finalize();
}
