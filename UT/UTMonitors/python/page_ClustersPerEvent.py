###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
# To run this script you need to set JOBOPTSEARCHPATH
# to the correct path

from Gaudi.Configuration import *
from Configurables import CreateHistDBPages
createHistAlg = CreateHistDBPages("createHistAlg")

tae = "Central"
folderBase = "Summary"
pageName = "ClustersPerEvent"

histoName = "TT-Cluster_Number"

createHistAlg.PageNames = []
createHistAlg.HistoNames = []

createHistAlg.PageNames.append("/" + folderBase + "/" + pageName)
folderHistos = [tae + "/" + histoName + "/" + tae]
createHistAlg.HistoNames.append(folderHistos)

importOptions("inc_layout1hist.py")
importOptions("incTT_genOpts.py")
