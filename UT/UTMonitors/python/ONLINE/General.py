from __future__ import print_function
###############################################################################
# (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
from Gaudi.Configuration import *


def patchMessages():
    """
        Messages in the online get redirected.
        Setup here the FMC message service

        @author M.Frank
  """
    import Configurables as Configs
    app = ApplicationMgr()
    Configs.AuditorSvc().Auditors = []
    app.MessageSvcType = 'LHCb::FmcMessageSvc'
    try:
        del allConfigurables['MessageSvc']
    except KeyError:
        pass
    msg = Configs.LHCb__FmcMessageSvc('MessageSvc')
    msg.LoggerOnly = True
    msg.fifoPath = os.environ['LOGFIFO']
    msg.OutputLevel = 3
    msg.doPrintAlways = True  #False


def SetupApplicationMgr(task):
    print('----> SetupApplicationMgr')
    import OnlineEnv
    ApplicationMgr().AppName = ''
    #ApplicationMgr().HistogramPersistency = 'NONE'
    #ApplicationMgr().EvtSel = 'NONE'
    ApplicationMgr().ExtSvc += ["MonitorSvc"]
    ApplicationMgr().SvcOptMapping.append('LHCb::FmcMessageSvc/MessageSvc')
    #import MonitoringEnv as Online
    ApplicationMgr().EvtSel = OnlineEnv.mbmSelector(
        input='Events', type='USER', decode=False)
    #ApplicationMgr().EvtSel.REQ1 = "EvType=2;TriggerMask=0x0,0x4,0x0,0x0;VetoMask=0,0,0,0x700;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0";

    ### Set the trigger masks...
    #### 0x281000 means that bits 48, 55 (no bias) and 57 (error banks) are true, 0xffffffff accepts everything
    if task == 'STCalibMon':
        ApplicationMgr(
        ).EvtSel.REQ1 = "EvType=1;TriggerMask=0xffffffff,0xffffffff,0xffffffff,0xffffffff;VetoMask=0,0,0,0;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0"
    else:
        ApplicationMgr(
        ).EvtSel.REQ1 = "EvType=2;TriggerMask=0x0,0x2810000,0x0,0x1;VetoMask=0,0,0,0x700;MaskType=ANY;UserType=USER;Frequency=PERC;Perc=100.0"

    for i in ['ToolSvc', 'AuditorSvc']:
        ApplicationMgr().ExtSvc.append(i)

        #importOptions("/group/st/sw/OnlineMonitoring/scripts/Online.opts")

    mepMgr = OnlineEnv.mepManager(OnlineEnv.PartitionID,
                                  OnlineEnv.PartitionName, ['Events'], True)
    ApplicationMgr().Runable = OnlineEnv.evtRunable(mepMgr)
    ApplicationMgr().ExtSvc.append(mepMgr)
    ApplicationMgr().AuditAlgorithms = False
    AuditorSvc().Auditors.append('TimingAuditor/TIMER')

    #ApplicationMgr().HistogramPersistency  = "ROOT"
    ApplicationMgr().SvcOptMapping.append(
        'LHCb::OnlineEvtSelector/EventSelector')

    from Configurables import MonitorSvc
    MonitorSvc().DimUpdateInterval = 1
    MonitorSvc().ExpandCounterServices = True
    MonitorSvc().ExpandNameInfix = "<part>_x_<program>/"
    MonitorSvc().PartitionName = OnlineEnv.PartitionName
    MonitorSvc().ProgramName = os.environ.get('TASKNAME') + "_00"

    from Configurables import EventClockSvc
    EventClockSvc().EventTimeDecoder = 'OdinTimeDecoder'

    MessageSvc.OutputLevel = 1

    from Gaudi.Configuration import HistogramPersistencySvc
    HistogramPersistencySvc().Warnings = False

    print('<---- end SetupApplicationMgr')


def DatabaseTags(task):
    import sys, os
    partition = os.environ['PARTITION']
    from Configurables import LHCbApp, CondDB
    LHCbApp().DataType = '2016'
    LHCbApp().Persistency = 'MDF'
    if partition == 'LHCb':
        sys.path.insert(
            0, '/group/online/dataflow/options/%s/MONITORING' % partition)
        import ConditionsMap
        LHCbApp().DDDBtag = ConditionsMap.DDDBTag
        LHCbApp().CondDBtag = ConditionsMap.CondDBTag
        CondDB().Tags["ONLINE"] = 'fake'
        CondDB().UseDBSnapshot = True
        CondDB().DBSnapshotDirectory = '/group/online/hlt/conditions'
    else:
        LHCbApp().DDDBtag = ''
        LHCbApp().CondDBtag = ''
    LHCbApp().DQFLAGStag = ''
    LHCbApp().OutputLevel = 3
    #  from Configurables import CondDB
    CondDB().IgnoreHeartBeat = True
    CondDB().EnableRunStampCheck = False

    if task in ['STCalibMon', 'ITNZSMon', 'TTNZSMon']:
        try:
            from DDDB.Configuration import GIT_CONDDBS
        except ImportError:
            GIT_CONDDBS = {}
        if 'STCOND' in GIT_CONDDBS:
            # get Git CondDB access component
            from Configurables import GitEntityResolver, XmlParserSvc
            from Gaudi.Configuration import appendPostConfigAction
            GitSTCOND = GitEntityResolver(
                'GitSTCOND', PathToRepository=GIT_CONDDBS['STCOND'])
            GitSTCOND.Ignore = r'^(?!(lhcb\.xml$|[IT]TCondDB)).*$'

            # FIXME: this is ugly but it's unavoidable until we get
            #        https://its.cern.ch/jira/browse/LHCBPS-1738 fixed
            def inject_git_stcond():
                try:
                    xps = XmlParserSvc()
                    xps.EntityResolver.EntityResolvers.insert(0, GitSTCOND)
                except AttributeError:
                    # ignore non git-based configuration
                    pass

            appendPostConfigAction(inject_git_stcond)
        # it's fine to set the SQLite access when using Git because it's ignored
        # in that case
        from Configurables import CondDB, CondDBAccessSvc
        connection = "sqlite_file:$STSQLDDDBROOT/db/STCOND.db/COND"
        CondDB().addLayer(CondDBAccessSvc("COND", ConnectionString=connection))
