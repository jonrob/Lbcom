###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
UT/UTMonitors
-------------
#]=======================================================================]

gaudi_add_module(UTMonitors
    SOURCES
	  src/UTTightDigitMonitor.cpp
        src/UTDataSizeMonitor.cpp
        src/UTErrorMonitor.cpp
        src/UTFullEventDump.cpp
        src/UTLiteClusterMonitor.cpp
        src/UTSummaryMonitor.cpp
    LINK
        Boost::headers
        Gaudi::GaudiAlgLib
        Gaudi::GaudiKernel
        Gaudi::GaudiUtilsLib
        LHCb::DAQEventLib
        LHCb::DigiEvent
        LHCb::LHCbAlgsLib
        LHCb::LHCbKernel
        LHCb::RecEvent
        LHCb::UTDetLib
        LHCb::UTKernelLib
        LHCb::UTTELL1Event
        ROOT::Hist
)
