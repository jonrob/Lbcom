/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/UTTELL1Data.h"
#include "Kernel/UTDAQDefinitions.h"

#include "LHCbAlgs/Consumer.h"

namespace LHCb {

  /**
   *  Algorithm to dump the content of the NZS UTTELL1Data objects. Job options:
   *  - \b InputLocation: Location of NZS data e.g. UTFull
   *  - \b TELL1List: List of sourceIDs of Tell1's that you want to dump (Default
   *                  no Tell1's are dumped).
   *  - \b LinkList: List of links that you want to dump (Default all 23 links are
   *                  dumped).
   *
   *  @author Andy Beiter (based on code by Mathias Knecht, M Needham and Jeroen van Tilburg)
   *  @date   2018-09-04
   */
  class UTFullEventDump : public LHCb::Algorithm::Consumer<void( const UTTELL1Datas& )> {

  public:
    UTFullEventDump( const std::string& name, ISvcLocator* pSvcLocator );
    void operator()( const UTTELL1Datas& input ) const override;

  private:
    Gaudi::Property<std::vector<unsigned int>> m_TELL1List{this, "TELL1List"};
    Gaudi::Property<std::vector<unsigned int>> m_linkList{this, "LinkList"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( UTFullEventDump, "UTFullEventDump" )

} // namespace LHCb

LHCb::UTFullEventDump::UTFullEventDump( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"InputLocation", UTTELL1DataLocation::UTFull}} {
  // By default all links are dumped
  for ( unsigned int i = 0; i < UTDAQ::noptlinks; ++i ) m_linkList.value().push_back( i );
}

void LHCb::UTFullEventDump::operator()( const UTTELL1Datas& input ) const {
  // Loop over the tell1's
  for ( unsigned int tell1 : m_TELL1List ) {
    UTTELL1Data* thisBoard = input.object( tell1 );
    if ( !thisBoard ) {
      warning() << "Tell1 " << tell1 << " board not found" << endmsg;
      continue;
    }

    // Get the header and the data
    const UTTELL1Data::Data& header = thisBoard->header();
    const UTTELL1Data::Data& data   = thisBoard->data();

    // Loop over the optical links
    for ( unsigned i : m_linkList ) {
      // Information about tell1 and optical link
      info() << endmsg;
      info() << "TELL1 " << tell1 << ", optical link " << i << endmsg;

      // Header part
      info() << std::setfill( '#' ) << std::setw( 29 ) << " HEADERS " << std::setw( 20 ) << "#" << endmsg;
      info() << "Port 1:    |Port 2:    |Port 3:    |Port 4:    |#" << endmsg;
      info() << "-----------|-----------|-----------|-----------|#" << endmsg;
      info() << std::setfill( ' ' );
      for ( unsigned int iHead = 0; iHead < UTDAQ::nports * UTDAQ::nheaders; ++iHead ) {
        info() << std::setw( 3 ) << header[i][iHead];
        if ( iHead % 3 == 2 )
          info() << "|";
        else
          info() << " ";
      }
      info() << "#" << endmsg;

      // Data part
      info() << std::setfill( '#' ) << std::setw( 29 ) << " DATA ##" << std::setw( 105 ) << "#" << endmsg;
      info() << "Port|    Channel:" << std::setfill( ' ' ) << std::setw( 117 ) << "#" << endmsg;
      info() << "    |";
      for ( unsigned iChan = 0; iChan < UTDAQ::nstrips; ++iChan ) { info() << std::setw( 3 ) << iChan << " "; }
      info() << "#" << endmsg;
      info() << "    |" << std::setfill( '-' ) << std::setw( 129 ) << "#" << endmsg;
      for ( unsigned int iPort = 0; iPort < UTDAQ::nports; ++iPort ) {
        info() << std::setfill( ' ' ) << std::setw( 4 ) << iPort << "|";
        for ( unsigned iChan = 0; iChan < UTDAQ::nstrips; ++iChan ) {
          info() << std::setw( 3 ) << data[i][iPort * UTDAQ::nports + iChan] << " ";
        }
        info() << "#" << endmsg;
      }
      info() << std::setfill( '#' ) << std::setw( 134 ) << "#" << endmsg;

    } // boards to printout
  }   // links
}
