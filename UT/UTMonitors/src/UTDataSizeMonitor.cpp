/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Event/RawBank.h>
#include <Event/RawEvent.h>
#include <GAUDI_VERSION.h>
#include <Gaudi/Accumulators/Histogram.h>
#include <GaudiAlg/GaudiHistoAlg.h>
#include <Kernel/IUTReadoutTool.h>
#include <Kernel/UTTell1ID.h>
#include <LHCbAlgs/Consumer.h>
#include <mutex>
#include <optional>
#include <string>

namespace {
  template <typename T>
  nlohmann::json x2j( const T& value ) {
#if GAUDI_MAJOR_VERSION < 37
    return value.toJSON();
#else
    return value;
#endif
  }
} // namespace

/**
 *  Class for checking data size of the UT raw banks per tell1
 *
 *  @author A.Beiter (based on code by M.Needham)
 *  @date   2018-09-04
 *
 *  Move part to UTMonitors to look at data size per tell1
 */
namespace UT {
  class UTDataSizeMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::RawEvent& ),
                                                             LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg>> {

  public:
    UTDataSizeMonitor( const std::string& name, ISvcLocator* svcloc );
    StatusCode initialize() override;
    void       operator()( const LHCb::RawEvent& ) const override;

  private:
    mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_prof_dataSizeVsTELL1;
    const std::string                                               hpath = "";
    mutable std::mutex                                              m_mutex;
    ToolHandle<IUTReadoutTool>                                      m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};

    // warnings
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_NumTELLBoardsChanged{
        this, "Number of TELL boards changed during the job", 0};
  };

  DECLARE_COMPONENT( UTDataSizeMonitor )
} // namespace UT

UT::UTDataSizeMonitor::UTDataSizeMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"RawEventLocation", LHCb::RawEventLocation::Default}} {}

StatusCode UT::UTDataSizeMonitor::initialize() {
  if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
  return Consumer::initialize().andThen( [&] {
    struct HistosInitialized {};
    addConditionDerivation( {m_readoutTool->getReadoutInfoKey()}, "HistosInitialized-" + name(),
                            [&]( const IUTReadoutTool::ReadoutInfo& roInfo ) {
                              if ( !m_prof_dataSizeVsTELL1 ) {
                                // Get the tell1 mapping from source ID to tell1 number
                                m_prof_dataSizeVsTELL1.emplace(
                                    this, hpath + "MeanRawBankSizevsTELL1", "Mean Raw Bank Size vs TELL1",
                                    Gaudi::Accumulators::Axis<double>{roInfo.nBoards, 0.5, roInfo.nBoards + 0.5} );
                              }
                              nlohmann::json dataSizeVsTELL1j = x2j( *m_prof_dataSizeVsTELL1 );
                              auto           nBins            = dataSizeVsTELL1j.at( "bins" ).get<unsigned long>();
                              if ( roInfo.nBoards != static_cast<unsigned int>( nBins ) ) { ++m_NumTELLBoardsChanged; }
                              return HistosInitialized{};
                            } );
  } );
}

void UT::UTDataSizeMonitor::operator()( const LHCb::RawEvent& rawEvt ) const {
  // Retrieve the RawEvent:
  for ( const auto& bank : rawEvt.banks( LHCb::RawBank::UT ) ) {
    // board info....
    size_t       bankSize = bank->size() / sizeof( char );
    unsigned int sourceID = bank->sourceID();
    unsigned int TELL1ID  = ( m_readoutTool )->SourceIDToTELLNumber( sourceID );
    ( *m_prof_dataSizeVsTELL1 )[TELL1ID] += bankSize;
  }
}
