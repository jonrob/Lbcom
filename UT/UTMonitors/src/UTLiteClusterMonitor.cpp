/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/UTCluster.h"
#include "Event/UTLiteCluster.h"
#include "Event/UTSummary.h"
#include "UTDet/DeUTDetector.h"

#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiHistoAlg.h"

using namespace LHCb;

namespace UT {
  /**
   *  Class for monitoring UTLiteClusters
   *
   *  @author A.Beiter (based on code by M.Needham)
   *  @date   2018-09-04
   */
  class UTLiteClusterMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::UTLiteCluster::UTLiteClusters& ),
                                                                Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {

  public:
    UTLiteClusterMonitor( const std::string& name, ISvcLocator* svcloc );
    StatusCode initialize() override;
    void       operator()( const LHCb::UTLiteCluster::UTLiteClusters& ) const override;

  private:
    typedef LHCb::UTLiteCluster::UTLiteClusters UTLiteClusters;

    // Fill histograms for a given cluster
    void fillHistograms( const LHCb::UTLiteCluster& aCluster ) const;
  };

  DECLARE_COMPONENT( UTLiteClusterMonitor )

  UT::UTLiteClusterMonitor::UTLiteClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name, pSvcLocator, {"InputData", UTLiteClusterLocation::UTClusters}} {}

  StatusCode UT::UTLiteClusterMonitor::initialize() {
    if ( histoTopDir().empty() ) setHistoTopDir( "UT/" );
    return Consumer::initialize();
  }

  void UTLiteClusterMonitor::operator()( const UTLiteClusters& clusterCont ) const {
    // number of digits
    plot( (double)clusterCont.size(), "#clusters", "Number of clusters", 0., 5000., 500 );
    // histograms per cluster
    std::for_each( clusterCont.begin(), clusterCont.end(), [&]( const auto& clus ) { fillHistograms( clus ); } );
  }

  void UTLiteClusterMonitor::fillHistograms( const UTLiteCluster& aCluster ) const {
    // cluster Size
    plot( (double)aCluster.pseudoSize(), "pseudoSize", "pseudoSize of cluster", -0.5, 6.5, 7 );

    // high threshold
    plot( (double)aCluster.highThreshold(), "highThres", "High threshold", -0.5, 1.5, 2 );

    // histogram by station
    const int iStation = aCluster.station();
    plot( (double)iStation, "clustersPerStation", "Number of clusters per station", -0.5, 4.5, 5 );

    // by layer
    const int iLayer = aCluster.layer();
    plot( (double)( 10 * iStation + iLayer ), "clustersPerLayer", "Number of clusters per layer", -0.5, 40.5, 41 );

    plot( (double)aCluster.interStripFraction(), "interstrip", "Interstrip fraction", -0.125, 1.125, 5 );
  }
} // namespace UT
