/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include <Event/UTTELL1BoardErrorBank.h>
#include <GAUDI_VERSION.h>
#include <Gaudi/Accumulators/Histogram.h>
#include <Gaudi/Histograming/Sink/Utils.h>
#include <GaudiAlg/GaudiHistoAlg.h>
#include <Kernel/IUTReadoutTool.h>
#include <Kernel/UTDAQDefinitions.h>
#include <LHCbAlgs/Consumer.h>
#include <TAxis.h>
#include <TH2D.h>

namespace {
  template <typename T>
  nlohmann::json x2j( const T& value ) {
#if GAUDI_MAJOR_VERSION < 37
    return value.toJSON();
#else
    return value;
#endif
  }
} // namespace

using namespace LHCb;

/**
 *  Class to monitor the error banks. The output consists of:
 *  - One overview histograms that counts the number of error banks per Tell1.
 *  - One 2D histogram for each Tell1 with an error bank. It displays the error
 *    type versus the optical link number.
 *  There are three important job options:
 *  - \b DetType: to switch between "UT"
 *  - \b ErrorLocation: to set the error location in case it is different from
 *     the default location.
 *  - \b ExpertHisto: Set this to false to turn of the 2D histograms.
 *
 *  @author Andy Beiter (based on code by Jeroen Van Tilburg)
 *  @date   2018-09-04
 */
class UTErrorMonitor : public LHCb::Algorithm::Consumer<void( const UTTELL1BoardErrorBanks& ),
                                                        LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg>> {
public:
  UTErrorMonitor( const std::string& name, ISvcLocator* pSvcLocator );
  StatusCode initialize() override;
  void       operator()( const UTTELL1BoardErrorBanks& errors ) const override;

private:
  // Total number of active ports in detector
  unsigned int m_activePorts;

  Gaudi::Property<bool> m_expertHisto{this, "ExpertHisto", true}; ///< Flag to turn off filling of expert histograms

  // Book histrograms for online monitoring
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_evtCounter;
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_errorBanks;
  mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_2d_errorTypes;

  /// Map of error histograms booked in initialize
  mutable std::map<unsigned int, std::optional<Gaudi::Accumulators::Histogram<2>>> m_errorHistos;

  /// Label the y-axis with the error types
  void labelHistoErrorTypes( nlohmann::json j );

  /// Fraction of ports which send error banks
  mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_fracErrors;

  /// Fraction of ports which send error banks per TELL1
  mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_prof_fracErrors;

  ToolHandle<IUTReadoutTool> m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};

  /// helper flag to make sure to we run the deferred initialization only once
  bool m_initialized = false;

  // warnings
  mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_UTReadoutInfoChanged{
      this, "UTReadoutInfo changed during Run in an incompatible way", 0};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( UTErrorMonitor )

UTErrorMonitor::UTErrorMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"ErrorLocation", UTTELL1BoardErrorBankLocation::UTErrors}} {}

StatusCode UTErrorMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    addConditionDerivation(
        {m_readoutTool->getReadoutInfoKey()}, name() + "-DeferredInitialize",
        [&]( const IUTReadoutTool::ReadoutInfo& roInfo ) {
          // Get the maximum number of Tell1s to determine number of histogram bin
          using Axis1D           = Gaudi::Accumulators::Axis<double>;
          auto         allTell1s = m_readoutTool->allTELLNumbers( &roInfo );
          unsigned int maxTell1s = allTell1s.size();

          if ( !m_initialized ) {
            // Book histograms
            m_1d_evtCounter.emplace( this, "Event counter", "Event counter", Axis1D{1, 0.5, 1.5} );
            m_1d_errorBanks.emplace( this, "ErrbanksTell1", "Error banks per Tell1",
                                     Axis1D{maxTell1s, 0.5, maxTell1s + 0.5} );
            m_1d_fracErrors.emplace( this, "ErrPortFrac", "Fraction of ports which sent error banks",
                                     Axis1D{100, 0., 1.0} );
            m_prof_fracErrors.emplace( this, "ErrPortFrac_prof", "Fraction of ports which sent error banks per TELL1",
                                       Axis1D{maxTell1s, 0.5, maxTell1s + 0.5} );
            m_2d_errorTypes.emplace( this, "ErrorTypes", "Error types per TELL1",
                                     Axis1D{maxTell1s, 0.5, maxTell1s + 0.5}, Axis1D{10, 0., 10.} );

            labelHistoErrorTypes( x2j( *m_2d_errorTypes ) );

            // Get the tell1 mapping from source ID to tell1 number
            m_activePorts = 0;
            for ( auto tellID : allTell1s ) {
              // Create a title for the histogram
              std::string strTellID  = std::to_string( tellID );
              HistoID     histoID    = "error-types_$tell" + strTellID;
              std::string histoTitle = "Error types UTTELL" + strTellID;
              m_errorHistos[tellID].emplace( this, histoID, histoTitle,
                                             Axis1D{UTDAQ::nports * UTDAQ::noptlinks, 0., UTDAQ::noptlinks},
                                             Axis1D{10, .0, 10.} );
              labelHistoErrorTypes( x2j( *m_errorHistos[tellID] ) );
              // Work out the total number of links for the detector
              m_activePorts += UTDAQ::nports * UTDAQ::noptlinks;
            }

            m_activePorts -= 128; //< Very nastily hardcoded for now.

            if ( msgLevel( MSG::DEBUG ) ) debug() << "Number of active ports in UT is " << m_activePorts << endmsg;

            m_initialized = true;

          } else if ( static_cast<std::size_t>(
                          nlohmann::json( x2j( *m_1d_errorBanks ) ).at( "bins" ).get<unsigned long>() != maxTell1s ) ) {
            ++m_UTReadoutInfoChanged;
          }

          return 0; // we have to return something even if nobody is going to use it
        } );
  } );
}

void UTErrorMonitor::labelHistoErrorTypes( nlohmann::json j ) {

  std::string dir{"dir"};
  std::string name{"subdir/name"};
  auto [h, fullDir] =
      Gaudi::Histograming::Sink::jsonToRootHistogram<Gaudi::Histograming::Sink::Traits<false, TH2D, 2>>( dir, name, j );
  TAxis* axis = h.GetYaxis();
  if ( axis ) {
    axis->SetBinLabel( 1, "None" );
    axis->SetBinLabel( 2, "CorruptedBank" );
    axis->SetBinLabel( 3, "OptLinkDisabled" );
    axis->SetBinLabel( 4, "TlkLinkLoss" );
    axis->SetBinLabel( 5, "OptLinkNoClock" );
    axis->SetBinLabel( 6, "SyncRAMFull" );
    axis->SetBinLabel( 7, "SyncEvtSize" );
    axis->SetBinLabel( 8, "OptLinkNoEvent" );
    axis->SetBinLabel( 9, "PseudoHeader" );
    axis->SetBinLabel( 10, "WrongPCN" );
  }
}

void UTErrorMonitor::operator()( const UTTELL1BoardErrorBanks& errors ) const {

  ++( *m_1d_evtCounter )[1.];
  // Get the error banks

  // Number of links with error banks
  unsigned int nErrors = 0;
  /// Store number of error banks/TELL1 (48 (42) Tell1s, 1->48 (42) for UT)
  std::vector<unsigned int> nErrorBanksPerTELL1( m_readoutTool->nBoard(), 0 );

  // Loop over the error banks
  for ( const auto& bank : errors ) {

    // Print out the error bank information
    if ( msgLevel( MSG::DEBUG ) ) debug() << *bank << endmsg;

    // Get the number of the tell1
    unsigned int sourceID = bank->Tell1ID();
    unsigned int tellNum  = m_readoutTool->SourceIDToTELLNumber( sourceID );

    // Plot the number of error banks versus sequential tell number
    ++( *m_1d_errorBanks )[tellNum];

    if ( !m_expertHisto.value() ) continue;

    // Loop over the ErrorInfo objects (one for each FPGA-PP)
    const auto& errorInfo = bank->errorInfo();
    for ( auto errorIt = errorInfo.begin(); errorIt != errorInfo.end(); ++errorIt ) {

      // Get the PP number [0:3]
      unsigned int pp = errorIt - errorInfo.begin();

      // Get the majority vote for this PP
      unsigned int pcn = ( *errorIt )->pcnVote();

      // Loop over the Beetles and ports
      for ( unsigned int beetle = 0u; beetle < UTDAQ::nBeetlesPerPPx; ++beetle ) {
        for ( unsigned int port = 0u; port < UTDAQ::nports; ++port ) {
          // Fill 2D histogram with the error types versus Beetle port
          unsigned int errorType = ( *errorIt )->linkInfo( beetle, port, pcn );
          double       portBin   = double( port ) / double( UTDAQ::nports ) + beetle + pp * UTDAQ::nBeetlesPerPPx;
          ++( *m_errorHistos.at( tellNum ) )[{portBin, errorType}];
          if ( errorType > 0 ) {
            nErrors++;
            nErrorBanksPerTELL1[tellNum - 1] += 1;
            ++( *m_2d_errorTypes )[{tellNum, errorType}];
          }
        }
      }
    }
  }

  double fracErrors = (double)nErrors / m_activePorts;
  if ( fracErrors > 1E-5 ) ++( *m_1d_fracErrors )[fracErrors];
  int iTell = 1;
  for ( const auto& i : nErrorBanksPerTELL1 ) {
    ( *m_prof_fracErrors )[iTell++] += i / ( UTDAQ::nports * UTDAQ::noptlinks );
  }
}
