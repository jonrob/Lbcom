/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/UTDigit.h"
#include "Kernel/IUTReadoutTool.h"
#include "Kernel/UTDAQDefinitions.h"
#include "Kernel/UTDetectorPlot.h"
#include "Kernel/UTNames.h"
#include "TH1D.h"
#include "UTDet/DeUTDetector.h"
#include "UTDet/DeUTSector.h"

#include "LHCbAlgs/Consumer.h"

#include "Gaudi/Accumulators.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include <GAUDI_VERSION.h>
#include <Gaudi/Accumulators/Histogram.h>
#include <Gaudi/Histograming/Sink/Utils.h>
#include <boost/accumulators/accumulators.hpp>
#include <boost/accumulators/statistics/mean.hpp>
#include <boost/accumulators/statistics/median.hpp>
#include <boost/accumulators/statistics/stats.hpp>

#include <algorithm>
#include <limits>
#include <string>

// Use root histos to get MPV for sectors (in absence of something better)
// Boost accumulator classes to store MPVs of sectors

//-----------------------------------------------------------------------------
// Implementation file for class : UTTightDigitMonitor
//
// 2018-09-04 : Andy Beiter (based on code by Mark Tobin)
//-----------------------------------------------------------------------------

/** @class UTTightDigitMonitor UTTightDigitMonitor.h
 *
 *
 *  @author Xuhao Yuan (based on code by Mark Tobin, Andy Beiter)
 *  @date   2021-04-02
 */

namespace {
  // empty derived condition triggering creation of histograms
  struct HistoCreationCond {};

  template <typename T, typename K, typename OWNER>
  void map_emplace( T& t, K&& key, OWNER* owner, std::string const& name, std::string const& title,
                    Gaudi::Accumulators::Axis<typename T::mapped_type::AxisArithmeticType> axis1 ) {
    t.emplace( std::piecewise_construct, std::forward_as_tuple( key ),
               std::forward_as_tuple( owner, name, title, axis1 ) );
  }

  template <typename T>
  nlohmann::json x2j( const T& value ) {
#if GAUDI_MAJOR_VERSION < 37
    return value.toJSON();
#else
    return value;
#endif
  }
} // namespace

/// Define some typedefs for boost accumulators
namespace UT {

  // Median of distribution
  typedef boost::accumulators::accumulator_set<double, boost::accumulators::stats<boost::accumulators::tag::median(
                                                           boost::accumulators::with_p_square_quantile )>>
      MedianAccumulator;

  // Mean of distribution
  typedef boost::accumulators::accumulator_set<
      double, boost::accumulators::stats<boost::accumulators::tag::mean( boost::accumulators::immediate )>>
      MeanAccumulator;

  class UTTightDigitMonitor
      : public LHCb::Algorithm::Consumer<
            void( LHCb::ODIN const&, LHCb::UTDigits const&, DeUTDetector const&, HistoCreationCond const& ),
            LHCb::DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeUTDetector, HistoCreationCond>> {
  public:
    /// Standard constructor
    UTTightDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override; ///< Algorithm initialization
    void       operator()( LHCb::ODIN const&, LHCb::UTDigits const&, DeUTDetector const&,
                     HistoCreationCond const& ) const override;

  private:
    // Protects against double booking of histograms
    bool m_histoBooked{false};

    /// Fill basic histograms
    void fillHistograms( const LHCb::UTDigit* digit, std::vector<unsigned int>& nDigitsPerTELL,
                         DeUTDetector const& det ) const;

    /// Fill detailed histograms
    void fillDetailedHistograms( const LHCb::UTDigit* digit, DeUTDetector const& det ) const;

    /// Fill the hitmaps when required.
    void fillDigitMaps( const LHCb::UTDigit* digit, DeUTDetector const& det ) const;

    /// Fill the cluster mpv map per sector
    void fillMPVMap( DeUTSector const& sector, double charge ) const;

    /// Toggles to turn plots off/on
    Gaudi::Property<bool> m_plotBySvcBox{this, "ByServiceBox", false, "Plot by Service Box"};
    Gaudi::Property<bool> m_plotByDetRegion{this, "ByDetectorRegion", false, "Plot by unique detector region"};
    Gaudi::Property<bool> m_plotByPort{this, "ByPort", false, "Plot number of digits/tell by vs port"};
    Gaudi::Property<bool> m_hitMaps{this, "HitMaps", false, "True if digit maps are to be shown"};

    /// Cuts on the data quality
    Gaudi::Property<double>       m_chargeCut{this, "ChargeCut", 0, "Cut on charge"};
    Gaudi::Property<unsigned int> m_minNDigits{this, "MinTotalDigits", 0,
                                               "Cut on minimum number of digits in the event"};
    Gaudi::Property<unsigned int> m_maxNDigits{this, "MaxTotalDigits", std::numeric_limits<unsigned int>::max(),
                                               "Cut on maximum number of digits in the event"};
    Gaudi::Property<double>       m_minMPVCharge{this, "MinMPVCharge", 0, "Minimum charge for calculation on MPV"};

    Gaudi::Property<unsigned int> m_overFlowLimit{this, "OverflowLimit", 5000,
                                                  "Overflow limit for number of digits in event"};
    Gaudi::Property<unsigned int> m_resetRate{this, "ResetRate", 1000, "Reset rate for MPV calculation"};

    mutable Gaudi::Accumulators::Counter<>                m_eventsCount{this, "Number of events"};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_UndefinedNoise{
        this, "Some digit have zero or undefined noise", 0};
    mutable Gaudi::Accumulators::MsgCounter<MSG::WARNING> m_UndefinedDigit{this,
                                                                           "Zero or undefined S/N for some digits", 0};

    unsigned int m_nTELL40s; ///< Number of TELL40 boards expected.

    /// Book histograms
    void bookHistograms( DeUTDetector const& det, IUTReadoutTool::ReadoutInfo const& roInfo );

    // filled in monitor clusters
    const std::string                                               hpath = "";
    mutable std::optional<Gaudi::Accumulators::Histogram<1>>        m_1d_nDigits;
    mutable std::optional<Gaudi::Accumulators::Histogram<1>>        m_1d_nDigits_gt_100;
    mutable std::optional<Gaudi::Accumulators::Histogram<1>>        m_1d_nDigitsVsTELL40;
    mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_prof_nDigitsVsTELL40;
    mutable std::optional<Gaudi::Accumulators::Histogram<2>>        m_2d_nDigitsVsTELL40;

    mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_nDigitsVsTELL40Links;
    mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_nDigitsVsTELL40Sectors;
    mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_nDigits_overflow;
    // filled in fillHistograms
    // do we need these two?
    mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_DigitSize;
    mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_2d_DigitSizeVsTELL40;
    mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_2d_UTNVsTELL40;
    mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_2d_ChargeVsTELL40;
    mutable std::optional<Gaudi::Accumulators::Histogram<2>> m_2d_DigitsPerPortVsTELL40;
    mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_totalCharge;
    /// Plots by service box
    mutable std::map<std::string, std::optional<Gaudi::Accumulators::Histogram<1>>> m_1ds_chargeByServiceBox;
    /// Plots by detector region
    mutable std::map<std::string, std::optional<Gaudi::Accumulators::Histogram<1>>> m_1ds_chargeByDetRegion;
    /// Hitmaps
    mutable std::optional<Gaudi::Accumulators::Histogram<2>>        m_2d_hitmap;
    mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_prof_sectorMPVs;
    mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_prof_sectorTruncMean1;
    // first and last 15%
    mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_prof_sectorTruncMean2;
    mutable std::optional<Gaudi::Accumulators::ProfileHistogram<1>> m_prof_sectorBinnedMPV;
    mutable std::optional<Gaudi::Accumulators::Histogram<2>>        m_2d_sectorMPVs;
    mutable std::optional<Gaudi::Accumulators::Histogram<2>>        m_2d_sectorMPVsNorm;

    unsigned int              m_nSectors;              ///< Number of sectors (ie hybrids)
    unsigned int              m_nBeetlePortsPerSector; ///< Number of beetle ports per sector
    static const unsigned int m_nBeetlePortsPerUTSector =
        16u; ///< Number of bins per UT sector in the hitmap (beetle ports)
    unsigned int                                                                           m_nSectorsPerTELL40;
    mutable std::map<const unsigned int, std::optional<Gaudi::Accumulators::Histogram<1>>> m_1ds_chargeBySector;

    mutable std::map<const unsigned int, std::vector<double>> m_chargeBySector;

    /// Contains the bin corresponding to a given sectors in the 1d representation of the hitmap
    std::map<const unsigned int, unsigned int>               m_sectorBins1D;
    mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_nDigitsVsSector;
    mutable std::optional<Gaudi::Accumulators::Histogram<1>> m_1d_nDigitsVsBeetlePort;
    // port

    mutable std::mutex         m_mutex;
    ToolHandle<IUTReadoutTool> m_readoutTool{this, "ReadoutTool", "UTReadoutTool"};
  };

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT( UTTightDigitMonitor )
} // namespace UT

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
UT::UTTightDigitMonitor::UTTightDigitMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"ODINLocation", LHCb::ODINLocation::Default},
                KeyValue{"TightDigitLocation", LHCb::UTDigitLocation::UTTightDigits},
                {"UTLocation", DeUTDetLocation::location()},
                {"HistoCreationCondLocation", "UTClusterMonitorHistoCreationCond"}}} {}

//=============================================================================
// Initialization
//=============================================================================
StatusCode UT::UTTightDigitMonitor::initialize() {
  return Consumer::initialize().andThen( [&] {
    if ( "" == histoTopDir() ) setHistoTopDir( "UT/" );
    // Turn all histograms on in expert mode
    if ( fullDetail() ) {
      m_hitMaps         = true;
      m_plotBySvcBox    = true;
      m_plotByDetRegion = true;
      m_plotByPort      = true;
    }

    m_nBeetlePortsPerSector = m_nBeetlePortsPerUTSector;
    m_nSectorsPerTELL40     = UTDAQ::noptlinks * UTDAQ::nports / m_nBeetlePortsPerSector;

    // Triggers booking of histograms when geometry is loaded
    addConditionDerivation(
        {DeUTDetLocation::location(), m_readoutTool->getReadoutInfoKey()},
        this->template inputLocation<HistoCreationCond>(),
        [this]( DeUTDetector const& det, IUTReadoutTool::ReadoutInfo const& roInfo ) -> HistoCreationCond {
          bookHistograms( det, roInfo );
          return {};
        } );

    return StatusCode::SUCCESS;
  } );
}

//=============================================================================
// Main execution
//=============================================================================
void UT::UTTightDigitMonitor::operator()( LHCb::ODIN const& odin, LHCb::UTDigits const& digits, DeUTDetector const& det,
                                          HistoCreationCond const& ) const {

  plot1D( odin.bunchId(), "BCID", "BCID", -0.5, 2807.5, 2808 );
  ++m_eventsCount;

  /// This  plots digit ADCs vs Sampling time
  /// Store number of digits/TELL40 (48 (42) Tell1s, 1->48
  std::vector<unsigned int> nDigitsPerTELL40( m_nTELL40s, 0 );

  const unsigned int nDigits = digits.size();
  debug() << "Number of digits in " << inputLocation<LHCb::UTDigits>() << " is " << nDigits << endmsg;

  if ( nDigits < m_minNDigits || nDigits > m_maxNDigits ) return;

  ++( *m_1d_nDigits )[nDigits];
  if ( 100 < nDigits ) { ++( *m_1d_nDigits_gt_100 )[nDigits]; }
  if ( m_overFlowLimit < nDigits )
    ++( *m_1d_nDigits_overflow )[(unsigned int)m_overFlowLimit];
  else
    ++( *m_1d_nDigits_overflow )[nDigits];
  // Loop over digits

  for ( const auto& digit : digits ) {
    fillHistograms( digit, nDigitsPerTELL40, det );
    if ( fullDetail() ) fillDetailedHistograms( digit, det );
  } // End of digit iterator

  // Fill histogram for number of digits/TELL40
  unsigned int TELL40 = 1;
  for ( auto itClPerTELL40 = nDigitsPerTELL40.begin(); itClPerTELL40 != nDigitsPerTELL40.end();
        ++itClPerTELL40, ++TELL40 ) {
    verbose() << "TELL40: " << TELL40 << ",digits: " << ( *itClPerTELL40 ) << endmsg;
    unsigned int nDigits = ( *itClPerTELL40 );
    if ( 0 < nDigits ) {
      ++( *m_2d_nDigitsVsTELL40 )[{TELL40, nDigits}];
      ( *m_prof_nDigitsVsTELL40 )[TELL40] += nDigits;
    }
  }
}

//================================================================================================================================
// Fill the MPV cluster maps
// Three means are calculated:
// 1) Truncated mean using 70% of the distribution
// 2) Truncated mean using only the 1st 70% of the distribution
// 3) Binned mean with 20 bins from 0 to 100 ADC counts
//================================================================================================================================
void UT::UTTightDigitMonitor::fillMPVMap( DeUTSector const& sector, double charge ) const {
  unsigned int sectorID = sector.elementID().uniqueSector();
  if ( charge > m_minMPVCharge ) {
    auto                 lock          = std::scoped_lock{m_mutex};
    std::vector<double>& sectorCharges = m_chargeBySector.at( sectorID );
    ++( *m_1ds_chargeBySector.at( sectorID ) )[charge];

    sectorCharges.push_back( charge );
    if ( sectorCharges.size() > m_resetRate ) {
      std::sort( sectorCharges.begin(), sectorCharges.end() );
      unsigned int fullRange = sectorCharges.end() - sectorCharges.begin();
      unsigned int usedRange = static_cast<unsigned int>( 0.7 * fullRange );
      unsigned int start     = ( fullRange - usedRange ) / 2;
      unsigned int stop      = usedRange + ( fullRange - usedRange ) / 2;
      double       mean1     = std::accumulate( sectorCharges.begin() + start, sectorCharges.begin() + stop, 0. );
      mean1 /= usedRange;
      double mean2 = std::accumulate( sectorCharges.begin(), sectorCharges.begin() + usedRange, 0. );
      mean2 /= usedRange;
      std::vector<double> binnedMPV( 20, 0. );

      for ( const auto& iMPV : sectorCharges ) {
        unsigned int bin = static_cast<int>( iMPV / 5. );
        if ( bin < binnedMPV.size() ) binnedMPV[bin] += 1;
      }
      int         maxBin = std::max_element( binnedMPV.begin(), binnedMPV.end() ) - binnedMPV.begin();
      double      mean3  = ( maxBin * 5. ) + 2.5;
      std::string dir{"dir"};
      std::string name{"subdir/name"};

      auto [hSec, fullDir] =
          Gaudi::Histograming::Sink::jsonToRootHistogram<Gaudi::Histograming::Sink::Traits<false, TH1D, 1>>(
              dir, name, x2j( *m_1ds_chargeBySector[sectorID] ) );

      debug() << "Sector " << sectorID << ": truncMean=" << mean1 << ", mean2(70%)=" << mean2
              << ", mean3(binned)=" << mean3
              << ", full=" << std::accumulate( sectorCharges.begin(), sectorCharges.end(), 0. ) / fullRange
              << ", histo=" << ( hSec.GetBinCenter( hSec.GetMaximumBin() ) ) << endmsg;
      unsigned int bin = m_sectorBins1D.at( sectorID );
      ( *m_prof_sectorTruncMean1 )[bin] += mean1;
      ( *m_prof_sectorTruncMean2 )[bin] += mean2;
      ( *m_prof_sectorBinnedMPV )[bin] += mean3;

      double mpv = mean3;
      ++( *m_prof_sectorMPVs )[bin] += mpv;
      UT::UTDetectorPlot       hitMap( "map", "map" );
      UT::UTDetectorPlot::Bins bins = hitMap.toBins( sector );
      double                   xBin = bins.xBin;
      for ( int yBin = bins.beginBinY; yBin != bins.endBinY; ++yBin ) {
        ( *m_2d_sectorMPVs )[{xBin, yBin}] += mpv;
        ++( *m_2d_sectorMPVsNorm )[{xBin, yBin}];
      }
      sectorCharges.clear();
    }
  }
}

//==============================================================================
// Book histograms
//==============================================================================
void UT::UTTightDigitMonitor::bookHistograms( DeUTDetector const& det, IUTReadoutTool::ReadoutInfo const& roInfo ) {
  using Axis1D = Gaudi::Accumulators::Axis<double>;
  // make sure we fill Histograms only once !
  // In case we load 2 different geometries, we'll be called twice. But in that case, our histograms make no sense
  if ( m_histoBooked ) {
    throw GaudiException( "Double loading of UT geometry detected. This is not supported",
                          "UTTightDigitMonitor::bookHistograms", StatusCode::FAILURE );
  }
  m_histoBooked = true;

  m_nTELL40s = m_readoutTool->nBoard( &roInfo );
  ;

  // filled in monitor digits
  m_1d_nDigits.emplace( this, hpath + "NumberOfDigits", "Number of digits", Axis1D{2000, 0., 2000.} );
  m_1d_nDigits_gt_100.emplace( this, hpath + "NumberOfDigitsGT100", "Number of digits (N > 100)",
                               Axis1D{2000, 0., 2000.} );
  m_1d_nDigits_gt_100.emplace( this, hpath + "NumberOfDigitsNoOverflow)", "Number of digits (no overflow)",
                               Axis1D{1000, 0., m_overFlowLimit + 1.} );
  m_2d_nDigitsVsTELL40.emplace( this, hpath + "NumberOfDigitsPerTELL40", "Number of digits per TELL40",
                                Axis1D{m_nTELL40s, 0.5, m_nTELL40s + 0.5}, Axis1D{50, 0., 100.} );
  // Number of digits produced by each TELL40
  m_1d_nDigitsVsTELL40.emplace( this, hpath + "NumberOfDigitsVsTELL40", "Number of digits vs TELL40",
                                Axis1D{m_nTELL40s, 0.5, m_nTELL40s + 0.5} );
  m_prof_nDigitsVsTELL40.emplace( this, hpath + "MeanNumberOfDigitsPerTELL40", "Mean number of digits per TELL40",
                                  Axis1D{m_nTELL40s, 0.5, 0.5 + m_nTELL40s} );
  m_1d_nDigitsVsTELL40Links.emplace( this, hpath + "NumberOfDigitsVsTELL40Links", "Number of digits vs TELL40 links",
                                     Axis1D{m_nTELL40s * UTDAQ::noptlinks, 1., m_nTELL40s + 1.} );
  m_1d_nDigitsVsTELL40Sectors.emplace( this, hpath + "NumberOfDigitsVsTELL40Sectors",
                                       "Number of digits vs TELL40 sectors",
                                       Axis1D{m_nTELL40s * m_nSectorsPerTELL40, 1., m_nTELL40s + 1.} );
  // filled in fillHistograms
  m_1d_DigitSize.emplace( this, hpath + "DigitSize", "Digit Size", Axis1D{4, 0.5, 4.5} );
  m_2d_DigitSizeVsTELL40.emplace( this, hpath + "DigitSizeVsTELL40", "Digit Size vs TELL40",
                                  Axis1D{m_nTELL40s, 0.5, m_nTELL40s + 0.5}, Axis1D{4, 0.5, 4.5} );
  m_2d_UTNVsTELL40.emplace( this, hpath + "SignalToNoiseVsTELL40", "Signal to Noise vs TELL40",
                            Axis1D{m_nTELL40s, 0.5, m_nTELL40s + 0.5}, Axis1D{25, 0., 100.} );
  m_2d_ChargeVsTELL40.emplace( this, hpath + "ClusterChargeVsTELL40", "Cluster Charge vs TELL40",
                               Axis1D{m_nTELL40s, 0.5, m_nTELL40s + 0.5}, Axis1D{60, 0., 60.} );

  if ( m_plotByPort ) {
    m_2d_ChargeVsTELL40.emplace( this, hpath + "DigitsPerPortVsTELL40", "Digits per port vs TELL40",
                                 Axis1D{m_nTELL40s, 0.5, m_nTELL40s + 0.5}, Axis1D{96, 0., 96.} );
  }
  m_1d_totalCharge.emplace( this, hpath + "ClusterADCValues", "Cluster ADC Values", Axis1D{200, 0., 200.} );
  /// list of service boxes
  for ( const auto& svcBox : m_readoutTool->serviceBoxes() ) {
    std::string quadrant = svcBox.substr( 0, 2 );
    if ( !m_1ds_chargeByServiceBox[quadrant] ) {
      m_1ds_chargeByServiceBox[quadrant].emplace( this, hpath + "ClusterADCValues" + quadrant,
                                                  "Cluster ADC Values" + quadrant, Axis1D{200, 0., 200.} );
    }
    if ( m_plotBySvcBox ) {
      m_1ds_chargeByServiceBox[svcBox].emplace( this, hpath + "ClusterADCValues" + svcBox,
                                                "Cluster ADC Values" + svcBox, Axis1D{200, 0., 200.} );
    }
  } // End of service box loop

  if ( m_plotByDetRegion ) {
#if 0
    for ( const auto& region : LHCb::UTNames().allDetRegions() ) {
      m_1ds_chargeByDetRegion[region].emplace(
        this, hpath + "ClusterADCValues" + region, "Cluster ADC Values" + region,
        Axis1D{200, 0., 200.} );
    };
#endif
  }
  // Book histograms for hitmaps
  if ( m_hitMaps ) {
    std::string        idMap    = "UT digit map";
    std::string        idMPVMap = "UT Sector MPVs";
    UT::UTDetectorPlot hitMap( idMap, idMap, m_nBeetlePortsPerUTSector );
    m_2d_hitmap.emplace( this, hpath + hitMap.name(), hitMap.name(),
                         Axis1D{hitMap.nBinX(), hitMap.minBinX(), hitMap.maxBinX()},
                         Axis1D{hitMap.nBinY(), hitMap.minBinY(), hitMap.maxBinY()} );
    UT::UTDetectorPlot MPVMap( idMPVMap, idMPVMap );
    m_2d_sectorMPVs.emplace( this, hpath + MPVMap.name(), MPVMap.name(),
                             Axis1D{MPVMap.nBinX(), MPVMap.minBinX(), MPVMap.maxBinX()},
                             Axis1D{MPVMap.nBinY(), MPVMap.minBinY(), MPVMap.maxBinY()} );
    m_2d_sectorMPVsNorm.emplace( this, hpath + MPVMap.name() + "Normalisation", MPVMap.name() + " Normalisation",
                                 Axis1D{MPVMap.nBinX(), MPVMap.minBinX(), MPVMap.maxBinX()},
                                 Axis1D{MPVMap.nBinY(), MPVMap.minBinY(), MPVMap.maxBinY()} );
    // Create histogram of with total charge for each sector
    m_sectorBins1D.clear();
    int bin = 1;
    det.applyToAllSectors( [&]( DeUTSector const& sector ) {
      std::string idh   = "charge_$sector" + std::to_string( sector.elementID().uniqueSector() );
      std::string title = "Total charge in " + sector.nickname();
      m_1ds_chargeBySector[sector.elementID().uniqueSector()].emplace( this, "BySector/" + idh, title,
                                                                       Axis1D{50, 0., 100.} );
      m_sectorBins1D[sector.elementID().uniqueSector()] = bin;
      ++bin;
    } );
    // 1d representation for online monitoring
    m_nSectors = m_sectorBins1D.size();
    m_1d_nDigitsVsSector.emplace( this, hpath + "NumberOfDigitsPerSector", "Number of digits per sector",
                                  Axis1D{m_nSectors, 0.5, 0.5 + m_nSectors} );
    m_1d_nDigitsVsSector.emplace( this, hpath + "NumberOfDigitsPerBeetlePort", "Number of digits per beetle port",
                                  Axis1D{m_nSectors * m_nBeetlePortsPerSector, 0.5, 0.5 + m_nSectors} );
    m_prof_sectorMPVs.emplace( this, hpath + "ClusterMPVVsSector", "Cluster MPV vs Sector",
                               Axis1D{m_nSectors, 0.5, 0.5 + m_nSectors} );
    m_prof_sectorTruncMean1.emplace( this, hpath + "ClusterTruncMeanVsSector", "Cluster trunc mean vs Sector",
                                     Axis1D{m_nSectors, 0.5, 0.5 + m_nSectors} );
    m_prof_sectorTruncMean2.emplace( this, hpath + "DigitTruncMean1st70PercentVsSector",
                                     "Digit trunc mean (1st 70%) vs Sector",
                                     Axis1D{m_nSectors, 0.5, 0.5 + m_nSectors} );
    m_prof_sectorBinnedMPV.emplace( this, hpath + "DigitBinnedMPVVsSector", "Digit binned MPV vs Sector",
                                    Axis1D{m_nSectors, 0.5, 0.5 + m_nSectors} );
  } // end of hitmaps
}

//==============================================================================
// Fill histograms
//==============================================================================
void UT::UTTightDigitMonitor::fillHistograms( const LHCb::UTDigit* digit, std::vector<unsigned int>& nDigitsPerTELL40,
                                              DeUTDetector const& det ) const {

  const double totalCharge = digit->depositedCharge();
  if ( totalCharge < m_chargeCut ) return;

    // calculate MPVs
#ifdef USE_DD4HEP
  auto sector = det.findSector( digit->channelID() );
#else
  auto& sector = *det.findSector( digit->channelID() );
#endif
  if ( m_hitMaps ) {
    fillDigitMaps( digit, det );
    fillMPVMap( sector, totalCharge );
    unsigned int sectorID = sector.elementID().uniqueSector();
    unsigned int bin      = m_sectorBins1D.at( sectorID );
    ++( *m_1d_nDigitsVsSector )[bin];
    unsigned int port = ( digit->channelID().strip() - sector.firstStrip() ) / UTDAQ::nstrips;
    ++( *m_1d_nDigitsVsBeetlePort )[( bin - 0.5 ) + static_cast<double>( port ) / m_nBeetlePortsPerSector];
  }

  const unsigned int digitSize = 1u;

  // const unsigned int sourceID = cluster->sourceID();
  const UTDAQID      utdaqID       = m_readoutTool->channelIDToDAQID( digit->channelID() );
  const unsigned int tell40channel = UTDAQ::nStripsInUTSector * utdaqID.lane() + utdaqID.channel();
  unsigned int       TELL40ID      = utdaqID.board();
  nDigitsPerTELL40[TELL40ID] += 1;
  ++( *m_1d_nDigitsVsTELL40 )[TELL40ID];
  unsigned int tell40Link = tell40channel / 128;
  ++( *m_1d_nDigitsVsTELL40Links )[( TELL40ID ) + ( ( 0.5 + tell40Link ) / UTDAQ::noptlinks )];
  unsigned int tell40Sector = tell40channel / ( m_nBeetlePortsPerSector * UTDAQ::nstrips );
  ++( *m_1d_nDigitsVsTELL40Sectors )[( TELL40ID ) + ( ( 0.5 + tell40Sector ) / m_nSectorsPerTELL40 )];

  ++( *m_1d_DigitSize )[digitSize];
  ++( *m_2d_DigitSizeVsTELL40 )[{TELL40ID, digitSize}];

  std::optional<double> noise = sector.noise( digit->channelID() );
  if ( noise && noise.value() > 1e-3 ) {
    const double signalToNoise = digit->depositedCharge() / noise.value();
    ++( *m_2d_UTNVsTELL40 )[{TELL40ID, signalToNoise}];
  } else
    ++m_UndefinedNoise;
  ++( *m_2d_ChargeVsTELL40 )[{TELL40ID, totalCharge}];

  if ( m_plotByPort ) {
    // const unsigned int tell1Channel = cluster->tell1Channel();
    unsigned int port = tell40channel / UTDAQ::nstrips;
    ++( *m_2d_DigitsPerPortVsTELL40 )[{TELL40ID, port}];
  }
  // Always fill histograms per readout quadrant
  // Get service box and set up histogram IDs
  ++( *m_1d_totalCharge )[totalCharge];
  std::string svcBox   = m_readoutTool->serviceBox( digit->channelID() );
  std::string quadrant = svcBox.substr( 0, 2 );
  ++( *m_1ds_chargeByServiceBox.at( quadrant ) )[totalCharge];

  if ( m_plotBySvcBox ) ++( *m_1ds_chargeByServiceBox.at( svcBox ) )[totalCharge];
}
//==============================================================================
/// Fill more detailed histograms
//==============================================================================
void UT::UTTightDigitMonitor::fillDetailedHistograms( const LHCb::UTDigit* digit, DeUTDetector const& det ) const {
  // high threshold
  // by strip, modulo a few things....
  const unsigned int strip = ( digit->channelID() ).strip();
  plot1D( strip % 8, "strip modulo 8", "strip modulo 8", -0.5, 8.5, 9 );
  plot1D( strip % 32, "strip modulo 32", "strip modulo 32", -0.5, 32.5, 33 );
  plot1D( strip % 128, "strip modulo 128", "strip modulo 128", -0.5, 128.5, 129 );

  // histogram by station
  const int station = ( digit->channelID() ).station();
  plot1D( station, "Number of digits per station", "Number of digits per station", -0.5, 4.5, 5 );

  // by layer
  const int layer = digit->layer();
  plot( (double)( 10 * station + layer ), "Number of digits per layer", "Number of digits per layer", -0.5, 40.5, 41 );

#if 0
  const double neighbourSum = cluster->neighbourSum();
  plot1D( cluster->pseudoSize(), "Pseudo size of cluster", "Pseudo size of cluster", -0.5, 10.5, 11 );
#endif

  const double totalCharge = digit->depositedCharge();

#if 0
  const unsigned int digitSize = 1u;
  if ( 3 > digitSize ) {
    plot1D( neighbourSum, "Neighbour sum (1- and 2-strip clusters)", "Neighbour sum (1- and 2-strip clusters)", -16.5,
            26.5, 43 );
    plot1D( neighbourSum / totalCharge, "Relative neighbour sum (1- and 2-strip clusters)",
            "Relative neighbour sum (1- and 2-strip clusters)", -1.02, 1.02, 51 );
  }
#endif
  // charge and S/N
  plot1D( totalCharge, digit->layerName() + "/Charge", "Charge", -0.5, 500.5, 501 );
#ifdef USE_DD4HEP
  auto sector = det.findSector( digit->channelID() );
#else
  auto& sector = *det.findSector( digit->channelID() );
#endif
  std::optional<double> noise = sector.noise( digit->channelID() );
  if ( noise && noise.value() > 1e-3 ) {
    const double signalToNoise = digit->depositedCharge() / noise.value();
    plot1D( signalToNoise, LHCb::UTNames().UniqueLayerToString( digit->channelID() ) + "/Signal to noise", "S/N", 0.,
            100., 100 );
  } else
    ++m_UndefinedDigit;
    // Plot digit ADCs for 1, 2, 3, 4 strip digits
#if 0
  std::string svcBox   = m_readoutTool->serviceBox( cluster->channelID() );
  std::string idhStrip = " (" + std::to_string( clusterSize ) + " strip)";
  std::string idh;
  idh = "Cluster ADCs in " + svcBox.substr( 0, 2 ) + idhStrip;
  plot1D( totalCharge, idh, idh, 0., 200., 200 );
  idh = "Cluster ADCs in " + svcBox + idhStrip;
  plot1D( totalCharge, idh, idh, 0., 200., 200 );

  // Charge in each sector for 1, 2, 3 and 4 strip clusters
  idh = "BySector/s" + std::to_string( cluster->size() ) + "/charge_$sector" +
        std::to_string( cluster->channelID().uniqueSector() );
  plot1D( totalCharge, idh, "Total charge in " + cluster->sectorName(), 0., 200., 200 );
#endif
}
//==============================================================================
/// Fill histograms of digit maps
//==============================================================================
void UT::UTTightDigitMonitor::fillDigitMaps( const LHCb::UTDigit* digit, DeUTDetector const& det ) const {
  const std::string idMap = "UT digit map";
#ifdef USE_DD4HEP
  auto utSector = det.findSector( digit->channelID() );
#else
  auto& utSector = *det.findSector( digit->channelID() );
#endif

  // make the real hit map
  UT::UTDetectorPlot       hitMap( idMap, idMap, m_nBeetlePortsPerUTSector );
  UT::UTDetectorPlot::Bins bins  = hitMap.toBins( utSector );
  int                      port  = ( digit->channelID().strip() - utSector.firstStrip() ) / UTDAQ::nstrips;
  double                   xBin  = bins.xBin - double( port + 1 ) / m_nBeetlePortsPerUTSector + 0.5;
  int                      nBins = bins.endBinY - bins.beginBinY;

  for ( int yBin = bins.beginBinY; yBin != bins.endBinY; ++yBin ) { ( *m_2d_hitmap )[{xBin, yBin}] += ( 1. / nBins ); }
}
