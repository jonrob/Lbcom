/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "AIDA/IHistogram1D.h"
#include "AIDA/IHistogram2D.h"
#include "AIDA/IProfile1D.h"
#include "Event/RawEvent.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "LHCbAlgs/Consumer.h"
#include "TProfile.h"
#include <mutex>

namespace {
  auto allBankNames() {
    std::vector<std::string> bn;
    bn.reserve( (int)LHCb::RawBank::LastType );
    for ( int i = 0; i != (int)LHCb::RawBank::LastType; i++ ) {
      bn.push_back( LHCb::RawBank::typeName( (LHCb::RawBank::BankType)i ) );
    }
    return bn;
  }
} // namespace

/**
 *  @author Olivier Deschamps
 *  @date   2008-03-27
 */
class RawBankSizeMonitor : public LHCb::Algorithm::Consumer<void( const LHCb::RawEvent& rawEvent ),
                                                            Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  RawBankSizeMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : Consumer{name, pSvcLocator, {"RawEventLocation", LHCb::RawEventLocation::Default}} {
    setHistoDir( name );
    setProperty( "HistoPrint", true ).ignore();
  }

  StatusCode initialize() override;
  void       operator()( const LHCb::RawEvent& rawEvent ) const override;

private:
  std::vector<LHCb::RawBank::BankType>        m_bankTypes;
  Gaudi::Property<std::vector<std::string>>   m_bankNames{this, "bankNames", allBankNames()};
  Gaudi::Property<std::map<std::string, int>> m_max{this, "MaxSizeMap", {}};
  Gaudi::Property<int>                        m_def{this, "MaxSizeDef", 40000};
  Gaudi::Property<int>                        m_bin{this, "Bins", 200};
  Gaudi::Property<bool>                       m_prof{this, "Profile", true};

  mutable Gaudi::Accumulators::StatCounter<unsigned int> m_totalBankSize{this, "Total RawEvent size (bytes)"};

  /// mutex lock
  mutable std::mutex m_mutex;
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RawBankSizeMonitor )

StatusCode RawBankSizeMonitor::initialize() {
  const auto sc = GaudiHistoAlg::initialize();
  if ( sc.isFailure() ) return sc;
  // convert bankNames to bankTypes
  for ( const auto& bname : m_bankNames ) {
    bool found = false;
    for ( int i = 0; i != (int)LHCb::RawBank::LastType && !found; i++ ) {
      const auto _name = LHCb::RawBank::typeName( (LHCb::RawBank::BankType)i );
      if ( _name == bname ) {
        found = true;
        m_bankTypes.push_back( (LHCb::RawBank::BankType)i );
      }
    }
    if ( !found ) warning() << "Requested bank '" << bname << "' is not a valid name" << endmsg;
  }
  return sc;
}

void RawBankSizeMonitor::operator()( const LHCb::RawEvent& rawEvent ) const {
  // lock for histogram updating
  std::lock_guard lock( m_mutex );
  // Loop over banks
  unsigned int total = 0;
  for ( const auto& bt : m_bankTypes ) {
    // get the banks for this type
    const auto bks = rawEvent.banks( bt );
    // skipping missing banks (many no longer valid for Run3)
    if ( bks.empty() ) { continue; }
    // name string
    const auto name = LHCb::RawBank::typeName( bt );
    // max size for plotting
    auto                        im  = m_max.find( name );
    const auto                  max = ( im == m_max.end() ? m_def.value() : im->second );
    std::map<int, unsigned int> size;
    int                         s_min = 9999;
    int                         s_max = -1;
    int                         s_sum = 0;
    for ( const auto ib : bks ) {
      const auto source = ib->sourceID();
      if ( s_min > source ) { s_min = source; }
      if ( s_max < source ) { s_max = source; }
      s_sum += ib->size();       // size in Bytes
      size[source] = ib->size(); // size in Bytes
    }
    plot1D( s_sum, name + "/size", "Overall " + name + " bank size (bytes)", 0, (double)max, m_bin.value() );
    total += s_sum;
    for ( auto [k, v] : size ) {
      if ( m_prof.value() ) {
        profile1D( (double)k, (double)v, name + "/sizeVsSourceID",        //
                   name + " bank size (bytes) as a function of sourceID", //
                   s_min, s_max + 1, s_max - s_min + 1, "s" );
      } else {
        plot2D( (double)k, (double)v, name + "/sizeVsSourceID",        //
                name + " bank size (bytes) as a function of sourceID", //
                s_min, s_max + 1, 0., (double)max, s_max - s_min + 1, m_bin.value() );
      }
    }
  } // bank type loop
  m_totalBankSize += total;
}
