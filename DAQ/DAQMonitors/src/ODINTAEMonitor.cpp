/*****************************************************************************\
* (c) Copyright 2023 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/ODIN.h"
#include "Event/TAEUtils.h"
#include "Kernel/STLExtensions.h"
#include "LHCbAlgs/MergingTransformer.h"

#include "Gaudi/Accumulators/Histogram.h"

#include <algorithm>

namespace {
  template <typename T>
  using axis1D        = Gaudi::Accumulators::Axis<T>;
  const auto AxisBCID = axis1D<double>( 3565, -0.5, 3564.5, "BCID" );

  using EventTypes = LHCb::ODIN::EventTypes;

  const auto TAEEventTypes = {EventTypes::et_bit_08, EventTypes::et_bit_09, EventTypes::et_bit_10,
                              EventTypes::et_bit_11, EventTypes::et_bit_12, EventTypes::et_bit_13,
                              EventTypes::et_bit_14, EventTypes::et_bit_15};
} // namespace

using ODINs = Gaudi::Functional::vector_of_const_<LHCb::ODIN const*>;

class ODINTAEMonitor final : public LHCb::Algorithm::MergingConsumer<void( ODINs const& )> {
public:
  ODINTAEMonitor( const std::string& name, ISvcLocator* pSvcLocator )
      : LHCb::Algorithm::MergingConsumer<void( ODINs const& )>{name, pSvcLocator, {"ODINVector", {}}} {}

  void operator()( ODINs const& odins ) const override;

private:
  LHCb::TAE::Handler m_taeHandler{this};

  // some plots are maintained in ODIN/... for consistency with ODINMonitor
  mutable Gaudi::Accumulators::Histogram<1> m_trgtype{this, "ODIN/TrgType", "TrgType", {16, -0.5, 15.5}};
  // /ODINMon/TAEIndex for backward compatibility
  mutable Gaudi::Accumulators::ProfileHistogram<1> m_taeindex{this, "TAEIndex", "TAEIndex vs TAE offset",
                                                              LHCb::TAE::defaultAxis};

  mutable Gaudi::Accumulators::WeightedHistogram<2> m_bcid_taesummary{
      this, "/ODINMon/TAESummary", "TAE summary", AxisBCID,
      axis1D<double>{4 + 3 + static_cast<unsigned int>( TAEEventTypes.size() ),
                     -0.5,
                     -0.5 + 4 + 3 + TAEEventTypes.size(),
                     "",
                     {"ee", "be", "eb", "bb", "isTAE", "central", "first", "et_bit_08 (isolated)",
                      "et_bit_09 (leading)", "et_bit_10 (trailing)", "et_bit_11 (empty before isolated)",
                      "et_bit_12 (empty after isolated)", "et_bit_13 (empty before leading)",
                      "et_bit_14 (empty after trailing)", "et_bit_15 (empty isolated)"}}};
};

DECLARE_COMPONENT( ODINTAEMonitor )

void ODINTAEMonitor::operator()( ODINs const& odinVector ) const {
  auto taeEvents = m_taeHandler.arrangeTAE( odinVector, odinVector, 3 );
  if ( taeEvents.empty() ) { // Something went wrong
    return;
  }

  for ( auto const& element : taeEvents ) {
    int               offset = element.first; // 0 is central, negative for Prev, positive for Next
    LHCb::ODIN const& odin   = element.second.first;

    auto bcid    = odin.bunchId();
    auto bx      = static_cast<uint8_t>( odin.bunchCrossingType() );
    auto trgtype = odin.triggerType();
    // histograms filled only for the central (trigger) event
    if ( offset == 0 ) { ++m_trgtype[trgtype]; }
    // histograms filled for all offsets
    {
      int offset = 0;
      m_bcid_taesummary[{bcid, offset + bx}] += 1;
      offset += 4;
      m_bcid_taesummary[{bcid, offset++}] += odin.isTAE();
      m_bcid_taesummary[{bcid, offset++}] += odin.timeAlignmentEventCentral();
      m_bcid_taesummary[{bcid, offset++}] += odin.timeAlignmentEventFirst();
      for ( auto const& et : TAEEventTypes ) { m_bcid_taesummary[{bcid, offset++}] += odin.eventTypeBit( et ); }
    }
    m_taeindex[offset] += odin.timeAlignmentEventIndex();
  }
}
