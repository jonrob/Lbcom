/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/RawBank.h"
#include "Event/RawBankReadoutStatus.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "GaudiUtils/Aida2ROOT.h"
#include "LHCbAlgs/Consumer.h"
#include <TAxis.h>
#include <TH1D.h>
#include <TH2D.h>

/**
 *  @author Olivier Deschamps
 *  @date   2008-01-23
 */
class RawBankReadoutStatusMonitor
    : public LHCb::Algorithm::Consumer<void( LHCb::RawBankReadoutStatuss const& ),
                                       Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {
public:
  RawBankReadoutStatusMonitor( const std::string&, ISvcLocator* );

  StatusCode initialize() override;
  void       operator()( LHCb::RawBankReadoutStatuss const& ) const override;

private:
  void                                      setLabels1D( AIDA::IHistogram1D* histo1D ) const;
  void                                      setLabels2D( AIDA::IHistogram2D* histo2D ) const;
  int                                       m_degree;
  Gaudi::Property<std::vector<std::string>> m_bankTypes{this, "bankNames"};
  Gaudi::Property<int>         m_nSources{this, "NumberOfSources", -1}; // Number Of Source in the StatusMap
  Gaudi::Property<std::string> m_lab{this, "xLabelOptions", ""};
  Gaudi::Property<std::vector<std::string>> m_labels{this, "axisLabels"};
};

// Declaration of the Algorithm Factory
DECLARE_COMPONENT( RawBankReadoutStatusMonitor )

RawBankReadoutStatusMonitor::RawBankReadoutStatusMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name, pSvcLocator, {"RawBankReadoutStatueLocation", LHCb::RawBankReadoutStatusLocation::Default}} {

  // default labelling
  m_degree          = 0;
  unsigned int word = 1;
  m_labels.value().push_back( "Counter" );
  while ( word <= LHCb::RawBankReadoutStatus::Status::Unknown ) {
    std::stringstream slabel;
    slabel << static_cast<LHCb::RawBankReadoutStatus::Status>( word );
    m_labels.value().push_back( slabel.str() );
    m_degree++;
    word *= 2;
  }
  setHistoDir( name );
}

StatusCode RawBankReadoutStatusMonitor::initialize() {
  return GaudiHistoAlg::initialize().andThen( [&] {
    info() << "---- Histo bins label : " << endmsg;
    int k = -1;
    for ( const auto& i : m_labels ) { info() << " | " << k++ << " = '" << i << "' | " << endmsg; }
  } );
}

void RawBankReadoutStatusMonitor::operator()( LHCb::RawBankReadoutStatuss const& statuss ) const {

  if ( msgLevel( MSG::DEBUG ) ) debug() << "==> Execute" << endmsg;

  if ( 0 == statuss.size() ) {
    Warning( "The RawBankReadoutStatus container is empty *" + LHCb::RawBankReadoutStatusLocation::Default + ")" )
        .ignore();
    for ( const auto& typeName : m_bankTypes ) {
      std::stringstream base;
      base << typeName << "/";
      std::stringstream tit1D;
      tit1D << "Status summary for " << typeName << " bank ";
      AIDA::IHistogram1D* histo1D = plot1D( LHCb::RawBankReadoutStatus::Status::MissingStatus, base.str() + "1",
                                            tit1D.str(), -1, (double)m_degree, m_degree + 1 );
      plot1D( -1, base.str() + "1", tit1D.str(), -1, (double)m_degree, m_degree + 1 );
      setLabels1D( histo1D );
    }
  }

  for ( const LHCb::RawBankReadoutStatus* status : statuss ) {
    const auto&             statusMap = status->statusMap();
    LHCb::RawBank::BankType type      = status->key();
    const auto&             typeName  = LHCb::RawBank::typeName( type );
    bool ok = m_bankTypes.empty() || std::find( m_bankTypes.begin(), m_bankTypes.end(), typeName ) != m_bankTypes.end();
    if ( !ok ) continue;

    std::stringstream base;
    base << typeName << "/";
    // 1D summary histogram for the whole bank
    std::stringstream tit1D( "" );
    tit1D << "Status summary for " << typeName << " bank ";
    AIDA::IHistogram1D* histo1D = plot1D( -1., base.str() + "1", tit1D.str(), -1, (double)m_degree, m_degree + 1 );
    setLabels1D( histo1D );
    if ( msgLevel( MSG::DEBUG ) ) debug() << "Status " << status->status() << endmsg;
    for ( int i = 0; i < m_degree; ++i ) {
      const int word = status->status() >> i;
      const int isok = ( 0x1 & word );
      if ( isok ) plot1D( (double)i, base.str() + "1", tit1D.str(), -1, (double)m_degree, m_degree + 1 );
      if ( msgLevel( MSG::DEBUG ) ) debug() << i << " -> " << isok << endmsg;
    }

    // 2D summary histogram as a function of the sourceID
    std::stringstream tit2D;
    tit2D << "Status as a function of sourceID for " << typeName << " bank ";
    double min = 0.;
    double max = ( m_nSources.value() > 0
                       ? m_nSources.value()
                       : std::accumulate( statusMap.begin(), statusMap.end(), -1,
                                          []( int mx, const auto& kv ) { return std::max( mx, kv.first ); } ) ) +
                 1;
    int bin = (int)max;

    for ( auto&& [k, v] : statusMap ) {
      double              source = k;
      AIDA::IHistogram2D* histo2D =
          plot2D( source, -1., base.str() + "2", tit2D.str(), min, max, -1., (double)m_degree, bin, m_degree + 1 );
      setLabels2D( histo2D );
      for ( int i = 0; i < m_degree; ++i ) {
        if ( 0x1 & ( v >> i ) )
          plot2D( source, (double)i, base.str() + "2", tit2D.str(), min, max, -1., (double)m_degree, bin,
                  m_degree + 1 );
      }
    }
  }
}

void RawBankReadoutStatusMonitor::setLabels1D( AIDA::IHistogram1D* histo ) const {
  TH1D*  th1d  = Gaudi::Utils::Aida2ROOT::aida2root( histo );
  int    k     = 1;
  TAxis* xAxis = th1d->GetXaxis();
  for ( const auto& i : m_labels ) xAxis->SetBinLabel( k++, i.c_str() );
  xAxis->LabelsOption( m_lab.value().c_str() );
}
void RawBankReadoutStatusMonitor::setLabels2D( AIDA::IHistogram2D* histo ) const {
  TH2D*  th2d  = Gaudi::Utils::Aida2ROOT::aida2root( histo );
  int    k     = 1;
  TAxis* yAxis = th2d->GetYaxis();
  for ( const auto& i : m_labels ) yAxis->SetBinLabel( k++, i.c_str() );
}
