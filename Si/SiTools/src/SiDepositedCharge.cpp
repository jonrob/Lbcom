/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "GaudiKernel/IRndmGenSvc.h"
#include "GaudiKernel/RndmGenerators.h"
#include "Kernel/LHCbConstants.h"
#include "Kernel/SiLandauFun.h"
#include "MCInterfaces/ISiDepositedCharge.h"
#include "SiDepositedChargeBase.h"

using namespace LHCb;
using namespace Gaudi::Units;

/** @class SiDepositedCharge SiDepositedCharge.h
 *
 *  Landau convolved with a Gaussian - see <a href="http://cdsweb.cern.ch/record/690291">LHCb 2003-160</a>
 *
 *  @author M.Needham
 *  @date   13/3/2002
 */

class SiDepositedCharge : public extends<SiDepositedChargeBase, ISiDepositedCharge> {

public:
  using extends::extends;

  /** initialize */
  StatusCode initialize() override;

  /** calculate deposited charge (in electrons)
   * @param  aHit hit
   * @return deposited charge
   */
  double charge( const LHCb::MCHit* aHit ) const override;

private:
  SmartIF<IRndmGen> m_LandauDist;
};

DECLARE_COMPONENT( SiDepositedCharge )

StatusCode SiDepositedCharge::initialize() {
  StatusCode sc = SiDepositedChargeBase::initialize();
  if ( sc.isFailure() ) return sc;

  /// initialize generators .
  auto tRandNumSvc = service<IRndmGenSvc>( "RndmGenSvc", true );
  m_LandauDist     = tRandNumSvc->generator( Rndm::Landau( 0.2226, 1.0 ) );
  if ( !m_LandauDist ) return Error( "Failed to init generator ", sc );

  return sc;
}

double SiDepositedCharge::charge( const MCHit* aHit ) const {
  // calculate - deposited charge Landau convolved with Gauss
  // see for example Bichsel '88
  double            pathLength = m_scalingFactor * aHit->pathLength();
  const MCParticle* aParticle  = aHit->mcParticle();
  double            beta       = aHit->p() / std::hypot( aParticle->virtualMass(), aHit->p() );
  beta                         = std::max( 0.2, beta );
  double betaGamma             = aHit->p() / aParticle->virtualMass();

  // calculate scale of landau
  double scale = SiLandauFun::scale( beta, pathLength );

  return ( SiLandauFun::MPVFromScale( beta, betaGamma, scale ) + ( m_LandauDist->shoot() * scale ) +
           ( m_GaussDist->shoot() * atomicBinding( pathLength ) ) ) /
         LHCbConstants::SiEnergyPerIonPair;
}
