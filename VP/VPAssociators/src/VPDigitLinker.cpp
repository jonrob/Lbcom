/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Associators/Location.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/MCVPDigit.h"
#include "Event/VPCluster.h"
#include "Event/VPDigit.h"
#include "Kernel/VPConstants.h"
#include <map>

namespace LHCb::VP {

  /**
   *  This algorithm creates association tables between the channel IDs of
   *  the VP digits and the corresponding MC hits and particles.
   *
   */
  struct DigitLinker
      : Algorithm::MultiLinker<std::tuple<LinksByKey, LinksByKey>( const VPDigits&, const MCVPDigits& )> {

    DigitLinker( const std::string& name, ISvcLocator* pSvcLocator );
    std::tuple<LinksByKey, LinksByKey> operator()( const VPDigits&, const MCVPDigits& ) const override;
  };

  DECLARE_COMPONENT_WITH_ID( DigitLinker, "VPDigitLinker" )
} // namespace LHCb::VP

namespace {
  template <typename Container>
  const LHCb::MCHit* substituteDeltaRays( const LHCb::MCHit* hit, const Container& hits ) {
    auto particle = hit->mcParticle();
    // Check if the hit originates from a delta ray.
    // Only If no other hit from the mother particle is found,
    // add the delta ray hit as a separate entry.
    if ( particle && particle->originVertex() &&
         particle->originVertex()->type() == LHCb::MCVertex::MCVertexType::DeltaRay ) {
      // Check if there is another hit from the mother particle of the delta.
      auto mhit = std::find_if( hits.begin(), hits.end(), [mother = particle->mother()]( const LHCb::MCHit* h ) {
        // some mchits may be nullptr if from spillover
        if ( h == static_cast<LHCb::MCHit*>( nullptr ) ) return false;
        return h->mcParticle() == mother;
      } );
      if ( mhit != hits.end() ) {
        // Add the deposit to the hit of the mother particle.
        return *mhit;
      }
    }
    return hit;
  }
  // match between VPDigit and MCVPDigit based on channelID, allowing for cardinal direction crosstalk
  LHCb::MCVPDigit const* matchChannels( const LHCb::MCVPDigits& mcdigits, const LHCb::Detector::VPChannelID channel ) {
    const auto mcDigit = mcdigits.object( channel );
    if ( mcDigit ) return mcDigit;
    // see if a pixel next to this has signal and this is from crosstalk
    auto sensor = channel.sensor();
    auto chip   = channel.chip();
    auto row    = to_unsigned( channel.row() );
    auto col    = to_unsigned( channel.col() );
    // right: col = col+1
    if ( col + 1 < VP::NColumns ) {
      LHCb::Detector::VPChannelID newChan( sensor, chip, LHCb::Detector::VPChannelID::ColumnID( col + 1 ),
                                           LHCb::Detector::VPChannelID::RowID( row ) );
      const auto                  mcDigit = mcdigits.object( newChan );
      if ( mcDigit ) return mcDigit;
    }
    // left: col = col-1
    if ( col > 0 ) {
      LHCb::Detector::VPChannelID newChan( sensor, chip, LHCb::Detector::VPChannelID::ColumnID( col - 1 ),
                                           LHCb::Detector::VPChannelID::RowID( row ) );
      const auto                  mcDigit = mcdigits.object( newChan );
      if ( mcDigit ) return mcDigit;
    }
    // bottom: row = row-1
    if ( row > 0 ) {
      LHCb::Detector::VPChannelID newChan( sensor, chip, LHCb::Detector::VPChannelID::ColumnID( col ),
                                           LHCb::Detector::VPChannelID::RowID( row - 1 ) );
      const auto                  mcDigit = mcdigits.object( newChan );
      if ( mcDigit ) return mcDigit;
    }
    // top: row = row+1
    if ( row + 1 < VP::NRows ) {
      LHCb::Detector::VPChannelID newChan( sensor, chip, LHCb::Detector::VPChannelID::ColumnID( col ),
                                           LHCb::Detector::VPChannelID::RowID( row + 1 ) );
      const auto                  mcDigit = mcdigits.object( newChan );
      if ( mcDigit ) return mcDigit;
    }
    return nullptr;
  }
} // namespace

LHCb::VP::DigitLinker::DigitLinker( const std::string& name, ISvcLocator* svcLoc )
    : MultiLinker( name, svcLoc,
                   {KeyValue{"DigitLocation", VPDigitLocation::Default},
                    KeyValue{"MCDigitLocation", MCVPDigitLocation::Default}},
                   {KeyValue{"HitLinkLocation", Links::location( VPDigitLocation::Default + "2MCHits" )},
                    KeyValue{"ParticleLinkLocation", Links::location( VPDigitLocation::Default )}} ) {}

std::tuple<LHCb::LinksByKey, LHCb::LinksByKey> LHCb::VP::DigitLinker::
                                               operator()( const LHCb::VPDigits& digits, const LHCb::MCVPDigits& mcdigits ) const {
  // Create association tables.
  auto hitLinks      = outputLinks<MCHit>();
  auto particleLinks = outputLinks<MCParticle>();

  // Loop over the digits.
  for ( const auto digit : digits ) {
    // Get the MC digit with the same channel ID.
    const auto mcDigit = matchChannels( mcdigits, digit->key() );
    if ( !mcDigit ) continue;
    // Get the MC hits associated to the MC digit and their weights.
    const auto&        hits            = mcDigit->mcHits();
    const auto&        depositAndTimes = mcDigit->depositAndTimes();
    const unsigned int nDeposits       = depositAndTimes.size();
    // Make a map of the MC hits/particles and weights.
    std::map<const MCHit*, double>      hitMap;
    std::map<const MCParticle*, double> particleMap;
    for ( unsigned int i = 0; i < nDeposits; ++i ) {
      const auto hit = hits[i];
      // test if SmartRef<MCHit> points to an MCHit
      if ( hit.data() == static_cast<MCHit*>( nullptr ) ) continue; // spillover MCHit contributed to this digit
      const double weight = depositAndTimes[i].first;
      const MCHit* subHit = substituteDeltaRays( hit, hits );
      hitMap[subHit] += weight;
      particleMap[subHit->mcParticle()] += weight;
    }
    // Make the associations
    hitLinks.link( digit->key(), hitMap );
    particleLinks.link( digit->key(), particleMap );
  }
  return {std::move( hitLinks ), std::move( particleLinks )};
}
