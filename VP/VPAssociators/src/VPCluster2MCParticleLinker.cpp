/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/VPDigit.h"
#include "Event/VPLightCluster.h"
#include <Associators/Location.h>

namespace LHCb::VP {

  /**
   * These algorithms create association tables between the VP clusters
   * and the corresponding MC particles, based on the association tables
   * for individual pixels produced by VPDigitLinker.
   */
  struct Cluster2MCParticleLinker
      : Algorithm::Linker<LinksByKey( const std::vector<VPLightCluster>&, const LinksByKey& )> {
    Cluster2MCParticleLinker( const std::string& name, ISvcLocator* pSvcLocator );

    StatusCode initialize() override;

    LinksByKey operator()( const std::vector<VPLightCluster>& clusters,
                           const LinksByKey&                  digitLinks ) const override; // < Algorithm execution
  };

  DECLARE_COMPONENT_WITH_ID( Cluster2MCParticleLinker, "VPCluster2MCParticleLinker" )

} // namespace LHCb::VP

LHCb::VP::Cluster2MCParticleLinker::Cluster2MCParticleLinker( const std::string& name, ISvcLocator* svcLoc )
    : Linker( name, svcLoc,
              {KeyValue{"ClusterLocation", VPClusterLocation::Light},
               KeyValue{"VPDigit2MCParticleLinksLocation", Links::location( VPDigitLocation::Default )}},
              KeyValue{"OutputLocation", Links::location( VPClusterLocation::Light )} ) {}

StatusCode LHCb::VP::Cluster2MCParticleLinker::initialize() {
  return Linker::initialize().andThen( [&] {
    warning()
        << "VPLightClusters no longer contain a vector of pixels contributing to the cluster, for performance reasons"
        << endmsg;
    warning() << "Efficiencies will not be 100% correct." << endmsg;
    warning() << "Use VPFullClusters and VPFullCluster2MCParticleLinker to obtain the correct efficiencies." << endmsg;
  } );
}

LHCb::LinksByKey LHCb::VP::Cluster2MCParticleLinker::operator()( const std::vector<LHCb::VPLightCluster>& clusters,
                                                                 const LHCb::LinksByKey& bareDigitLinks ) const {
  // Get the association table between digits and particles.
  auto digitLinks = inputLinks<MCParticle>( bareDigitLinks );

  auto output = LHCb::LinksByKey{std::in_place_type<VPLightCluster>, std::in_place_type<MCParticle>,
                                 LHCb::LinksByKey::Order::decreasingWeight};
  // Loop over the clusters.
  for ( const VPLightCluster& cluster : clusters ) {
    std::map<const MCParticle*, double> particleMap;
    // Get the pixels in the cluster, not available anymore in VPLightClusters.
    // const auto& pixels = cluster.pixels();
    // double sum = 0.;
    // for (const auto& pixel : pixels) {
    //    for (const auto& entry : digitLinks.from(pixel)) {
    //       const double weight = entry.weight();
    //       particleMap[entry.to()] += weight;
    //       sum += weight;
    //    }
    // }
    const auto& centralpixel = cluster.channelID();
    double      sum          = 0.;
    for ( const auto& entry : digitLinks.from( centralpixel ) ) {
      const double weight = entry.weight();
      particleMap[entry.to()] += weight;
      sum += weight;
    }
    if ( sum < 1.e-2 ) continue;
    for ( auto [hit, weight] : particleMap ) output.link( cluster.channelID(), hit, weight / sum );
  }
  return output;
}
