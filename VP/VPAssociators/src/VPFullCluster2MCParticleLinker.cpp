/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Associators/Linker.h"
#include "Event/LinksByKey.h"
#include "Event/MCParticle.h"
#include "Event/VPDigit.h"
#include "Event/VPFullCluster.h"
#include "GaudiKernel/LinkManager.h"
#include <Associators/Location.h>

namespace LHCb::VP {

  /**
   * These algorithms create association tables between the VP clusters
   * and the corresponding MC particles, based on the association tables
   * for individual pixels produced by VPDigitLinker.
   */
  struct FullCluster2MCParticleLinker
      : Algorithm::Linker<LinksByKey( const std::vector<VPFullCluster>&, const MCParticles&, const LinksByKey& )> {
    FullCluster2MCParticleLinker( const std::string& name, ISvcLocator* pSvcLocator );

    LinksByKey operator()( const std::vector<VPFullCluster>&, const MCParticles&,
                           const LinksByKey& ) const override; // < Algorithm execution
  };

  DECLARE_COMPONENT_WITH_ID( FullCluster2MCParticleLinker, "VPFullCluster2MCParticleLinker" )

} // namespace LHCb::VP

LHCb::VP::FullCluster2MCParticleLinker::FullCluster2MCParticleLinker( const std::string& name, ISvcLocator* svcLoc )
    : Linker( name, svcLoc,
              {KeyValue{"ClusterLocation", VPFullClusterLocation::Default},
               KeyValue{"MCParticlesLocation", MCParticleLocation::Default},
               KeyValue{"VPDigit2MCParticleLinksLocation", Links::location( VPDigitLocation::Default )}},
              KeyValue{"OutputLocation", Links::location( VPFullClusterLocation::Default )} ) {}

LHCb::LinksByKey LHCb::VP::FullCluster2MCParticleLinker::operator()( const std::vector<LHCb::VPFullCluster>& clusters,
                                                                     const LHCb::MCParticles&                mcParts,
                                                                     const LHCb::LinksByKey& bareDigitLinks ) const {
  LinksByKey output{std::in_place_type<VPFullCluster>, std::in_place_type<MCParticle>};
  // Loop over the clusters.
  for ( const VPFullCluster& cluster : clusters ) {
    std::map<unsigned int, double> particleMap;
    // Get the pixels in the cluster, not available anymore in VPFullClusters
    const auto& pixels = cluster.pixels();
    double      sum    = 0.;
    for ( const auto& pixel : pixels ) {
      bareDigitLinks.applyToLinks( pixel, [&particleMap, &sum]( unsigned int, unsigned int tgtIndex, float weight ) {
        particleMap[tgtIndex] += weight;
        sum += weight;
      } );
    }
    if ( sum < 1.e-2 ) continue;
    for ( const auto [mcPartIndex, partWeight] : particleMap ) {
      output.link( cluster.channelID(), mcParts( mcPartIndex ), partWeight / sum );
    }
  }
  return output;
}
