###############################################################################
# (c) Copyright 2000-2021 CERN for the benefit of the LHCb Collaboration      #
#                                                                             #
# This software is distributed under the terms of the GNU General Public      #
# Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   #
#                                                                             #
# In applying this licence, CERN does not waive the privileges and immunities #
# granted to it by virtue of its status as an Intergovernmental Organization  #
# or submit itself to any jurisdiction.                                       #
###############################################################################
#[=======================================================================[.rst:
VP/VPAssociators
----------------
#]=======================================================================]

gaudi_add_module(VPAssociators
    SOURCES
        src/VPCluster2MCHitLinker.cpp
        src/VPCluster2MCParticleLinker.cpp
        src/VPDigitLinker.cpp
        src/VPFullCluster2MCHitLinker.cpp
        src/VPFullCluster2MCParticleLinker.cpp
    LINK
        Gaudi::GaudiKernel
        LHCb::AssociatorsBase
        LHCb::DigiEvent
        LHCb::LinkerEvent
        LHCb::MCEvent
)
