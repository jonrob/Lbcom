/*****************************************************************************\
* (c) Copyright 2021 CERN for the benefit of the LHCb Collaboration           *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/

#include "Event/VPDigit.h"
#include "Kernel/VPConstants.h"
#include "LHCbAlgs/Consumer.h"

#include "GaudiAlg/GaudiHistoAlg.h"

#include <AIDA/IHistogram1D.h>
#include <AIDA/IHistogram2D.h>

#include <string>
#include <vector>

namespace {
  constexpr auto NAsics = VP::NSensorsPerModule * VP::NChipsPerSensor;
  // Transform sensor number to range [0,3] for plotting purposes
  constexpr uint get_sensorNr( uint sensor ) { return sensor % VP::NSensorsPerModule; }
  // Transform ASIC number to range [0,11] for plotting purposes
  constexpr uint get_asicNr( uint sensorNr, uint chipNr ) { return 3 * sensorNr + chipNr; }
} // namespace

namespace LHCb {

  /**
   * * Class for plotting VP hits
   *
   * * Notes:
   *   - VP hit maps currently superpose all modules
   *
   * @author T.H.McGrath
   * @date   2021-02-09
   */
  class VPHitMonitors
      : public Algorithm::Consumer<void( const VPDigits& ), Gaudi::Functional::Traits::BaseClass_t<GaudiHistoAlg>> {

  public:
    // Constructor
    VPHitMonitors( const std::string& name, ISvcLocator* pSvcLocator );
    // Monitor initailization
    StatusCode initialize() override;
    // Functional operator
    void operator()( const VPDigits& ) const override;

  private:
    // Histograms
    AIDA::IHistogram1D*              m_hmod = nullptr; // Number of pixel hits per module
    AIDA::IHistogram1D*              m_hsen = nullptr; // Number of pixel hits per sensor
    std::vector<AIDA::IHistogram2D*> m_hmaps;          // Number of pixel hist per pixel on ASIC

  }; // class VPHitMonitor

  // Declaration of the Algorithm Factory
  DECLARE_COMPONENT_WITH_ID( VPHitMonitors, "VPHitMonitors" )

} // namespace LHCb

LHCb::VPHitMonitors::VPHitMonitors( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer( name, pSvcLocator, {KeyValue{"DigitLocation", VPDigitLocation::Default}} ) {}

StatusCode LHCb::VPHitMonitors::initialize() {
  return Consumer::initialize().andThen( [&] {
    // Histogram booking base
    auto bookHisto = [this]( std::string name, std::string title, uint nBins, double min, double max ) {
      AIDA::IHistogram1D* h = book1D( name, title, min, max, nBins );
      declareInfo( name, h, title );
      return h;
    };
    auto bookHisto2D = [this]( std::string name, std::string title, uint nBinsX, double minX, double maxX, uint nBinsY,
                               double minY, double maxY ) {
      AIDA::IHistogram2D* h2 = book2D( name, title, minX, maxX, nBinsX, minY, maxY, nBinsY );
      declareInfo( name, h2, title );
      return h2;
    };

    m_hmod = bookHisto( "VPHitsPerModule", "VP pixel hits per module (Hits vs Module); Module; Hits", VP::NModules, 0,
                        VP::NModules );
    m_hsen = bookHisto( "VPHitsPerSensor", "VP pixel hits per sensor (Hits vs Sensor); Sensor; Hits", VP::NSensors, 0,
                        VP::NSensors );

    // Book vector of hit maps for ASIC
    m_hmaps.resize( NAsics, nullptr );
    for ( uint asicNr = 0; asicNr < NAsics; ++asicNr ) {

      m_hmaps[asicNr] = bookHisto2D( "VPHitsPerPixelOnASIC" + std::to_string( asicNr ),
                                     "VP pixel hits per pixel on VP" + std::to_string( asicNr / 3 ) +
                                         std::to_string( asicNr % 3 ) + " for all Modules (Row vs Column); Column; Row",
                                     VP::NColumns, 0, VP::NColumns, VP::NRows, 0, VP::NRows );
    }
    return StatusCode::SUCCESS;
  } );
}

void LHCb::VPHitMonitors::operator()( const LHCb::VPDigits& vpDigits ) const {
  for ( const VPDigit* vpDigit : vpDigits ) {
    const auto& channel = vpDigit->channelID();

    uint module = channel.module();
    uint sensor = to_unsigned( channel.sensor() );
    uint chip   = to_unsigned( channel.chip() );
    uint column = to_unsigned( channel.col() );
    uint row    = to_unsigned( channel.row() );
    uint asicNr = get_asicNr( get_sensorNr( sensor ), chip );

    // info() << "Column: " << column << " Row: " << row << endmsg;

    m_hmod->fill( module );
    m_hsen->fill( sensor );
    m_hmaps[asicNr]->fill( column, row );
  }
}
