/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/Track.h"
#include "GaudiAlg/GaudiTupleAlg.h"
#include "LHCbAlgs/Consumer.h"
#include "Linker/LinkedFrom.h"
#include "Linker/LinkedTo.h"
#include "VPDet/DeVP.h"

namespace LHCb {
  /**
   * Class for Velo track monitoring Ntuple
   *  @author T. Bird
   *  @date   2013-10-02
   */
  class VPTrackEff : public Algorithm::Consumer<void( const MCParticles&, const MCHits&, const DeVP& ),
                                                Algorithm::Traits::usesBaseAndConditions<GaudiTupleAlg, DeVP>> {

  public:
    VPTrackEff( const std::string& name, ISvcLocator* pSvcLocator );
    void       operator()( const MCParticles&, const MCHits&, const DeVP& ) const override;
    StatusCode initialize() override;

  private:
    const MCVertex*              findMCOriginVertex( const MCParticle& particle, const double decaylengthtolerance );
    Gaudi::Property<std::string> m_trackLocation{this, "TrackLocation", TrackLocation::Velo};
  };

  DECLARE_COMPONENT_WITH_ID( VPTrackEff, "VPTrackEff" )

} // namespace LHCb

namespace {
  // Walk up the decay tree of a particle and determine its origin vertex.
  const LHCb::MCVertex* findMCOriginVertex( const LHCb::MCParticle& particle,
                                            const double            decaylengthtolerance = 1.e-3 ) {
    const LHCb::MCVertex* ov = particle.originVertex();
    if ( !ov ) return ov;
    const LHCb::MCParticle* mother = ov->mother();
    if ( mother && mother != &particle ) {
      const LHCb::MCVertex* mov = mother->originVertex();
      if ( !mov ) return ov;
      const double d = ( mov->position() - ov->position() ).R();
      if ( mov == ov || d < decaylengthtolerance ) { ov = findMCOriginVertex( *mother, decaylengthtolerance ); }
    }
    return ov;
  }

  // Return the origin vertex type of the mother particle.
  int mcMotherType( const LHCb::MCParticle& particle ) {
    const LHCb::MCVertex& vertex = findMCOriginVertex( particle );
    int                   rc( -1 );
    if ( vertex.isPrimary() )
      rc = 0;
    else if ( const LHCb::MCParticle* mother = vertex.mother(); vertex.mother() ) {
      if ( mother->particleID().hasBottom() )
        rc = 3;
      else if ( mother->particleID().hasCharm() )
        rc = 2;
      else if ( mother->particleID().hasStrange() )
        rc = 1;
      else
        rc = 4;
    }
    return rc;
  }

  // Return the origin vertex type of a particle.
  int mcVertexType( const LHCb::MCParticle& particle ) {
    const LHCb::MCVertex& vertex = findMCOriginVertex( particle );
    return vertex.type();
  }
} // namespace

LHCb::VPTrackEff::VPTrackEff( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"MCparticleLocation", MCParticleLocation::Default},
                KeyValue{"MCHitLocation", MCHitLocation::VP}, KeyValue{"DeVPLocation", DeVPLocation::Default}}} {}

StatusCode LHCb::VPTrackEff::initialize() {
  return Consumer::initialize().andThen( [&] { setHistoTopDir( "VP/" ); } );
}

void LHCb::VPTrackEff::operator()( const LHCb::MCParticles& particles, const LHCb::MCHits& hits,
                                   const DeVP& det ) const {

  const auto linker        = get<LHCb::LinksByKey>( LHCb::LinksByKey::linkerName( m_trackLocation ) );
  const auto directLinker  = LinkedTo<MCParticle>{linker};
  const auto inverseLinker = LinkedFrom<Track>{linker};

  // Create a map from all MCParticles to MCHits
  std::map<const MCParticle*, std::vector<const MCHit*>> mchitmap;
  for ( const MCParticle* particle : particles ) { mchitmap[particle] = {}; }

  // First collect hits
  for ( const MCHit* hit : hits ) {
    if ( !hit ) {
      warning() << "MCHit shouldn't be NULL!" << endmsg;
      continue;
    }
    // Skip hits not linked to a particle.
    if ( !hit->mcParticle() ) continue;
    const double z = hit->entry().z();
    if ( z < -300 * Gaudi::Units::mm || z < 800 * Gaudi::Units::mm ) { continue; }
    bool inAA( false );
    det.runOnAllSensors( [&hit, &inAA]( const DeVPSensor& sensor ) {
      if ( inAA ) return;
      if ( !sensor.isInside( hit->midPoint() ) ) return;
      inAA = sensor.isInActiveArea( sensor.globalToLocal( hit->midPoint() ) );
    } );
    if ( inAA ) { mchitmap[hit->mcParticle()].push_back( hit ); }
  }

  Tuple tuple = nTuple( "VPAcceptance", "" );

  for ( const MCParticle* particle : particles ) {
    const bool forward = particle->momentum().z() > 0.;
    const int  ntracks = int( inverseLinker.range( particle ).size() );

    // Determine origin vertex, decay vertex, and primary vertex.
    double          fd = -1.;
    Gaudi::XYZPoint ov( -99999., -99999., -99999. );
    Gaudi::XYZPoint dv( -99999., -99999., -99999. );
    Gaudi::XYZPoint pv( -99999., -99999., -99999. );

    if ( particle->endVertices().size() > 0 ) { dv = particle->endVertices().front()->position(); }
    if ( particle->originVertex() != 0x0 ) {
      ov = particle->originVertex()->position();
      if ( particle->endVertices().size() > 0 ) {
        fd = ( dv - ov ).R();
      } else {
        fd = 99999.;
      }
    }
    if ( particle->primaryVertex() != 0x0 ) { pv = particle->primaryVertex()->position(); }

    Gaudi::XYZPoint firsthit( 99999., 99999., 99999. );
    if ( !forward ) firsthit = Gaudi::XYZPoint( 99999., 99999., -99999. );
    int nmcparts( -999 );
    int nhits( 0 );
    int nhitsleft( 0 );
    int nhitsright( 0 );
    if ( ntracks > 0 ) {
      const Track* track = inverseLinker.range( particle ).try_front();
      nmcparts           = int( directLinker.range( track ).size() );

      for ( const LHCbID& lhcbID : track->lhcbIDs() ) {
        if ( !track->isOnTrack( lhcbID ) ) continue;
        if ( !lhcbID.isVP() ) continue;
        const DeVPSensor& sensor = det.sensor( lhcbID.vpID() );
        ++nhits;
        if ( sensor.isLeft() ) {
          ++nhitsleft;
        } else if ( sensor.isRight() ) {
          ++nhitsright;
        }

        Gaudi::XYZPoint currenthit( sensor.channelToGlobalPoint( lhcbID.vpID() ) );
        if ( forward ) {
          if ( firsthit.z() > currenthit.z() ) firsthit = currenthit;
        } else {
          if ( firsthit.z() < currenthit.z() ) firsthit = currenthit;
        }
      }
    } // ntracks > 0

    tuple->column( "p", particle->p() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pt", particle->pt() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pid", particle->particleID().pid() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "threecharge", particle->particleID().threeCharge() )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pvx", pv.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pvy", pv.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "pvz", pv.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "ovx", ov.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "ovy", ov.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "ovz", ov.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "dvx", dv.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "dvy", dv.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "dvz", dv.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhx", firsthit.x() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhy", firsthit.y() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhz", firsthit.z() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhr", firsthit.Rho() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fhphi", firsthit.phi() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "fd", fd ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "eta", particle->momentum().eta() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "phi", particle->momentum().phi() ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "forward", forward ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "frompv", int( particle->originVertex()->isPrimary() ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nendvertex", int( particle->endVertices().size() ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "ntracks", ntracks ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nhits", nhits ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nhitsleft", nhitsleft ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nhitsright", nhitsright ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nmchits", int( mchitmap.find( particle )->second.size() ) )
        .ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "nmcparts", nmcparts ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "mcmothertype", mcMotherType( *particle ) ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
    tuple->column( "mcvertextype", mcVertexType( *particle ) ).ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );

    tuple->write().ignore( /* AUTOMATICALLY ADDED FOR gaudi/Gaudi!763 */ );
  }
}
