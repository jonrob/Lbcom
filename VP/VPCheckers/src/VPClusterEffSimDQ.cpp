/*****************************************************************************\
* (c) Copyright 2000-2022 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/LinksByKey.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/VPDigit.h"
#include "Event/VPFullCluster.h"
#include "LHCbAlgs/Consumer.h"
#include <Gaudi/Accumulators/Histogram.h>
#include <Gaudi/Algorithm.h>
#include <math.h>

/** @class VPClusterEffSimDQ VPClusterEffSimDQ.h
 *
 * Checks the VPCluster efficiency for simulation
 * (copied from VPClusterEfficiency in Rec for Boole SimDQ purposes)
 */

namespace LHCb {
  class VPClusterEffSimDQ
      : public Algorithm::Consumer<void( const std::vector<LHCb::VPFullCluster>&, const LHCb::MCHits&,
                                         const LHCb::MCParticles&, const LHCb::LinksByKey& )> {
  public:
    /// Standard constructor
    VPClusterEffSimDQ( const std::string& name, ISvcLocator* pSvcLocator );
    /// Consumer operator: takes VP clusters & MCHits to make efficiency plots
    void operator()( const std::vector<LHCb::VPFullCluster>&, const LHCb::MCHits&, const LHCb::MCParticles&,
                     const LHCb::LinksByKey& ) const override;

  private:
    // number of calls
    mutable Gaudi::Accumulators::Counter<> m_numCalls{this, "numCalls"};

    const std::string hpath = "/VP/VPClusterEffSimDQ/";

    // MC hit pos and efficiency
    mutable Gaudi::Accumulators::Histogram<1> m_sensID{
        this, hpath + "MCHitsSensID", "Number of MCHits verse sensor number", {208, -0.5, 207.5}};
    mutable Gaudi::Accumulators::Histogram<2> m_mcHitXY{
        this, hpath + "MCHitXY", "All MCHit x,y VP all modules", {{240, -60, 60}, {240, -60, 60}}};
    mutable Gaudi::Accumulators::Histogram<1> m_mcHitLen{
        this, hpath + "MCHitLen", "Distance traveled by MCHit in VP sensor (mm)", {100, 0., 1.}};
    mutable Gaudi::Accumulators::Histogram<1> m_mcHitEnergy{
        this, hpath + "MCHitEnergy", "Energy deposited by MCHit in VP sensor (MeV)", {100, 0., 1.}};
    mutable Gaudi::Accumulators::ProfileHistogram<2> m_eff2DMCHit{
        this, hpath + "eff2DMCHit", "Efficiency MCHit -> Pixel x,y VP all modules", {{240, -60, 60}, {240, -60, 60}}};
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_effPath{
        this,
        hpath + "effPath",
        "Efficiency (MCHit->Pixel) v distance traveled by MCHit in VP sensor (mm)",
        {100, 0., 1.}};
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_effRadius{
        this, hpath + "effRadius", "Efficiency (MCHit->Pixel) v radius in the global frame (mm)", {100, 0., 50.}};
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_effSensorNum{
        this, hpath + "effSensorNum", "Efficiency (MCHit->Pixel) v sensor number", {208, -0.5, 207.5}};

    // cluster from MCHit information
    mutable Gaudi::Accumulators::Histogram<1> m_hitPerPix{
        this, hpath + "hitPerPix", "Number of MCHits per VP pixel", {10, -0.5, 9.5}};
    mutable Gaudi::Accumulators::Histogram<1> m_fracNoise{
        this, hpath + "fracNoise", "Fraction pixels spill or noise per cluster", {26, 0., 1.04}};
    mutable Gaudi::Accumulators::Histogram<2> m_fracNoiseSize{
        this,
        hpath + "fracNoiseSize",
        "Fraction pixels spill or noise per cluster v cluster size",
        {{26, 0., 1.04}, {50, 0.5, 50.5}}};
    mutable Gaudi::Accumulators::Histogram<2> m_goodClus2D{
        this, hpath + "goodClus2D", "Good (>70% true) Clusters pos all modules", {{240, -60, 60}, {240, -60, 60}}};
    mutable Gaudi::Accumulators::Histogram<2> m_badClus2D{
        this, hpath + "badClus2D", "Bad (<70% true) Clusters pos all modules", {{240, -60, 60}, {240, -60, 60}}};

    // residuals mchit to cluster pos
    mutable Gaudi::Accumulators::Histogram<1> m_resDx{
        this, hpath + "resDx", "Residuals along x [mm]", {200, -0.2, 0.2}};
    mutable Gaudi::Accumulators::Histogram<1> m_resDy{
        this, hpath + "resDy", "Residuals along y [mm]", {200, -0.2, 0.2}};

    // all cluster info
    mutable Gaudi::Accumulators::Histogram<1> m_numClus{this, hpath + "numClus", "# clusters", {250, 0, 10000}};
    mutable Gaudi::Accumulators::Histogram<1> m_numClusLog{this, hpath + "numClusLog", "log10(# clusters)", {80, 0, 8}};
    mutable Gaudi::Accumulators::Histogram<1> m_clusvR{
        this, hpath + "clusvR", "# cluster distribution - r", {50, 5.1, 50}};
    mutable Gaudi::Accumulators::Histogram<1> m_clusvModNum{
        this, hpath + "clusvModNum", "# cluster distribution - module", {52, -0.5, 51.5}};
    mutable Gaudi::Accumulators::Histogram<1> m_clusSize{
        this, hpath + "clusSize", "# pixel per cluster distribution", {51, -0.5, 50.5}};
    mutable Gaudi::Accumulators::Histogram<2> m_clusSizeSens{
        this, hpath + "clusSizeSens", "# pixel per cluster verse sensor", {{208, -0.5, 207.5}, {51, -0.5, 50.5}}};

    // purity plots
    mutable Gaudi::Accumulators::Histogram<1> m_clusPurity{
        this, hpath + "clusPurity", "Cluster Purity distribution", {110, 0, 1.1}};
    mutable Gaudi::Accumulators::Histogram<1> m_pixSeen{
        this, hpath + "pixSeen", "# pixel distribution - from one MCHit", {20, 0, 100}};
    mutable Gaudi::Accumulators::ProfileHistogram<1> m_purityPix{
        this, hpath + "purityPix", "Purity vs # pixels", {20, 0, 100}};

    // pixels in clusters
    mutable Gaudi::Accumulators::Histogram<1> m_clusSizeTrue{
        this, hpath + "clusSizeTrue", "Number of true pixels in cluster", {32, 0.5, 31.5}};
  };
  DECLARE_COMPONENT_WITH_ID( VPClusterEffSimDQ, "VPClusterEffSimDQ" )
} // namespace LHCb

//=============================================================================
// Standard constructor, initializes variables
//=============================================================================
LHCb::VPClusterEffSimDQ::VPClusterEffSimDQ( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {KeyValue{"VPClusterLocation", LHCb::VPFullClusterLocation::Default},
                KeyValue{"MCHitLocation", LHCb::MCHitLocation::VP},
                KeyValue{"MCParticleLocation", LHCb::MCParticleLocation::Default},
                KeyValue{"VPDigit2MCHitLinksLocation", "Link/" + LHCb::VPDigitLocation::Default + "2MCHits"}}} {}

//=============================================================================
// Set the ROOT file histogram top dir to VP for Velo Pixel histograms
//=============================================================================
//=============================================================================
// Main execution
//=============================================================================
void LHCb::VPClusterEffSimDQ::operator()( const std::vector<LHCb::VPFullCluster>& clusters, const LHCb::MCHits& mcHits,
                                          const LHCb::MCParticles&, const LHCb::LinksByKey& links ) const {
  if ( msgLevel( MSG::DEBUG ) ) {
    debug() << "Found " << clusters.size() << " VP clusters" << endmsg;
    debug() << "Found " << mcHits.size() << " mcHits" << endmsg;
  }

  // Table linking a LHCb::Detector::VPChannelID* to std::vector<LHCb::MCHit>
  std::map<const unsigned int, std::vector<LHCb::MCHit const*>> MCHitForchannelId;
  links.applyToAllLinks( [&MCHitForchannelId, &mcHits]( unsigned int channelId, unsigned int mcHitKey, float ) {
    MCHitForchannelId[channelId].emplace_back( mcHits[mcHitKey] );
  } );

  // Table linking a LHCb::MCHit const* to std::vector<LHCb::Detector::VPChannelID>> --orig
  std::map<LHCb::MCHit const*, std::vector<unsigned int>> channelIdForMCHit;
  links.applyToAllLinks( [&channelIdForMCHit, &mcHits]( unsigned int channelId, unsigned int mcHitKey, float ) {
    channelIdForMCHit[mcHits[mcHitKey]].emplace_back( channelId );
  } );
  ++m_numCalls; // update counter for number of calls
  for ( const auto& mcH : mcHits ) {
    // all MCHits
    // start updating histogram accumulators
    ++m_sensID[mcH->sensDetID()];
    ++m_mcHitXY[{mcH->midPoint().x(), mcH->midPoint().y()}];
    // distance particle traveled in sensor & energy
    auto dist = mcH->pathLength();
    ++m_mcHitLen[dist];
    // energy
    auto energy = mcH->energy();
    ++m_mcHitEnergy[energy];
    // were MCHits found
    double efficiency = ( channelIdForMCHit.find( mcH ) != channelIdForMCHit.end() );
    m_eff2DMCHit[{mcH->midPoint().x(), mcH->midPoint().y()}] += efficiency;
    m_effPath[dist] += efficiency;
    m_effRadius[mcH->midPoint().Rho()] += efficiency;
    m_effSensorNum[mcH->sensDetID()] += efficiency;
  }

  // plot info on clusters
  for ( auto& clus : clusters ) {
    unsigned int           mainPix  = 0;
    unsigned int           otherPix = 0;
    std::set<unsigned int> mcKeysClus;
    for ( auto& channelID : clus.pixels() ) {
      unsigned int numMC = 0;
      if ( MCHitForchannelId.find( channelID.channelID() ) != MCHitForchannelId.end() ) {
        numMC = ( MCHitForchannelId.find( channelID.channelID() )->second.size() );
      }
      ++m_hitPerPix[(double)numMC];

      // check if pixel is from MCHit
      if ( numMC != 0 ) {
        ++mainPix;
      } else {
        ++otherPix;
      }
    }
    if ( otherPix + mainPix <= 0 ) continue;
    double fracOther = ( (double)otherPix ) / ( (double)( otherPix + mainPix ) );
    ++m_fracNoise[fracOther];
    ++m_fracNoiseSize[{fracOther, clus.pixels().size()}];
    // plot positions of "good" clusters i.e. > 70% from MCHits
    if ( fracOther < 0.3 ) {
      ++m_goodClus2D[{clus.x(), clus.y()}];
    } else {
      ++m_badClus2D[{clus.x(), clus.y()}];
    }
  }

  // cluster residual plots
  for ( auto& clus : clusters ) {
    std::set<LHCb::MCHit const*> associated_hits;
    for ( auto& channelID : clus.pixels() ) {
      if ( MCHitForchannelId.find( channelID.channelID() ) != MCHitForchannelId.end() ) {
        for ( auto& mcHit : ( MCHitForchannelId.find( channelID.channelID() ) )->second ) {
          if ( channelIdForMCHit.find( mcHit ) != channelIdForMCHit.end() ) { associated_hits.insert( mcHit ); }
        }
      }
    }
    for ( auto& mcHit : associated_hits ) {
      double x_dist = clus.x() - mcHit->midPoint().x();
      double y_dist = clus.y() - mcHit->midPoint().y();
      ++m_resDx[x_dist];
      ++m_resDy[y_dist];
    }
  }

  // plot distribution number of clusters
  ++m_numClus[clusters.size()];
  // use histogram underflow (anything <0) for empty events
  ++m_numClusLog[clusters.size() > 0 ? log10( (double)clusters.size() ) : -1];
  for ( auto& clus : clusters ) {
    ++m_clusvR[sqrt( clus.x() * clus.x() + clus.y() * clus.y() )];
    ++m_clusvModNum[clus.channelID().module()];
    ++m_clusSize[clus.pixels().size()];
    ++m_clusSizeSens[{to_unsigned( clus.channelID().sensor() ), clus.pixels().size()}];
  }

  // purity plots
  for ( auto& clus : clusters ) {
    std::set<LHCb::MCHit const*> associated_hits;
    std::vector<unsigned int>    ids_clu;
    for ( auto& channelID : clus.pixels() ) {
      ids_clu.push_back( channelID.channelID() );
      auto i = MCHitForchannelId.find( channelID.channelID() );
      if ( i != MCHitForchannelId.end() ) associated_hits.insert( i->second.begin(), i->second.end() );
    }
    for ( auto& mcHit : associated_hits ) {
      std::vector<unsigned int> ids_hit;
      for ( auto& pix : ( *channelIdForMCHit.find( mcHit ) ).second ) { ids_hit.push_back( pix ); }
      std::sort( ids_clu.begin(), ids_clu.end() );
      std::sort( ids_hit.begin(), ids_hit.end() );
      std::vector<int> common;
      set_intersection( ids_clu.begin(), ids_clu.end(), ids_hit.begin(), ids_hit.end(), back_inserter( common ) );
      ++m_clusPurity[( (double)common.size() ) / ids_hit.size()];
      ++m_pixSeen[ids_hit.size()];
      m_purityPix[ids_hit.size()] += ( (double)common.size() ) / ids_hit.size();
    }
    ++m_clusSizeTrue[ids_clu.size()];
  }
}
