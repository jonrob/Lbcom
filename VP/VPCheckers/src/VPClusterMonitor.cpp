/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "DetDesc/GenericConditionAccessorHolder.h"
#include "Event/MCHit.h"
#include "Event/MCParticle.h"
#include "Event/VPCluster.h"
#include "GaudiAlg/GaudiHistoAlg.h"
#include "LHCbAlgs/Consumer.h"
#include "Linker/LinkedTo.h"
#include "VPDet/DeVP.h"

namespace LHCb {

  /**
   *  @author Daniel Hynds
   *  @date   2012-07-06
   */
  class VPClusterMonitor : public Algorithm::Consumer<void( VPClusters const&, LHCb::LinksByKey const&, DeVP const& ),
                                                      DetDesc::usesBaseAndConditions<GaudiHistoAlg, DeVP>> {
  public:
    VPClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator );
    StatusCode initialize() override;
    void       operator()( VPClusters const&, LHCb::LinksByKey const&, DeVP const& ) const override;
  };

  DECLARE_COMPONENT_WITH_ID( VPClusterMonitor, "VPClusterMonitor" )

} // namespace LHCb

LHCb::VPClusterMonitor::VPClusterMonitor( const std::string& name, ISvcLocator* pSvcLocator )
    : Consumer{name,
               pSvcLocator,
               {{"ClusterLocation", VPClusterLocation::Default},
                {"LinkedHitsLocation", LHCb::LinksByKey::linkerName( VPClusterLocation::Default + "2MCHits" )},
                {"DeVPLocation", DeVPLocation::Default}}} {}

StatusCode LHCb::VPClusterMonitor::initialize() {
  return Consumer::initialize().andThen( [&] { setHistoTopDir( "VP/" ); } );
}

void LHCb::VPClusterMonitor::operator()( const LHCb::VPClusters& clusters, LHCb::LinksByKey const& linkbykey,
                                         const DeVP& det ) const {
  if ( msgLevel( MSG::DEBUG ) ) { debug() << "Found " << clusters.size() << " clusters" << endmsg; }
  // Loop over the clusters.
  for ( const VPCluster* cluster : clusters ) {
    // Get sensor
    const DeVPSensor&                  sensor = det.sensor( cluster->channelID().sensor() );
    std::vector<Detector::VPChannelID> pixels = cluster->pixels();
    // Loop over the pixel hits.
    for ( const auto& pix : pixels ) {
      // Get the pixel position in the global frame and plot it.
      Gaudi::XYZPoint pointGlobal = sensor.channelToGlobalPoint( pix );
      plot( pointGlobal.rho(), "PixelRadius", 0, 100, 200 );
      plot2D( pointGlobal.x(), pointGlobal.y(), "PixelXY", -100, 100, -100, 100, 400, 400 );
      plot2D( pointGlobal.z(), pointGlobal.x(), "PixelZX", -500, 1000, -100, 100, 1000, 400 );
      plot2D( pointGlobal.z(), pointGlobal.y(), "PixelZY", -500, 1000, -100, 100, 1000, 400 );
    }
    const unsigned int nPixels = pixels.size();
    // Get the global cluster position.
    const double    x = cluster->x();
    const double    y = cluster->y();
    const double    z = cluster->z();
    Gaudi::XYZPoint pGlobal( x, y, z );
    const double    rho = pGlobal.rho();
    plot( nPixels, "ClusterSize", 0, 100, 100 );
    plot( rho, "ClusterRadius", 0, 100, 200 );
    plot2D( rho, nPixels, "ClusterSizeVsRadius", 0, 100, 0, 100, 200, 100 );
    plot2D( x, y, "ClusterXY", -100, 100, -100, 100, 400, 400 );
    plot2D( z, x, "ClusterZX", -500, 1000, -100, 100, 1000, 400 );
    plot2D( z, y, "ClusterZY", -500, 1000, -100, 100, 1000, 400 );
    plot( cluster->fraction().first, "InterpixelFractionX", -0.1, 1.1, 120 );
    plot( cluster->fraction().second, "InterpixelFractionY", -0.1, 1.1, 120 );

    // Get MC hit for this cluster and plot residuals
    auto hits = LinkedTo<MCHit>( &linkbykey ).range( cluster->channelID() );
    if ( hits.empty() ) continue;
    const MCHit& hit = hits.front();
    // Get true track direction for this hit
    const double yangle = atan( hit.dydz() ) / Gaudi::Units::degree;
    const double xangle = atan( hit.dxdz() ) / Gaudi::Units::degree;
    const double theta  = sqrt( xangle * xangle + yangle * yangle );
    plot( theta, "TrackTheta", 0., 50., 100 );
    plot( xangle, "TrackAngleX", -30., 30., 120 );
    plot( yangle, "TrackAngleY", -30., 30., 120 );
    // Get hit position.
    const Gaudi::XYZPoint mchitPoint = hit.midPoint();
    // Calculate the residuals.
    const double dx = x - mchitPoint.x();
    const double dy = y - mchitPoint.y();
    const double dz = z - mchitPoint.z();
    const double d3 = sqrt( dx * dx + dy * dy + dz * dz );
    // Plot the residuals.
    plot( dx, "ResidualsX", -0.2, 0.2, 4000 );
    plot( dy, "ResidualsY", -0.2, 0.2, 4000 );
    plot( d3, "Residuals3d", -0.2, 0.2, 4000 );
    if ( 1 == nPixels ) {
      plot( dx, "ResidualsX1", -0.2, 0.2, 4000 );
      plot( dy, "ResidualsY1", -0.2, 0.2, 4000 );
    } else if ( 2 == nPixels ) {
      plot( dx, "ResidualsX2", -0.2, 0.2, 4000 );
      plot( dy, "ResidualsY2", -0.2, 0.2, 4000 );
    }
    plot2D( theta, d3, "Residuals3dVsTrackTheta", 0., 50., -0.2, 0.2, 100, 400 );
    plot2D( rho, d3, "Residuals3dVsRadius", 0., 100, -0.2, 0.2, 200, 400 );
    plot2D( xangle, dx, "ResidualsXVsTrackAngleX", -30., 30., -0.2, 0.2, 120, 400 );
    plot2D( xangle, dy, "ResidualsYVsTrackAngleX", -30., 30., -0.2, 0.2, 120, 400 );
    plot2D( xangle, nPixels, "ClusterSizeVsTrackAngleX", -30., 30., 0, 20, 120, 20 );
    // Plot the same for small y angles
    if ( yangle < 2 && yangle > -2 ) {
      plot2D( xangle, dx, "ResidualsXVsTrackAngleXSmallY", -30., 30., -0.2, 0.2, 120, 400 );
      plot2D( xangle, dy, "ResidualsYVsTrackAngleXSmallY", -30., 30., -0.2, 0.2, 120, 400 );
      plot2D( xangle, nPixels, "ClusterSizeVsTrackAngleXSmallY", -30., 30., 0, 20, 120, 20 );
    }
  }
}
