/*****************************************************************************\
* (c) Copyright 2000-2018 CERN for the benefit of the LHCb Collaboration      *
*                                                                             *
* This software is distributed under the terms of the GNU General Public      *
* Licence version 3 (GPL Version 3), copied verbatim in the file "COPYING".   *
*                                                                             *
* In applying this licence, CERN does not waive the privileges and immunities *
* granted to it by virtue of its status as an Intergovernmental Organization  *
* or submit itself to any jurisdiction.                                       *
\*****************************************************************************/
#include "Event/IntLink.h"
#include "Event/MCMuonDigit.h"
#include "Event/MuonDigit.h"
#include "LHCbAlgs/Transformer.h"

#include <exception>
#include <fmt/format.h>

//-----------------------------------------------------------------------------
// Implementation file for class : MuonTileDigitInfo
//
// 2006-01-22 : Alessio Sarti
//-----------------------------------------------------------------------------

namespace LHCb {

  /**
   *  @author Alessia Satta
   *  @date   2005-12-30
   */
  struct MuonTileDigitInfo : public LHCb::Algorithm::Transformer<IntLink( MuonDigits const&, MCMuonDigits const& )> {
    MuonTileDigitInfo( const std::string& name, ISvcLocator* pSvcLocator );
    IntLink operator()( LHCb::MuonDigits const&, LHCb::MCMuonDigits const& ) const override;
  };

  DECLARE_COMPONENT_WITH_ID( MuonTileDigitInfo, "MuonTileDigitInfo" )

} // namespace LHCb

LHCb::MuonTileDigitInfo::MuonTileDigitInfo( const std::string& name, ISvcLocator* pSvcLocator )
    : Transformer( name, pSvcLocator,
                   {{"MuonDigitLocation", LHCb::MuonDigitLocation::MuonDigit},
                    {"MCMuonDigitLocation", LHCb::MCMuonDigitLocation::MCMuonDigit}},
                   {"OutputLocation", LHCb::MCMuonDigitLocation::MCMuonDigit + "Info"} ) {}

LHCb::IntLink LHCb::MuonTileDigitInfo::operator()( LHCb::MuonDigits const&   digits,
                                                   LHCb::MCMuonDigits const& mcDigits ) const {
  IntLink            myIntLink{};
  std::map<int, int> mylink; ///< list of linked ints

  // loop and link MuonDigits to MC truth
  for ( auto const& digit : digits ) {
    // match the MCMuonDigit to the MuonDigit via the Key
    LHCb::MCMuonDigit* mcDigit = mcDigits.object( digit->key() );
    if ( !mcDigit ) {
      throw std::logic_error( fmt::format( "Could not find the match for {} in {}", digit->key(),
                                           LHCb::MCMuonDigitLocation::MCMuonDigit ) );
    }
    unsigned int digitinfo = mcDigit->DigitInfo().DigitInfo();
    long int     tile      = digit->key();
    mylink.insert( std::make_pair( tile, digitinfo ) );
  }
  const std::map<int, int>& myList = mylink;
  myIntLink.setLink( myList );
  return myIntLink;
}
